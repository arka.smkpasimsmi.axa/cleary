-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 08 Bulan Mei 2020 pada 15.54
-- Versi server: 10.3.16-MariaDB
-- Versi PHP: 7.2.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cleary`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_auth`
--

CREATE TABLE `m_auth` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `password` varchar(150) NOT NULL,
  `role` varchar(10) NOT NULL,
  `pengajar_nip` int(11) DEFAULT NULL,
  `siswa_nis` int(11) DEFAULT NULL,
  `status_aktivasi` varchar(20) NOT NULL,
  `created_by` varchar(150) NOT NULL,
  `created_dt` datetime NOT NULL,
  `changed_by` varchar(150) NOT NULL,
  `changed_dt` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `m_auth`
--

INSERT INTO `m_auth` (`id`, `name`, `email`, `password`, `role`, `pengajar_nip`, `siswa_nis`, `status_aktivasi`, `created_by`, `created_dt`, `changed_by`, `changed_dt`) VALUES
(18, 'Gumiwa', 'gumiwa@gmail.com', '$2y$10$iW0./mpZ.dJdfW1lU1lGNu/Z0gwys0mkjrA4TKn3wN0qOXT29ME7e', 'pengajar', 123456789, NULL, 'dikonfirmasi', 'Gumiwa', '0000-00-00 00:00:00', '', '2020-05-08 13:34:25'),
(19, 'Nuraeni Gumilar', 'nuraeni@gmail.com', '$2y$10$H9S/AbhOBbs1EH2JCmfNIuuY2zY1nrEV2OQFhymtg7tj85jSm1QVW', 'pengajar', 234567890, NULL, 'dikonfirmasi', 'Nuraeni', '0000-00-00 00:00:00', '', '2020-05-08 13:34:25'),
(20, 'Ida Farida', 'ida@gmail.com', '$2y$10$stYOUaxO4BCZUStQcOCLp.hvGI2NE7jk/DNbL3F4FcntvJtQAOFTq', 'pengajar', 345678901, NULL, 'dikonfirmasi', 'Ida', '0000-00-00 00:00:00', '', '2020-05-08 13:34:25'),
(21, 'Saep Eko', 'saep@gmail.com', '$2y$10$.GMWWZSzTeypwPxSyda8sOH0QceloWVrTME7NvJpUvVPeuut2jN7m', 'pengajar', 456789012, NULL, 'dikonfirmasi', 'Saep', '0000-00-00 00:00:00', '', '2020-05-08 13:34:25'),
(22, 'Palwih Ngadiasih', 'palwih@gmail.com', '$2y$10$zj8qttXcB5eiYYiO90VXzOBsFlSQa8HTbQhoAmJ0rwbPn0sKsG.lK', 'pengajar', 567890123, NULL, 'dikonfirmasi', 'Palwih', '0000-00-00 00:00:00', '', '2020-05-08 13:34:25'),
(23, 'Lucky Aditya', 'lucky@gmail.com', '$2y$10$0iZ1k56y3ozA3vtj8z4Ne.HW/8rK1NNL5x9IqECMn.lNBIimQhcpO', 'pengajar', 678901234, NULL, 'dikonfirmasi', 'Lucky', '0000-00-00 00:00:00', '', '2020-05-08 13:34:25'),
(24, 'Dwi Dona Kustiowati', 'dwi@gmail.com', '$2y$10$oShld72h0lpJrpOOZN2Wsehd31X54CtfpT0iEXnsoUqORKl/2arJa', 'pengajar', 789012345, NULL, 'dikonfirmasi', 'Dwi', '0000-00-00 00:00:00', '', '2020-05-08 13:34:25'),
(25, 'Hanafi', 'hanafi@gmail.com', '$2y$10$o/Joy.PDcFYfOJYMc0uUGOe9LxALc7Fe0feifcQIaHzpWc2Pbvm7a', 'pengajar', 890123456, NULL, 'dikonfirmasi', 'Hanafi', '0000-00-00 00:00:00', '', '2020-05-08 13:34:25'),
(26, 'Tri Hastuti', 'tri@gmail.com', '$2y$10$7LC/jORKU3luTAKCcAM2cOVNzUvDc5Epy/EWTbg8hFRyWpi6luwRa', 'pengajar', 214748364, NULL, 'dikonfirmasi', 'Tri', '0000-00-00 00:00:00', '', '2020-05-08 13:34:25'),
(27, 'Lilis Rohayati', 'lilis@gmail.com', '$2y$10$NXgpSK1/a8FsINZmwnYAc.0mcDHrr0Cu8o2CB9nAbr9vOvbSPgUfW', 'pengajar', 254795364, NULL, 'dikonfirmasi', 'Lilis', '0000-00-00 00:00:00', '', '2020-05-08 13:34:25'),
(28, 'Vanny Tauresia', 'vanny@gmail.com', '$2y$10$ms222/b5vfDTzb7zV5DDwOi0kFhVYGwomeSVnW/O6aW66v29muj8O', 'pengajar', 234567215, NULL, 'dikonfirmasi', 'Vanny', '0000-00-00 00:00:00', '', '2020-05-08 13:34:25'),
(29, 'Marthalena Desi', 'marthalena@gmail.com', '$2y$10$wJ7GiI2CsMzk6qcbuU02X.qRUEQ52eYF3ThPsF2Km4GPBreVT2V1.', 'pengajar', 215942758, NULL, 'dikonfirmasi', 'Marthalena', '0000-00-00 00:00:00', '', '2020-05-08 13:34:25'),
(30, 'Axa Rajandrya', 'axa@gmail.com', '$2y$10$HtcxjrShr2Yg4Q7SZL74KOnEWnnfv97yBiLxQIvkw3QDxZgcMgemm', 'siswa', NULL, 234531587, 'dikonfirmasi', 'Axa', '0000-00-00 00:00:00', '', '2020-05-08 13:28:06');

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_essay`
--

CREATE TABLE `m_essay` (
  `id` int(11) NOT NULL,
  `id_essay` int(11) NOT NULL,
  `soal_essay` text NOT NULL,
  `created_by` varchar(150) NOT NULL,
  `created_dt` datetime NOT NULL DEFAULT current_timestamp(),
  `changed_by` varchar(150) DEFAULT NULL,
  `changed_dt` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `m_essay`
--

INSERT INTO `m_essay` (`id`, `id_essay`, `soal_essay`, `created_by`, `created_dt`, `changed_by`, `changed_dt`) VALUES
(31, 432, 'Yang dimaksud denganfardu kifayah adalah', 'Gumiwa', '2020-05-08 20:39:47', NULL, NULL),
(32, 432, 'Cara memandikan jenazah bayi laki-laki adalah', 'Gumiwa', '2020-05-08 20:39:47', NULL, NULL),
(33, 432, 'Orang yang berhak memandikan jenazah yaitu', 'Gumiwa', '2020-05-08 20:39:47', NULL, NULL),
(34, 432, 'Yang dimaksud dengan mengkafani jenazah adalah', 'Gumiwa', '2020-05-08 20:39:47', NULL, NULL),
(35, 432, 'Perbedaan cara mengkafani jenazah laki-laki dan perempuan adalah', 'Gumiwa', '2020-05-08 20:39:47', NULL, NULL),
(36, 432, 'Rukun dan sunnah shalat jenazah adalah', 'Gumiwa', '2020-05-08 20:39:47', NULL, NULL),
(37, 432, 'Syarat shalat jenazah adalah', 'Gumiwa', '2020-05-08 20:39:47', NULL, NULL),
(38, 432, 'Tata cara menguburkan jenazah diantaranya', 'Gumiwa', '2020-05-08 20:39:47', NULL, NULL),
(39, 432, 'Manfaat dan tujuan ta’ziyah adalah', 'Gumiwa', '2020-05-08 20:39:47', NULL, NULL),
(40, 432, 'Seseorang disunnahkan utk berziarah kubur karena ', 'Gumiwa', '2020-05-08 20:39:47', NULL, NULL),
(41, 435, 'Tulis dasar hukum jual beli dalam alquran lengkap dengan syakalnya dan terjemahkan!', 'Gumiwa', '2020-05-08 20:41:00', NULL, NULL),
(42, 435, 'Jelaskan pengertian jual beli garar!', 'Gumiwa', '2020-05-08 20:41:00', NULL, NULL),
(43, 435, 'Jelaskan perbedaan perbankan syariah dan konvensional!', 'Gumiwa', '2020-05-08 20:41:00', NULL, NULL),
(44, 435, 'Bagaimana ketentuan syirkah abdan, jelaskan!', 'Gumiwa', '2020-05-08 20:41:00', NULL, NULL),
(45, 266, 'Deskripsikan/ ceritakan bagaimana  sikap orang tuamu dalam menanggapi pembelajaran di rumah karena masalah kesehatan covid-19  ! Sikap orang tua saya setuju bahwa belajar di rumah merupakan salah satu bentuk pencegahan penularan virus corona. Tempat ramai seperti sekolah dan juga ruang publik lainnya dapat meningkatkan potensi penularan virus!', 'Hanafi', '2020-05-08 20:43:30', NULL, NULL),
(46, 266, 'Sesuai dengan hadits Riwayat Muslim, Mengapa  mengurus,   menyayangi dan menghormati orang tua  lebih mulia daripada berjihad / perang  di jalan Allah ! (tuliskan 3 alasan)', 'Hanafi', '2020-05-08 20:43:30', NULL, NULL),
(47, 266, 'Langkah-langkah apa yang harus kamu lakukan agar dapat memuliakan gurumu untuk medapatkan ilmu yang bermanfaat dan berkah darinya ! ( tuliskan 3 poin ) ', 'Hanafi', '2020-05-08 20:43:30', NULL, NULL),
(48, 266, 'Tulis kembali ayat berikut ini ke dalam bahasa arab yang baik dan benar : “…wahfidh lahumaa janaahadzdzulli minarrohmati  waqul robbirham humaa kamaa robbayaanii shoghiiroo …”Qs.Al-Isro ayat 24.! ', 'Hanafi', '2020-05-08 20:43:30', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_mapel`
--

CREATE TABLE `m_mapel` (
  `id` int(11) NOT NULL,
  `nama_mapel` varchar(150) NOT NULL,
  `created_by` varchar(150) NOT NULL,
  `created_dt` datetime NOT NULL DEFAULT current_timestamp(),
  `changed_by` varchar(150) DEFAULT NULL,
  `changed_dt` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `m_mapel`
--

INSERT INTO `m_mapel` (`id`, `nama_mapel`, `created_by`, `created_dt`, `changed_by`, `changed_dt`) VALUES
(3, 'PKN', '', '2020-05-08 19:58:51', NULL, NULL),
(4, 'Bahasa Indonesia', '', '2020-05-08 19:58:51', NULL, NULL),
(5, 'Matematika', '', '2020-05-08 19:58:51', NULL, NULL),
(6, 'Seni Budaya', '', '2020-05-08 19:58:51', NULL, NULL),
(7, 'PJOK', '', '2020-05-08 19:58:51', NULL, NULL),
(8, 'IPA', '', '2020-05-08 19:58:51', NULL, NULL),
(9, 'IPS', '', '2020-05-08 19:58:51', NULL, NULL),
(10, 'Bahasa Sunda', '', '2020-05-08 19:58:51', NULL, NULL),
(11, 'Biologi', '', '2020-05-08 19:58:51', NULL, NULL),
(12, 'Fisika', '', '2020-05-08 19:58:51', NULL, NULL),
(13, 'Kimia', '', '2020-05-08 19:58:51', NULL, NULL),
(14, 'Ekonomi', '', '2020-05-08 19:58:51', NULL, NULL),
(15, 'Sejarah', '', '2020-05-08 19:58:51', NULL, NULL),
(16, 'Bahasa Inggris', '', '2020-05-08 19:58:51', NULL, NULL),
(17, 'Sosiologi', '', '2020-05-08 19:58:51', NULL, NULL),
(18, 'Geografi', '', '2020-05-08 19:58:51', NULL, NULL),
(19, 'Komputer', '', '2020-05-08 19:58:51', NULL, NULL),
(20, 'Agama', '', '2020-05-08 19:58:51', NULL, NULL),
(21, 'Bahasa Jepang', '', '2020-05-08 19:58:51', NULL, NULL),
(22, 'Bahasa Korea', '', '2020-05-08 19:58:51', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_materi`
--

CREATE TABLE `m_materi` (
  `id` int(11) NOT NULL,
  `deskripsi_materi` text NOT NULL,
  `created_by` varchar(150) NOT NULL,
  `created_dt` datetime NOT NULL DEFAULT current_timestamp(),
  `changed_by` varchar(150) DEFAULT NULL,
  `changed_dt` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_pengajar`
--

CREATE TABLE `m_pengajar` (
  `nip` int(11) NOT NULL,
  `nama_pengajar` varchar(150) NOT NULL,
  `mapel_id` int(11) NOT NULL,
  `value_system` varchar(150) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `no_hp` varchar(150) DEFAULT NULL,
  `alamat` text NOT NULL,
  `bio` text DEFAULT NULL,
  `avatar` varchar(150) NOT NULL,
  `pengikut_nis` text DEFAULT NULL,
  `modul_id` bigint(20) NOT NULL,
  `created_by` varchar(150) NOT NULL,
  `created_dt` datetime NOT NULL DEFAULT current_timestamp(),
  `changed_by` varchar(150) DEFAULT NULL,
  `changed_dt` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `m_pengajar`
--

INSERT INTO `m_pengajar` (`nip`, `nama_pengajar`, `mapel_id`, `value_system`, `tanggal_lahir`, `no_hp`, `alamat`, `bio`, `avatar`, `pengikut_nis`, `modul_id`, `created_by`, `created_dt`, `changed_by`, `changed_dt`) VALUES
(123456789, 'Gumiwa', 20, 'Islam| Laki-laki| Kawin', '2020-05-08', NULL, 'Jl. Guntur', NULL, 'Gumiwa-20-05-08.png', NULL, 0, 'Gumiwa', '2020-05-08 20:08:30', NULL, NULL),
(214748364, 'Tri Hastuti', 4, 'Islam| Perempuan| Belum Kawin', '2020-05-08', NULL, 'Jl. Salatiga', NULL, 'Tri_Hastuti-20-05-08.jpg', NULL, 0, 'Tri', '2020-05-08 20:20:32', NULL, NULL),
(215942758, 'Marthalena Desi', 13, 'Islam| Perempuan| Kawin', '2019-11-29', NULL, 'Jl. Raja', NULL, 'Marthalena_Desi-20-05-08.png', NULL, 0, 'Marthalena', '2020-05-08 20:26:27', NULL, NULL),
(234567215, 'Vanny Tauresia', 5, 'Protestan| Perempuan| Belum Kawin', '2020-06-04', NULL, 'Jl. Kokasih', NULL, 'Vanny_Tauresia-20-05-08.png', NULL, 0, 'Vanny', '2020-05-08 20:23:10', NULL, NULL),
(234567890, 'Nuraeni Gumilar', 21, 'Islam| Perempuan| Belum Kawin', '2020-05-06', NULL, 'Jl. Cahaya', NULL, 'Nuraeni_Gumilar-20-05-08.png', NULL, 0, 'Nuraeni', '2020-05-08 20:09:30', NULL, NULL),
(254795364, 'Lilis Rohayati', 10, 'Islam| Laki-laki| Kawin', '2020-06-01', NULL, 'Jl. Lama', NULL, 'Lilis_Rohayati-20-05-08.jpg', NULL, 0, 'Lilis', '2020-05-08 20:21:38', NULL, NULL),
(345678901, 'Ida Farida', 5, 'Islam| Perempuan| Kawin', '2020-05-13', NULL, 'Jl. Furi', NULL, 'Ida_Farida-20-05-08.png', NULL, 0, 'Ida', '2020-05-08 20:11:47', NULL, NULL),
(456789012, 'Saep Eko', 15, 'Protestan| Laki-laki| Kawin', '2020-05-08', NULL, 'Jl. Tief', NULL, 'Saep_Eko-20-05-08.png', NULL, 0, 'Saep', '2020-05-08 20:14:12', NULL, NULL),
(567890123, 'Palwih Ngadiasih', 3, 'Hindu| Laki-laki| Belum Kawin', '2020-02-08', NULL, 'Jl. Angki', NULL, 'Palwih_Ngadiasih-20-05-08.png', NULL, 0, 'Palwih', '2020-05-08 20:16:48', NULL, NULL),
(678901234, 'Lucky Aditya', 5, 'Protestan| Laki-laki| Belum Kawin', '2020-05-08', NULL, 'Jl. Ningsih', NULL, 'Lucky_Aditya-20-05-08.png', NULL, 0, 'Lucky', '2020-05-08 20:17:38', NULL, NULL),
(789012345, 'Dwi Dona Kustiowati', 8, 'Islam| Laki-laki| Belum Kawin', '2020-06-05', NULL, 'Jl. Dayeuh', NULL, 'Dwi_Dona_Kustiowati-20-05-08.jpg', NULL, 0, 'Dwi', '2020-05-08 20:18:35', NULL, NULL),
(890123456, 'Hanafi', 20, 'Islam| Laki-laki| Kawin', '2020-04-27', NULL, 'Jl. Sisik', NULL, 'Hanafi-20-05-08.jpg', NULL, 0, 'Hanafi', '2020-05-08 20:19:10', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_pilgan`
--

CREATE TABLE `m_pilgan` (
  `id` int(11) NOT NULL,
  `id_pilgan` int(11) NOT NULL,
  `soal_pilgan` text NOT NULL,
  `a` text NOT NULL,
  `b` text NOT NULL,
  `c` text NOT NULL,
  `d` text NOT NULL,
  `jawaban` text NOT NULL,
  `created_by` varchar(150) NOT NULL,
  `created_dt` datetime NOT NULL DEFAULT current_timestamp(),
  `changed_by` varchar(150) DEFAULT NULL,
  `changed_dt` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_siswa`
--

CREATE TABLE `m_siswa` (
  `nis` int(11) NOT NULL,
  `nama_siswa` varchar(150) NOT NULL,
  `value_system` varchar(150) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `no_hp` varchar(150) NOT NULL,
  `alamat` text NOT NULL,
  `bio` text NOT NULL,
  `avatar` varchar(150) NOT NULL,
  `daftar_nilai` text NOT NULL,
  `mengikuti_nip` text DEFAULT NULL,
  `created_by` varchar(150) NOT NULL,
  `created_dt` datetime NOT NULL,
  `changed_by` varchar(150) NOT NULL,
  `changed_dt` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `m_siswa`
--

INSERT INTO `m_siswa` (`nis`, `nama_siswa`, `value_system`, `tanggal_lahir`, `no_hp`, `alamat`, `bio`, `avatar`, `daftar_nilai`, `mengikuti_nip`, `created_by`, `created_dt`, `changed_by`, `changed_dt`) VALUES
(234531587, 'Axa Rajandrya', 'Islam | Laki-laki  ', '2003-04-19', '', 'Jl. Nyomplong', '', 'Axa_Rajandrya-20-05-08.jpg', '', NULL, 'Axa', '0000-00-00 00:00:00', '', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_system`
--

CREATE TABLE `m_system` (
  `system_type` varchar(255) NOT NULL,
  `system_cd` varchar(255) NOT NULL,
  `system_value_txt` text DEFAULT NULL,
  `system_value_num` int(11) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_dt` datetime DEFAULT NULL,
  `changed_by` varchar(255) DEFAULT NULL,
  `changed_dt` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data untuk tabel `m_system`
--

INSERT INTO `m_system` (`system_type`, `system_cd`, `system_value_txt`, `system_value_num`, `description`, `created_by`, `created_dt`, `changed_by`, `changed_dt`) VALUES
('AGAMA', 'AGM001', 'Islam', NULL, NULL, 'SYSTEM', NULL, NULL, NULL),
('AGAMA', 'AGM002', 'Protestan', NULL, NULL, 'SYSTEM', NULL, NULL, NULL),
('AGAMA', 'AGM003', 'Katolik', NULL, NULL, 'SYSTEM', NULL, NULL, NULL),
('AGAMA', 'AGM004', 'Hindu', NULL, NULL, 'SYSTEM', NULL, NULL, NULL),
('AGAMA', 'AGM005', 'Buddha', NULL, NULL, 'SYSTEM', NULL, NULL, NULL),
('AGAMA', 'AGM006', 'Kong Hu Cu', NULL, NULL, 'SYSTEM', NULL, NULL, NULL),
('JENIS_KELAMIN', 'JNS001', 'Laki-laki', NULL, NULL, 'SYSTEM', NULL, NULL, NULL),
('JENIS_KELAMIN', 'JNS002', 'Perempuan', NULL, NULL, 'SYSTEM', NULL, NULL, NULL),
('STATUS_KAWIN', 'STS001', 'Kawin', NULL, NULL, 'SYSTEM', NULL, NULL, NULL),
('STATUS_KAWIN', 'STS002', 'Belum Kawin', NULL, NULL, 'SYSTEM', NULL, NULL, NULL),
('TINGKAT_SEKOLAH', 'TNGKT001', 'SD', NULL, NULL, 'SYSTEM', NULL, NULL, NULL),
('TINGKAT_SEKOLAH', 'TNGKT002', 'SMP', NULL, NULL, 'SYSTEM', NULL, NULL, NULL),
('TINGKAT_SEKOLAH', 'TNGKT003', 'SMA', NULL, NULL, 'SYSTEM', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_tipe_modul`
--

CREATE TABLE `m_tipe_modul` (
  `id` int(11) NOT NULL,
  `tipe_modul` varchar(150) NOT NULL,
  `created_by` varchar(150) NOT NULL,
  `created_dt` datetime NOT NULL DEFAULT current_timestamp(),
  `changed_by` varchar(150) DEFAULT NULL,
  `changed_dt` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `m_tipe_modul`
--

INSERT INTO `m_tipe_modul` (`id`, `tipe_modul`, `created_by`, `created_dt`, `changed_by`, `changed_dt`) VALUES
(1, 'Materi', 'Admin', '2020-03-29 11:40:36', NULL, NULL),
(2, 'Pilihan Ganda', 'Admin', '2020-03-29 11:40:36', NULL, NULL),
(3, 'Essay', 'Admin', '2020-03-29 11:40:36', NULL, NULL),
(4, 'Pilihan Ganda & Essay', 'Admin', '2020-03-29 11:40:36', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `r_modul`
--

CREATE TABLE `r_modul` (
  `id_modul` int(11) NOT NULL,
  `pengajar_nip` int(11) NOT NULL,
  `judul_modul` varchar(150) NOT NULL,
  `materi_id` int(11) DEFAULT NULL,
  `pilgan_id` int(11) DEFAULT NULL,
  `essay_id` int(11) DEFAULT NULL,
  `mapel_id` int(11) DEFAULT NULL,
  `tipe_modul_id` int(11) DEFAULT NULL,
  `waktu_pengerjaan` time DEFAULT NULL,
  `batas_waktu` date DEFAULT NULL,
  `created_by` varchar(150) NOT NULL,
  `created_dt` datetime NOT NULL DEFAULT current_timestamp(),
  `changed_by` varchar(150) DEFAULT NULL,
  `changed_dt` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `r_modul`
--

INSERT INTO `r_modul` (`id_modul`, `pengajar_nip`, `judul_modul`, `materi_id`, `pilgan_id`, `essay_id`, `mapel_id`, `tipe_modul_id`, `waktu_pengerjaan`, `batas_waktu`, `created_by`, `created_dt`, `changed_by`, `changed_dt`) VALUES
(62, 123456789, 'Pemulasaraan Jenazah', NULL, NULL, 432, 20, 3, '08:45:00', '2020-05-27', 'Gumiwa', '2020-05-08 20:39:47', NULL, NULL),
(63, 123456789, 'Ekonomi Syariah', NULL, NULL, 435, 20, 3, '08:45:00', '2020-06-03', 'Gumiwa', '2020-05-08 20:41:00', NULL, NULL),
(64, 890123456, 'Hormat kepada Orang Tua & Guru', NULL, NULL, 266, 20, 3, '08:45:00', '2020-06-02', 'Hanafi', '2020-05-08 20:43:30', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `r_nilai`
--

CREATE TABLE `r_nilai` (
  `id` int(11) NOT NULL,
  `siswa_nis` int(11) NOT NULL,
  `modul_id` int(11) NOT NULL,
  `nilai` int(11) NOT NULL,
  `mapel_id` int(11) NOT NULL,
  `tipe_modul_id` int(11) NOT NULL,
  `pengajar_nip` int(11) NOT NULL,
  `created_by` varchar(150) NOT NULL,
  `created_dt` datetime NOT NULL DEFAULT current_timestamp(),
  `changed_by` varchar(150) NOT NULL,
  `changed_dt` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `m_auth`
--
ALTER TABLE `m_auth`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `m_essay`
--
ALTER TABLE `m_essay`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `m_mapel`
--
ALTER TABLE `m_mapel`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `m_materi`
--
ALTER TABLE `m_materi`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `m_pengajar`
--
ALTER TABLE `m_pengajar`
  ADD PRIMARY KEY (`nip`);

--
-- Indeks untuk tabel `m_pilgan`
--
ALTER TABLE `m_pilgan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `m_siswa`
--
ALTER TABLE `m_siswa`
  ADD PRIMARY KEY (`nis`);

--
-- Indeks untuk tabel `m_system`
--
ALTER TABLE `m_system`
  ADD PRIMARY KEY (`system_cd`);

--
-- Indeks untuk tabel `m_tipe_modul`
--
ALTER TABLE `m_tipe_modul`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `r_modul`
--
ALTER TABLE `r_modul`
  ADD PRIMARY KEY (`id_modul`);

--
-- Indeks untuk tabel `r_nilai`
--
ALTER TABLE `r_nilai`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `m_auth`
--
ALTER TABLE `m_auth`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT untuk tabel `m_essay`
--
ALTER TABLE `m_essay`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT untuk tabel `m_mapel`
--
ALTER TABLE `m_mapel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT untuk tabel `m_materi`
--
ALTER TABLE `m_materi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT untuk tabel `m_pilgan`
--
ALTER TABLE `m_pilgan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT untuk tabel `m_tipe_modul`
--
ALTER TABLE `m_tipe_modul`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `r_modul`
--
ALTER TABLE `r_modul`
  MODIFY `id_modul` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT untuk tabel `r_nilai`
--
ALTER TABLE `r_nilai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
