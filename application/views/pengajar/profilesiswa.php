<?php foreach($profilesiswa as $data) : ?>
    <div class="main-content" style="min-height: 562px;">
        <section class="section">
          <div class="section-header">
            <h1>Detail Profile</h1>
          </div>
          <div class="section-body">
            <div class="row mt-sm-4">
              <div class="col-12 col-md-12 col-lg-5">
                <div class="card profile-widget">
                  <div class="profile-widget-header">                     
                    <img alt="image" src="<?= base_url('assets/img/avatar/'.$data->avatar); ?>" class="rounded-circle profile-widget-picture" width="50" height="100">
                    <div class="profile-widget-items">
                      <div class="profile-widget-item">
                        <div class="profile-widget-item-label">Total Modul</div>
                        <div class="profile-widget-item-value">2</div>
                      </div>
                      <div class="profile-widget-item">
                        <div class="profile-widget-item-label">Mengikuti</div>
                        <div class="profile-widget-item-value">30</div>
                      </div>
                    </div>
                  </div>
                  <div class="profile-widget-description">
                      <div class="profile-widget-name ml-2"><?php if($this->session->userdata('role')== 'pengajar'){echo "Siswa";} ?></div>
                      <?= $data->bio; ?>
                  </div>
                  <div class="card-footer text-center">
                    <?php if($this->session->userdata('role') == 'siswa') : ?>
                      <?= $this->session->userdata('email') ?>
                    <?php endif; ?>  
                  </div>
                </div>
              </div>
              <div class="col-12 col-md-12 col-lg-7">
                <div class="card">
                    <div class="card-header">
                      <h4>Detail Lengkap Profile</h4>
                    </div>
                    <div class="card-body">
                        <div class="row">                               
                          <div class="form-group col-6">
                            <label>Nama Lengkap</label>
                            <h6><?= $data->nama_siswa; ?></h6> 
                          </div>
                          <div class="form-group col-6">
                            <label>Tanggal Lahir</label>
                            <h6><?= $data->tanggal_lahir; ?></h6> 
                        </div>
                        </div>
                        <div class="row">
                            <?php $data = explode('| ', $data->value_system) ?>
                        <div class="form-group col-4">
                      <label for="jenis_kelamin" class="d-block">Jenis Kelamin</label>
                            <h6><?php print_r($data[1]); ?></h6>  
                      <small class="form-text text-danger" style="font-style: italic;"><?= form_error('jenis_kelamin'); ?></small>
                    </div>
                    <div class="form-group col-4">
                      <label for="agama" class="d-block">Agama</label>
                            <h6><?php print_r($data[0]); ?></h6>  
                    </div>
                          <div class="form-group col-4">
                          <?php foreach($profilesiswa as $data) : ?>
                            <label>No.Handphone</label>
                            <?php if($data->no_hp == "") : ?>
                            <h5>-</h5>
                            <?php else : ?>
                            <h6><?= $data->no_hp; ?></h6>
                            <?php endif; ?>
                          <?php endforeach; ?>
                        </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                            <?php foreach($profilesiswa as $data) : ?>
                                <label for="alamat">Alamat</label>
                                <h6><?= $data->alamat; ?></h6> 
                            <?php endforeach; ?>        
                            </div>
                        </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
<?php endforeach; ?>