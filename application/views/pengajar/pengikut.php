<?php if (!$this->session->userdata('nip')) {
  $this->session->set_flashdata('info', 'Silahkan login terlebih dahulu!');
  Redirect('Auth/login');
}elseif (!$this->session->userdata('role') == 'pengajar') {
  $this->session->set_flashdata('info', 'Silahkan daftar sebagai pengajar terlebih dahulu!');
} ?>

<div class="main-content" style="min-height: 562px;">
  <section class="section">
    <div class="section-header">
      <h1>Pengikut</h1>
    </div>
  </section>
  <div class="row">
    <?php foreach($pengikut as $list) : ?>
      <div class="col-lg-4 col-md-4">
        <div class="card card-primary">
          <div class="row">             
              <img alt="image" width="70" height="70" src="<?= base_url('assets/img/avatar/'.$list->avatar); ?>" class="rounded-circle profile-widget-picture ml-4 mt-3"> 
              <div class="mt-4 ml-4">
                <h5><?= $list->nama_siswa; ?></h5>
            </div>
          </div>
            <div class="card-header">
              <h4></h4>
              <div class="card-header-action">
                <a href="<?= base_url('Pengajar/profilesiswa/'.$list->nis); ?>" class="btn btn-success">Profile</a>
                </div>
            </div>
        </div>
      </div>
    <?php endforeach; ?> 
  </div>      
</div>      