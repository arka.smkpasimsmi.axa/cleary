<?php if (!$this->session->userdata('nip')) {
	$this->session->set_flashdata('info', 'Silahkan login terlebih dahulu!');
	Redirect('Auth/login');
}elseif (!$this->session->userdata('role') == 'pengajar') {
	$this->session->set_flashdata('info', 'Silahkan daftar sebagai pengajar terlebih dahulu!');
} ?>