<?php if (!$this->session->userdata('nip')) {
  $this->session->set_flashdata('info', 'Silahkan login terlebih dahulu!');
  Redirect('Auth/login');
}elseif (!$this->session->userdata('role') == 'pengajar') {
  $this->session->set_flashdata('info', 'Silahkan daftar sebagai pengajar terlebih dahulu!');
} ?>

<div class="main-content" style="min-height: 405px;">
  <section class="section">
    <div class="section-header">
      <h1>Mulai</h1>
    </div>

    <div class="section-body">
      <h2 class="section-title">Buat Modul</h2>
      <p class="section-lead">Buat modul anda di halaman ini.</p>
      <div class="card">
        <div class="card-header">
          <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
              <a class="nav-link active" id="materi-tab" data-toggle="tab" href="#materi" role="tab" aria-controls="materi" aria-selected="true">Materi</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="pilgan-tab" data-toggle="tab" href="#pilgan" role="tab" aria-controls="pilgan" aria-selected="false">Pilihan Ganda</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="essay-tab" data-toggle="tab" href="#essay" role="tab" aria-controls="essay" aria-selected="false">Essay</a>
            </li>
          </ul>
        </div>
        <div class="card-body">
          <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="materi" role="tabpanel" aria-labelledby="materi-tab">
              <form method="POST" action="<?= base_url('Pengajar/materi') ?>" enctype="multipart/form-data">
                <div class="card">
                  <div class="card-body">
                  <div style="background-color: transparent; padding: 20px 25px; text-align: center;">
                    <h4 style="font-size: 16px;">Materi</h4>
                  </div>
                    <div class="form-group row mb-4">
                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="judul_materi">Judul</label>
                      <div class="col-sm-12 col-md-7">
                        <input type="text" class="form-control" name="judul_materi" id="judul_materi" placeholder="Judul Materi" autocomplete="off" value="<?= set_value('judul_materi') ?>" required autofocus>
                        <small class="text-danger"><i><?= form_error('judul_materi') ?></i></small>
                      </div>
                    </div>
                    <div class="form-group row mb-4">
                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="nama_mapel">Mapel</label>
                      <div class="col-sm-12 col-md-7">
                        <?php foreach($auth as $data) : ?>
                          <input type="hidden" name="mapel_id" value="<?= $data->mapel_id ?>" readonly>
                          <input type="text" class="form-control" name="nama_mapel" id="nama_mapel" autocomplete="off" readonly value="<?= $data->nama_mapel ?>">
                        <?php endforeach; ?>
                      </div>
                    </div>
                    <div class="form-group row mb-4">
                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="deskripsi_materi">Deskripsi</label>
                      <div class="col-sm-12 col-md-7">
                        <textarea class="summernote" name="deskripsi_materi" id="deskripsi_materi"><?= set_value('deskripsi_materi') ?></textarea>
                        <small class="text-danger"><i><?= form_error('deskripsi_materi') ?></i></small>
                      </div>
                    </div>
                    <div class="form-group row mb-4">
                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                      <div class="col-sm-12 col-md-7">
                        <button class="btn btn-primary" type="submit">Tambah</button>
                      </div>
                    </div>
                  </div>
                </div>
              </form>
            </div>
            <div class="tab-pane fade" id="pilgan" role="tabpanel" aria-labelledby="pilgan-tab">
              <form method="POST" action="<?= base_url('Pengajar/pilgan') ?>">
                <div class="card">
                  <div class="card-body">
                  <div style="background-color: transparent; padding: 20px 25px; text-align: center;">
                    <h4 style="font-size: 16px;">Pilihan Ganda</h4>
                  </div>
                    <div id="form-pilgan">
                      <div class="form-group row mb-4">
                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="judul_pilgan">Judul</label>
                        <div class="col-sm-12 col-md-7">
                          <input type="text" class="form-control" name="judul_pilgan" id="judul_pilgan" placeholder="Judul Pilgan" autocomplete="off" value="<?= set_value('judul_pilgan') ?>" required autofocus>
                          <small class="text-danger"><i><?= form_error('judul_pilgan') ?></i></small>
                        </div>
                      </div>
                      <div class="form-group row mb-1">
                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                        <div class="col-sm-12 col-md-7">Soal</div>
                      </div>
                      <div class="form-group row mb-2">
                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="soal_pilgan">1.</label>
                        <div class="col-sm-12 col-md-7 col-lg-7">
                          <input type="text" class="form-control" name="soal_pilgan[]" id="soal_pilgan" placeholder="Soal Pilgan" autocomplete="off" value="<?= set_value('soal_pilgan') ?>" required autofocus>
                          <small class="text-danger"><i><?= form_error('soal_pilgan') ?></i></small>
                        </div>
                        <div class="col-sm-1">
                            <i class="fas fa-plus" id="addSoalPilgan" style="cursor: pointer;"></i>
                        </div>
                      </div>
                      <div class="form-group row mb-2">
                        <input type="hidden" name="mapel_id" value="<?= $data->mapel_id ?>" readonly>
                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-1" for="a">A.</label>
                        <div class="col-sm-12 col-md-3 col-lg-2">
                            <input type="text" class="form-control form-control-sm" name="a[]" id="a" autocomplete="off">
                        </div>
                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-1" for="c">C.</label>
                        <div class="col-sm-12 col-md-3 col-lg-2">
                            <input type="text" class="form-control form-control-sm" name="c[]" id="c" autocomplete="off">
                        </div>
                      </div>
                      <div class="form-group row mb-2">
                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-1" for="b">B.</label>
                        <div class="col-sm-12 col-md-3 col-lg-2">
                            <input type="text" class="form-control form-control-sm" name="b[]" id="b" autocomplete="off">
                        </div>
                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-1" for="d">D.</label>
                        <div class="col-sm-12 col-md-3 col-lg-2">
                            <input type="text" class="form-control form-control-sm" name="d[]" id="d" autocomplete="off">
                        </div>
                      </div>
                      <div class="form-group row mb-4">
                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="jawaban">Jawaban</label>
                        <div class="col-sm-12 col-md-7 col-lg-7">
                          <select name="jawaban[]" id="jawaban" class="form-control form-control-sm">
                            <option value="a" id="a">Pilih Jawaban</option>
                            <option value="a" id="a">A</option>
                            <option value="b" id="b">B</option>
                            <option value="c" id="c">C</option>
                            <option value="d" id="d">D</option>
                          </select>
                        </div>
                      </div>
                    </div>
                    <hr>
                    <div class="form-group row mb-4">
                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="waktu_pengerjaan">Waktu Pengerjaan</label>
                        <div class="col-sm-12 col-md-7">
                          <input type="text" name="waktu_pengerjaan" id="waktu_pengerjaan" class="form-control timepicker">
                        </div>
                    </div>
                    <div class="form-group row mb-4">
                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="batas_waktu">Batas Waktu</label>
                        <div class="col-sm-12 col-md-7">
                          <input type="date" name="batas_waktu" id="batas_waktu" class="form-control" value="<?= set_value('batas_waktu') ?>" required>
                          <small class="text-danger"><i><?= form_error('batas_waktu') ?></i></small>
                        </div>
                    </div>
                    <div class="form-group row mb-4">
                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                      <div class="col-sm-12 col-md-7">
                        <button class="btn btn-primary" type="submit">Tambah</button>
                      </div>
                    </div>
                  </div>
                </div>
              </form>
            </div>
            <div class="tab-pane fade" id="essay" role="tabpanel" aria-labelledby="essay-tab">
              <form method="POST" action="<?= base_url('Pengajar/essay') ?>">
                <div class="card">
                  <div class="card-body">
                  <div style="background-color: transparent; padding: 20px 25px; text-align: center;">
                    <h4 style="font-size: 16px;">Essay</h4>
                  </div>
                  <div class="form-group row mb-4">
                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="judul_essay">Judul</label>
                    <div class="col-sm-12 col-md-7">
                      <input type="text" class="form-control" name="judul_essay" id="judul_essay" placeholder="Judul Essay" autocomplete="off" value="<?= set_value('judul_essay') ?>" required autofocus>
                      <small class="text-danger"><i><?= form_error('judul_essay') ?></i></small>
                    </div>
                  </div>
                  <div class="form-group row mb-1">
                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                    <div class="col-sm-12 col-md-7">Soal</div>
                  </div>
                  <div id="form-essay">
                    <div class="form-group row mb-4">
                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="soal_essay">1.</label>
                      <div class="col-sm-11 col-md-7">
                        <input type="hidden" name="mapel_id" value="<?= $data->mapel_id ?>" readonly>
                        <input type="text" class="form-control" name="soal_essay[]" id="soal_essay" placeholder="Soal Essay" autocomplete="off" value="<?= set_value('soal_essay') ?>" required autofocus>
                        <small class="text-danger"><i><?= form_error('soal_essay') ?></i></small>
                      </div>
                      <div class="col-lg-1">
                          <i class="fas fa-plus" id="addSoalEssay" style="cursor: pointer;"></i>
                      </div>
                    </div>
                  </div>
                  <hr>
                  <div class="form-group row mb-4">
                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="waktu_pengerjaan">Waktu Pengerjaan</label>
                      <div class="col-sm-12 col-md-7">
                        <input type="text" name="waktu_pengerjaan" id="waktu_pengerjaan" class="form-control timepicker">
                      </div>
                  </div>
                  <div class="form-group row mb-4">
                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="batas_waktu">Batas Waktu</label>
                      <div class="col-sm-12 col-md-7">
                        <input type="date" name="batas_waktu" id="batas_waktu" class="form-control" value="<?= set_value('batas_waktu') ?>" required>
                        <small class="text-danger"><i><?= form_error('batas_waktu') ?></i></small>
                      </div>
                  </div>
                  <div class="form-group row mb-4">
                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                    <div class="col-sm-12 col-md-7">
                      <button class="btn btn-primary" type="submit">Tambah</button>
                    </div>
                  </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<script>
  $(document).ready(function(){
   
  // Add Soal Essay
    $("#addSoalPilgan").click(function() {
       $("#form-pilgan").append('<div id="input-group-remove"><div class="form-group row mb-2"><label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="soal_pilgan">2.</label><div class="col-sm-12 col-md-7 col-lg-7"><input type="text" class="form-control" name="soal_pilgan[]" id="soal_pilgan" placeholder="Soal Pilgan" autocomplete="off" value="<?= set_value('soal_pilgan') ?>" required autofocus><small class="text-danger"><i><?= form_error('soal_pilgan') ?></i></small></div><div class="col-sm-1"><i class="fas fa-minus" id="removeSoalPilgan" style="cursor: pointer;"></i></div></div><div class="form-group row mb-2"><label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label><label class="col-form-label text-md-right col-12 col-md-3 col-lg-1" for="a">A.</label> <div class="col-sm-12 col-md-3 col-lg-2"><input type="text" class="form-control form-control-sm" name="a[]" id="a" autocomplete="off"></div><label class="col-form-label text-md-right col-12 col-md-3 col-lg-1" for="c">C.</label><div class="col-sm-12 col-md-3 col-lg-2"><input type="text" class="form-control form-control-sm" name="c[]" id="c" autocomplete="off"></div></div><div class="form-group row mb-2"><label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label><label class="col-form-label text-md-right col-12 col-md-3 col-lg-1" for="b">B.</label><div class="col-sm-12 col-md-3 col-lg-2"><input type="text" class="form-control form-control-sm" name="b[]" id="b" autocomplete="off"></div>  <label class="col-form-label text-md-right col-12 col-md-3 col-lg-1" for="d">D.</label><div class="col-sm-12 col-md-3 col-lg-2"><input type="text" class="form-control form-control-sm" name="d[]" id="d" autocomplete="off"></div></div><div class="form-group row mb-4"><label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="Jawaban">Jawaban</label><div class="col-sm-12 col-md-7 col-lg-7"><select name="jawaban[]" id="jawaban" class="form-control form-control-sm"><option value="a" id="a">Pilih Jawaban</option><option value="a" id="a">A</option><option value="b" id="b">B</option><option value="c" id="c">C</option><option value="d" id="d">D</option></select></div></div></div>');
    });

  // Remove Soal Essay
    $("body").on("click","#removeSoalPilgan",function(e){
       $(this).parents('#input-group-remove').remove();
    });

  // Add Soal Essay
    $("#addSoalEssay").click(function() {
       $("#form-essay").append('<div class="form-group row mb-4" id="input-group-remove"><label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="soal_essay">2.</label><div class="col-sm-11 col-md-7"><input type="hidden" name="mapel_id" value="<?= $data->mapel_id ?>" readonly><input type="text" class="form-control" name="soal_essay[]" id="soal_essay" placeholder="Soal Essay" autocomplete="off" value="<?= set_value('soal_essay') ?>" required autofocus><small class="text-danger"><i><?= form_error('soal_essay') ?></i></small></div><div class="col-lg-1"><i class="fas fa-minus" id="removeSoalEssay" style="cursor: pointer;"></i></div></div>');
    });

  // Remove Soal Essay
    $("body").on("click","#removeSoalEssay",function(e){
       $(this).parents('#input-group-remove').remove();
    });

  });
</script>
