<?php if (!$this->session->userdata('nip')) {
  $this->session->set_flashdata('info', 'Silahkan login terlebih dahulu!');
  Redirect('Auth/login');
}elseif (!$this->session->userdata('role') == 'pengajar') {
  $this->session->set_flashdata('info', 'Silahkan daftar sebagai pengajar terlebih dahulu!');
} ?>

<?php foreach($profilepengajar as $data) : ?>
  <div class="main-content">
    <section class="section">
      <div class="section-header">
        <h1>Profile</h1>
      </div>
      <div class="section-body">
        <h2 class="section-title">Hi, <?= $data->nama_pengajar ?></h2>
        <p class="section-lead">
          Ubah informasi tentang anda di halaman ini.
        </p>

        <div class="row mt-sm-4">
          <div class="col-12 col-md-12 col-lg-5">
            <div class="card profile-widget">
              <div class="profile-widget-header">              
                <img alt="image" width="50" height="100" src="<?= base_url('assets/img/avatar/'.$data->avatar); ?>" class="rounded-circle profile-widget-picture">
                <div class="profile-widget-items">
                  <div class="profile-widget-item">
                    <div class="profile-widget-item-label">Modul</div>
                    <div class="profile-widget-item-value">187</div>
                  </div>
                  <div class="profile-widget-item">
                    <div class="profile-widget-item-label">Pengikut</div>
                    <div class="profile-widget-item-value">6,8K</div>
                  </div>
                  <div class="profile-widget-item">
                    <div class="profile-widget-item-label">Rating</div>
                    <div class="profile-widget-item-value">2,1K</div>
                  </div>
                </div>
              </div>
              <div class="profile-widget-description">
                <div class="profile-widget-name"> <?php if($this->session->userdata('role') == 'pengajar'){echo "Pengajar";} ?> <div class="slash"></div> <div class="text-muted d-inline font-weight-normal"><?= $data->nama_mapel ?></div></div>
                <?= $data->bio ?>
              </div>
              <div class="card-footer text-center">
                <?= $this->session->userdata('email') ?>
              </div>
            </div>
          </div>
          <div class="col-12 col-md-12 col-lg-7">
            <div class="card">
              <form action="<?= base_url('Pengajar/postprofile/'.$data->nip ) ?>" method="POST" class="needs-validation" novalidate="">
                <div class="card-header">
                  <h4>Edit Profile</h4>
                </div>
                <div class="card-body">
                    <div class="row">                               
                      <div class="form-group col-12">
                        <label for="nama_pengajar">Nama Lengkap</label>
                        <input type="text" class="form-control" name="nama_pengajar" id="nama_pengajar" value="<?= $data->nama_pengajar ?>" required="" autocomplete="off">
                        <div class="invalid-feedback">
                          Tolong isi kolom nama lengkap
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <?php $data = explode('| ', $data->value_system) ?>
                      <div class="form-group col-lg-4">
                        <label for="agama">Agama</label>
                        <select name="agama" id="agama" class="form-control">
                          <?php foreach($system as $sys) : ?>
                            <?php if($sys->system_type == 'AGAMA') : ?>
                              <option value="<?= $sys->system_value_txt ?>" <?php if($sys->system_value_txt == $data[0]){echo "selected";} ?>><?= $sys->system_value_txt ?></option>
                            <?php endif; ?>
                          <?php endforeach; ?>
                        </select>
                      </div>
                      <div class="form-group col-lg-4">
                        <label for="jenis_kelamin">Jenis Kelamin</label>
                        <select name="jenis_kelamin" id="jenis_kelamin" class="form-control">
                          <?php foreach($system as $sys) : ?>
                            <?php if($sys->system_type == 'JENIS_KELAMIN') : ?>
                              <option value="<?= $sys->system_value_txt ?>" <?php if($sys->system_value_txt == $data[1]){echo "selected";} ?>><?= $sys->system_value_txt ?></option>
                            <?php endif; ?>
                          <?php endforeach; ?>
                        </select>
                      </div>
                      <div class="form-group col-lg-4">
                        <label for="status_kawin">Status Kawin</label>
                        <select name="status_kawin" id="status_kawin" class="form-control">
                          <?php foreach($system as $sys) : ?>
                            <?php if($sys->system_type == 'STATUS_KAWIN') : ?>
                              <option value="<?= $sys->system_value_txt ?>" <?php if($sys->system_value_txt == $data[2]){echo "selected";} ?>><?= $sys->system_value_txt ?></option>
                            <?php endif; ?>
                          <?php endforeach; ?>
                        </select>
                      </div>
                    </div>
                <?php foreach($profilepengajar as $data) : ?>
                    <div class="row">
                      <div class="form-group col-lg-6">
                        <label for="tanggal_lahir">Tanggal Lahir</label>
                        <input type="date" name="tanggal_lahir" id="tanggal_lahir" class="form-control" value="<?= $data->tanggal_lahir ?>">
                      </div>
                      <div class="form-group col-lg-6">
                        <label for="no_hp">No. Handphone</label>
                        <input type="number" name="no_hp" id="no_hp" class="form-control" placeholder="No. Handphone" value="<?= $data->no_hp ?>" autocomplete="off">
                      </div>
                    </div>
                    <div class="row">
                      <div class="form-group col-12">
                        <label for="alamat">Alamat</label>
                        <textarea class="form-control" name="alamat" id="alamat" placeholder="Alamat Lengkap"><?= $data->alamat ?></textarea>
                      </div>
                    </div>
                    <div class="row">
                      <div class="form-group col-12">
                        <label for="bio">Bio</label>
                        <textarea class="form-control summernote-simple" name="bio" id="bio"><?= $data->bio ?></textarea>
                      </div>
                    </div>
                <?php endforeach; ?>
                </div>
                <div class="card-footer text-right">
                  <button class="btn btn-primary">Simpan Perubahan</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
<?php endforeach ?>