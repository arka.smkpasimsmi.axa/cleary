<?php if (!$this->session->userdata('nip')) {
	$this->session->set_flashdata('info', 'Silahkan login terlebih dahulu!');
	Redirect('Auth/login');
}elseif (!$this->session->userdata('role') == 'pengajar') {
	$this->session->set_flashdata('info', 'Silahkan daftar sebagai pengajar terlebih dahulu!');
} ?>



<div class="main-content" style="min-height: 655px;">
	<section class="section">
	  <div class="section-header">
	    <h1>Aktivitas</h1>
	    <div class="section-header-breadcrumb">
	    </div>
	  </div>

	  <div class="section-body">
	    <h2 class="section-title">Aktivitas Terbaru</h2>
	    <?php $cekData = $this->db->get_where('r_nilai', ['pengajar_nip' => $this->session->userdata('nip')])->row_array(); ?>
        	<?php if($cekData) : ?>
        <?php foreach($listnilai as $data) : ?>
			<?php if($data->pengajar_nip == $this->session->userdata('nip')) : ?>
					
			    <div class="card card-primary">
			      <div class="card-header">
			        <h4><?= $data->nama_siswa ?></h4>
			      </div>
			      <hr style="margin-top: -10px; width: 25%; border-top: 1px solid #6777ef">

			      <div class="card-body" style="margin-top: -20px;">
			      	<div class="row">
			      		<div class="col-lg-3 text-lg-left">
							Judul Modul : <b><?= $data->judul_modul ?></b>
			      		</div>
			      		<div class="col-lg-3 text-lg-center">
							Mata Pelajaran : <b><?= $data->nama_mapel ?></b>
			      		</div>
			      		<div class="col-lg-3 text-lg-center">
							Tipe Modul : <b><?= $data->tipe_modul ?></b>
			      		</div>
			      		<div class="col-lg-3 text-lg-right">
							Nilai : <i style="color:  <?php if($data->nilai <= 60){echo "red";}else{echo "green";} ?> "><?= $data->nilai ?></i>
			      		</div>
			      	</div>

			      </div>
			    </div>
			    
			<?php endif; ?>
		<?php endforeach; ?>
			<?php else: ?>

				<div class="text-center">Belum ada aktivitas</div>

			<?php endif; ?>
	  </div>
	</section>
</div>