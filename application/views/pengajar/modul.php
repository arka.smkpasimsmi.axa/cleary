<?php if (!$this->session->userdata('nip')) {
  $this->session->set_flashdata('info', 'Silahkan login terlebih dahulu!');
  Redirect('Auth/login');
}elseif (!$this->session->userdata('role') == 'pengajar') {
  $this->session->set_flashdata('info', 'Silahkan daftar sebagai pengajar terlebih dahulu!');
} ?>

<?php foreach ($modul as $data) : ?>
	<div class="main-content" style="min-height: 405px;">
	  <section class="section">
	    <div class="section-header">
			<div class="section-header-back">
		      <a href="<?= base_url('Pengajar/daftarmodul') ?>" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
		    </div>
		    <h1>Modul &mdash; <?= $data->judul_modul ?></h1>
	    </div>

	    <div class="section-body">
	      <div class="card">
	        <div class="card-body">
	        	<?php if($this->uri->segment(3) == 'materi') : ?>
	              <form method="POST" action="<?= base_url('Pengajar/editmateri/'.$data->id_modul) ?>" enctype="multipart/form-data">
	              	<input type="hidden" name="materi_id" value="<?= $data->materi_id ?>" readonly hidden>
	                <div class="card">
	                  <div class="card-body">
	                  <div style="background-color: transparent; padding: 20px 25px; text-align: center;">
	                    <h4 style="font-size: 16px;">Materi</h4>
	                  </div>
	                    <div class="form-group row mb-4">
	                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="judul_materi">Judul</label>
	                      <div class="col-sm-12 col-md-7">
	                        <input type="text" class="form-control" name="judul_materi" id="judul_materi" placeholder="Judul Materi" autocomplete="off" value="<?= $data->judul_modul ?>" required autofocus>
	                        <small class="text-danger"><i><?= form_error('judul_materi') ?></i></small>
	                      </div>
	                    </div>
	                    <div class="form-group row mb-4">
	                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="nama_mapel">Mapel</label>
	                      <div class="col-sm-12 col-md-7">
	                          <input type="hidden" name="mapel_id" value="<?= $data->mapel_id ?>" readonly>
	                          <input type="text" class="form-control" name="nama_mapel" id="nama_mapel" autocomplete="off" readonly value="<?= $data->nama_mapel ?>">
	                      </div>
	                    </div>
	                    <div class="form-group row mb-4">
	                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="deskripsi_materi">Deskripsi</label>
	                      <div class="col-sm-12 col-md-7">
	                        <textarea class="summernote" name="deskripsi_materi" id="deskripsi_materi"><?= $data->deskripsi_materi ?></textarea>
	                        <small class="text-danger"><i><?= form_error('deskripsi_materi') ?></i></small>
	                      </div>
	                    </div>
	                    <div class="form-group row mb-4">
	                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
	                      <div class="col-sm-12 col-md-7">
	                        <button class="btn btn-primary" type="submit">Simpan</button>
	                      </div>
	                    </div>
	                  </div>
	                </div>
	              </form>
          		<?php endif; ?>
          		<?php if($this->uri->segment(3) == 'pilgan') : ?>
	              	<input type="hidden" name="pilgan_id" value="<?= $data->pilgan_id ?>" readonly hidden>
	                <div class="card">
	                  <div class="card-body">
	                  <div style="background-color: transparent; padding: 20px 25px; text-align: center;">
	                    <h4 style="font-size: 16px;">Pilihan Ganda</h4>
	                  </div>
	                      <div class="form-group row mb-4">
	                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="judul_pilgan">Judul</label>
	                        <div class="col-sm-12 col-md-7">
	                          <input type="text" class="form-control" name="judul_pilgan" id="judul_pilgan" placeholder="Judul Pilgan" autocomplete="off" value="<?= $data->judul_modul ?>" required autofocus>
	                          <small class="text-danger"><i><?= form_error('judul_pilgan') ?></i></small>
	                        </div>
	                      </div>
	            	<?php $no = 1 ?>
	                <?php foreach($pilgan as $pil) : ?>
	              <form method="POST" action="<?= base_url('Pengajar/editpilgan/'.$data->id_modul.'/'.$pil->id) ?>" enctype="multipart/form-data" class="form-edit">
	                      <div class="form-group row mb-1">
	                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
	                        <div class="col-sm-12 col-md-7">Soal</div>
	                      </div>
	                      <div class="form-group row mb-2">
	                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="soal_pilgan"><?= $no++ ?></label>
	                        <div class="col-sm-12 col-md-7 col-lg-7">
	                          <input type="text" class="form-control" name="soal_pilgan" id="soal_pilgan" placeholder="Soal Pilgan" autocomplete="off" value="<?= $pil->soal_pilgan ?>" required autofocus>
	                          <small class="text-danger"><i><?= form_error('soal_pilgan') ?></i></small>
	                        </div>
	                        <div class="col-sm-1">
	                            <button style="border: none; background-color: white; cursor: pointer;" type="submit"><i class="ion-edit href-confirm-edit" style="padding-right: 5px; color: #6777ef;"></i></button>
	                            <button style="border: none; background-color: white; cursor: pointer;" type="submit"><i class="ion-trash-b href-confirm-delete" style="padding-right: 5px; color: #6777ef;"></i></button>
	                            <a href="<?= base_url('Pengajar/hapuspilgan/'.$data->id_modul.'/'.$pil->id) ?>" class="href-hapus" style="display: none;"></a>
	                        </div>
	                      </div>
	                      <div class="form-group row mb-2">
	                        <input type="hidden" name="mapel_id" value="<?= $data->mapel_id ?>" readonly>
	                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
	                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-1" for="a">A.</label>
	                        <div class="col-sm-12 col-md-3 col-lg-2">
	                            <input type="text" class="form-control form-control-sm" name="a" id="a" autocomplete="off" value="<?= $pil->a ?>">
	                        </div>
	                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-1" for="c">C.</label>
	                        <div class="col-sm-12 col-md-3 col-lg-2">
	                            <input type="text" class="form-control form-control-sm" name="c" id="c" autocomplete="off" value="<?= $pil->c ?>">
	                        </div>
	                      </div>
	                      <div class="form-group row mb-2">
	                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
	                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-1" for="b">B.</label>
	                        <div class="col-sm-12 col-md-3 col-lg-2">
	                            <input type="text" class="form-control form-control-sm" name="b" id="b" autocomplete="off" value="<?= $pil->b ?>">
	                        </div>
	                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-1" for="d">D.</label>
	                        <div class="col-sm-12 col-md-3 col-lg-2">
	                            <input type="text" class="form-control form-control-sm" name="d" id="d" autocomplete="off" value="<?= $pil->d ?>">
	                        </div>
	                      </div>
	                      <div class="form-group row mb-4">
	                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="jawaban">Jawaban</label>
	                        <div class="col-sm-12 col-md-7 col-lg-7">
	                          <select name="jawaban" id="jawaban" class="form-control form-control-sm">
	                            <option value="a" id="a" <?php if($pil->jawaban == 'a'){echo "selected";} ?>>A</option>
	                            <option value="b" id="b" <?php if($pil->jawaban == 'b'){echo "selected";} ?>>B</option>
	                            <option value="c" id="c" <?php if($pil->jawaban == 'c'){echo "selected";} ?>>C</option>
	                            <option value="d" id="d" <?php if($pil->jawaban == 'd'){echo "selected";} ?>>D</option>
	                          </select>
	                        </div>
	                      </div>
	              </form>
	                <?php endforeach; ?>
	                <form action="<?= base_url('Pengajar/tambahpilgan/'.$data->id_modul) ?>" method="POST" enctype="multipart/form-data">
	                <?php foreach($pilgan as $pil) : ?>
	           			<input type="hidden" name="id_pilgan" value="<?= $pil->id_pilgan ?>" readonly hidden>
	                <?php endforeach; ?>
	                    <div id="form-pilgan">

	                    </div>
		                <div class="form-group row mb-4">
		                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="jawaban"></label>
		                    <div class="col-sm-12 col-md-7 col-lg-7">
	                    		<i class="fas fa-plus" id="addSoalPilgan" style="cursor: pointer;"></i>
		                    </div>
		                </div>
	               		<hr>
	                    <div class="form-group row mb-4">
	                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="waktu_pengerjaan">Waktu Pengerjaan</label>
	                        <div class="col-sm-12 col-md-7">
	                          <input type="text" name="waktu_pengerjaan" id="waktu_pengerjaan" class="form-control timepicker" value="<?= $data->waktu_pengerjaan ?>">
	                        </div>
	                    </div>
	                    <div class="form-group row mb-4">
	                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="batas_waktu">Batas Waktu</label>
	                        <div class="col-sm-12 col-md-7">
	                          <input type="date" name="batas_waktu" id="batas_waktu" class="form-control" value="<?= $data->batas_waktu ?>" required>
	                          <small class="text-danger"><i><?= form_error('batas_waktu') ?></i></small>
	                        </div>
	                    </div>
	                    <div class="form-group row mb-4">
	                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
	                      <div class="col-sm-12 col-md-7">
	                        <button class="btn btn-primary" type="submit">Tambah</button>
	                      </div>
	                    </div>
	                </form>
	                  </div>
	                </div>
          		<?php endif; ?>
          		<?php if($this->uri->segment(3) == 'essay') : ?>
	              	<div class="card">
	                 <div class="card-body">
	                  <div style="background-color: transparent; padding: 20px 25px; text-align: center;">
	                    <h4 style="font-size: 16px;">Essay</h4>
	                  </div>
	                  <div class="form-group row mb-4">
	                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="judul_essay">Judul</label>
	                    <div class="col-sm-12 col-md-7">
	                      <input type="text" class="form-control" name="judul_essay" id="judul_essay" placeholder="Judul Essay" autocomplete="off" value="<?= $data->judul_modul ?>" required autofocus>
	                      <small class="text-danger"><i><?= form_error('judul_essay') ?></i></small>
	                    </div>
	                  </div>
	                  <div class="form-group row mb-1">
	                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
	                    <div class="col-sm-12 col-md-7">Soal</div>
	                  </div>
	            	<?php $no = 1 ?>
	           		<?php foreach($essay as $ess) : ?>
	              <form method="POST" action="<?= base_url('Pengajar/editessay/'.$data->id_modul.'/'.$ess->id) ?>" enctype="multipart/form-data" class="form-edit">
	                    <div class="form-group row mb-4">
	                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="soal_essay"><?= $no++ ?></label>
	                      <div class="col-sm-11 col-md-7">
	                        <input type="hidden" name="mapel_id" value="<?= $data->mapel_id ?>" readonly>
	                        <input type="text" class="form-control" name="soal_essay" id="soal_essay" placeholder="Soal Essay" autocomplete="off" value="<?= $ess->soal_essay ?>" required autofocus>
	                        <small class="text-danger"><i><?= form_error('soal_essay') ?></i></small>
	                      </div>
	                      <div class="col-lg-1">
	                            <button style="border: none; background-color: white; cursor: pointer;" type="submit"><i class="ion-edit btn-confirm-edit href-confirm-edit" style="padding-right: 5px; color: #6777ef;"></i></button>
	                            <button style="border: none; background-color: white; cursor: pointer;" type="submit"><i class="ion-trash-b href-confirm-delete" style="padding-right: 5px; color: #6777ef;"></i></button>
	                            <a href="<?= base_url('Pengajar/hapusessay/'.$data->id_modul.'/'.$ess->id) ?>" class="href-hapus" style="display: none;"></a>
	                      </div>
	                    </div>
	              </form>
	           		<?php endforeach; ?>
	                <form action="<?= base_url('Pengajar/tambahessay/'.$data->id_modul) ?>" method="POST" enctype="multipart/form-data">
	                <?php foreach($essay as $ess) : ?>
	           			<input type="hidden" name="id_essay" value="<?= $ess->id_essay ?>" readonly hidden>
	                <?php endforeach; ?>
	                  <div id="form-essay">

	                  </div>
		                <div class="form-group row mb-4">
		                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="jawaban"></label>
		                    <div class="col-sm-12 col-md-7 col-lg-7">
	                    		<i class="fas fa-plus" id="addSoalEssay" style="cursor: pointer;"></i>
		                    </div>
		                </div>
	                  	<hr>
	                    <div class="form-group row mb-4">
	                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="waktu_pengerjaan">Waktu Pengerjaan</label>
	                        <div class="col-sm-12 col-md-7">
	                          <input type="text" name="waktu_pengerjaan" id="waktu_pengerjaan" class="form-control timepicker" value="<?= $data->waktu_pengerjaan ?>">
	                        </div>
	                    </div>
	                    <div class="form-group row mb-4">
	                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="batas_waktu">Batas Waktu</label>
	                        <div class="col-sm-12 col-md-7">
	                          <input type="date" name="batas_waktu" id="batas_waktu" class="form-control" value="<?= $data->batas_waktu ?>" required>
	                          <small class="text-danger"><i><?= form_error('batas_waktu') ?></i></small>
	                        </div>
	                    </div>
	                  <div class="form-group row mb-4">
	                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
	                    <div class="col-sm-12 col-md-7">
	                      <button class="btn btn-primary" type="submit">Tambah</button>
	                    </div>
	                  </div>
	                  </div>
	                </div>
          		<?php endif; ?>
	        </div>
	      </div>
	    </div>
	  </section>
	</div>
<?php endforeach; ?>

<script>
  $(document).ready(function(){
   
  // Add Soal Essay
    $("#addSoalPilgan").click(function() {
       $("#form-pilgan").append('<div id="input-group-remove"><div class="form-group row mb-2"><label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="soal_pilgan">2.</label><div class="col-sm-12 col-md-7 col-lg-7"><input type="text" class="form-control" name="soal_pilgan[]" id="soal_pilgan" placeholder="Soal Pilgan" autocomplete="off" value="<?= set_value('soal_pilgan') ?>" required autofocus><small class="text-danger"><i><?= form_error('soal_pilgan') ?></i></small></div><div class="col-sm-1"><i class="fas fa-minus" id="removeSoalPilgan" style="cursor: pointer;"></i></div></div><div class="form-group row mb-2"><label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label><label class="col-form-label text-md-right col-12 col-md-3 col-lg-1" for="a">A.</label> <div class="col-sm-12 col-md-3 col-lg-2"><input type="text" class="form-control form-control-sm" name="a[]" id="a" autocomplete="off"></div><label class="col-form-label text-md-right col-12 col-md-3 col-lg-1" for="c">C.</label><div class="col-sm-12 col-md-3 col-lg-2"><input type="text" class="form-control form-control-sm" name="c[]" id="c" autocomplete="off"></div></div><div class="form-group row mb-2"><label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label><label class="col-form-label text-md-right col-12 col-md-3 col-lg-1" for="b">B.</label><div class="col-sm-12 col-md-3 col-lg-2"><input type="text" class="form-control form-control-sm" name="b[]" id="b" autocomplete="off"></div>  <label class="col-form-label text-md-right col-12 col-md-3 col-lg-1" for="d">D.</label><div class="col-sm-12 col-md-3 col-lg-2"><input type="text" class="form-control form-control-sm" name="d[]" id="d" autocomplete="off"></div></div><div class="form-group row mb-4"><label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="Jawaban">Jawaban</label><div class="col-sm-12 col-md-7 col-lg-7"><select name="jawaban[]" id="jawaban" class="form-control form-control-sm"><option value="a" id="a">Pilih Jawaban</option><option value="a" id="a">A</option><option value="b" id="b">B</option><option value="c" id="c">C</option><option value="d" id="d">D</option></select></div></div></div>');
    });

  // Remove Soal Essay
    $("body").on("click","#removeSoalPilgan",function(e){
       $(this).parents('#input-group-remove').remove();
    });

  // Add Soal Essay
    $("#addSoalEssay").click(function() {
       $("#form-essay").append('<div class="form-group row mb-4" id="input-group-remove"><label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="soal_essay">2.</label><div class="col-sm-11 col-md-7"><input type="hidden" name="mapel_id" value="<?= $data->mapel_id ?>" readonly><input type="text" class="form-control" name="soal_essay[]" id="soal_essay" placeholder="Soal Essay" autocomplete="off" value="<?= set_value('soal_essay') ?>" required autofocus><small class="text-danger"><i><?= form_error('soal_essay') ?></i></small></div><div class="col-lg-1"><i class="fas fa-minus" id="removeSoalEssay" style="cursor: pointer;"></i></div></div>');
    });

  // Remove Soal Essay
    $("body").on("click","#removeSoalEssay",function(e){
       $(this).parents('#input-group-remove').remove();
    });

  });
</script>