<?php if (!$this->session->userdata('nip')) {
  $this->session->set_flashdata('info', 'Silahkan login terlebih dahulu!');
  Redirect('Auth/login');
}elseif (!$this->session->userdata('role') == 'pengajar') {
  $this->session->set_flashdata('info', 'Silahkan daftar sebagai pengajar terlebih dahulu!');
} ?>

<div class="main-content" style="min-height: 405px;">
  <section class="section">
    <div class="section-header">
      <h1>Daftar Modul</h1>
    </div>

    <div class="section-body">
      <h2 class="section-title">Kelola Modul</h2>
      <p class="section-lead">Kelola modul anda di halaman ini.</p>
      <div class="card">
        <div class="card-header">
          <ul class="nav nav-tabs" id="myTab" role="tablist">
          	<!-- <li class="nav-item">
              <a class="nav-link active" id="semua-tab" data-toggle="tab" href="#semua" role="tab" aria-controls="semua" aria-selected="true">Semua</a>
            </li> -->
            <li class="nav-item">
              <a class="nav-link active" id="materi-tab" data-toggle="tab" href="#materi" role="tab" aria-controls="materi" aria-selected="true">Materi</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="pilgan-tab" data-toggle="tab" href="#pilgan" role="tab" aria-controls="pilgan" aria-selected="false">Pilihan Ganda</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="essay-tab" data-toggle="tab" href="#essay" role="tab" aria-controls="essay" aria-selected="false">Essay</a>
            </li>
          </ul>
        </div>
        <div class="card-body">
            <div class="tab-content" id="myTabContent">
            	<div class="tab-pane" id="semua" role="tabpanel" aria-labelledby="semua-tab">
                <form method="POST" action="<?= base_url('Pengajar/hapusmodul') ?>" enctype="multipart/form-data" id="form-delete-semua">
                  <div class="card">
                      <div class="card-body">
  	                  	<div style="background-color: transparent; padding: 20px 25px 50px; text-align: center;">
  	                    	<h4 style="font-size: 16px;">Semua Modul</h4>
                          <button class="btn btn-primary cursor-href float-right btn-confirm-delete" id="btn-delete-semua" disabled>Delete</button>
  	                    </div>
  	                    <?php $i=1;$h=1;$j=1;$k=1;$l=1;$m=1; foreach($modul as $data) : ?>
  	                    	<?php if($data->pengajar_nip == $this->session->userdata('nip')) : ?>
                            <a href="<?= base_url('Pengajar/modul/') ?><?php if($data->tipe_modul_id == 1){echo 'materi/';}elseif($data->tipe_modul_id == 2){echo 'pilgan/';}elseif($data->tipe_modul_id == 3){echo 'essay/';} ?><?= $data->id_modul ?>" class="a-daftar-modul">
  				                    <section class="daftar-modul">
                                <input hidden type="checkbox" name="materi_id[]" class="check-idModul-semua-<?= $j++ ?>" value="<?= $data->materi_id ?>">
                                <input hidden type="checkbox" name="pilgan_id[]" class="check-idModul-semua-<?= $k++ ?>" value="<?= $data->pilgan_id ?>">
                                <input hidden type="checkbox" name="essay_id[]" class="check-idModul-semua-<?= $l++ ?>" value="<?= $data->essay_id ?>">
          								      <h1><?= $data->judul_modul ?></h1>  
                                <div class="custom-checkbox custom-control float-right">
                                  <input type="checkbox" data-checkboxes="mygroup" class="custom-control-input check-item-semua check-id-semua-<?= $m++ ?>" id="<?= $i++ ?>" value="<?= $data->id_modul; ?>" name="id_modul[]">
                                  <label for="<?= $h++ ?>" class="custom-control-label">&nbsp;</label>
                                </div>
                                <p style="float: right; margin-right: 20px;"><?= $data->nama_modul ?></p>
          								    </section>
  			                    </a>
    				              <?php endif; ?>
    		                <?php endforeach; ?>
                      </div>
                  </div>
                </form>
              </div>
              <div class="tab-pane fade show active" id="materi" role="tabpanel" aria-labelledby="materi-tab">
                <form method="POST" action="<?= base_url('Pengajar/hapusmodul') ?>" enctype="multipart/form-data" id="form-delete-materi">
                  <div class="card">
                      <div class="card-body">
  	                  	<div style="background-color: transparent; padding: 20px 25px 50px; text-align: center;">
  	                    	<h4 style="font-size: 16px;">Modul Materi</h4>
                          <button class="btn btn-primary cursor-href float-right float-right btn-confirm-delete" id="btn-delete-materi" disabled>Delete</button>
  	                    </div>
  	                    <?php $i=1;$h=1;$j=1;$k=1;$l=1;$m=1; foreach($modul as $data) : ?>
  	                    	<?php if($data->pengajar_nip == $this->session->userdata('nip')) : ?>
  		                    	<?php if($data->tipe_modul_id == 1) : ?>
  				                    <a href="<?= base_url('Pengajar/modul/materi/'.$data->id_modul) ?>" class="a-daftar-modul">
  					                    <div class="daftar-modul">
                                  <input hidden type="checkbox" name="materi_id[]" class="check-idModul-materi-<?= $j++ ?>" value="<?= $data->materi_id ?>">
                                  <input hidden type="checkbox" name="pilgan_id[]" class="check-idModul-materi-<?= $k++ ?>" value="<?= $data->pilgan_id ?>">
                                  <input hidden type="checkbox" name="essay_id[]" class="check-idModul-materi-<?= $l++ ?>" value="<?= $data->essay_id ?>">
          									      <h1><?= $data->judul_modul ?></h1>
                                  <div class="custom-checkbox custom-control float-right">
                                    <input type="checkbox" data-checkboxes="mygroup" class="custom-control-input check-item-materi check-id-materi-<?= $m++ ?>" id="materi-<?= $i++ ?>" value="<?= $data->id_modul; ?>" name="id_modul[]">
                                    <label for="materi-<?= $h++ ?>" class="custom-control-label">&nbsp;</label>
                                  </div>
          									    </div>
  				                    </a>
    				                <?php endif; ?>
    				              <?php endif; ?>
    		                <?php endforeach; ?>
                      </div>
                  </div>
                </form>
              </div>
              <div class="tab-pane fade" id="pilgan" role="tabpanel" aria-labelledby="pilgan-tab">
                <form method="POST" action="<?= base_url('Pengajar/hapusmodul') ?>" enctype="multipart/form-data" id="form-delete-pilgan">
                  <div class="card">
                    	<div class="card-body">
  	                  	<div style="background-color: transparent; padding: 20px 25px 50px; text-align: center;">
  	                    	<h4 style="font-size: 16px;">Modul Pilihan Ganda</h4>
                          <button class="btn btn-primary cursor-href float-right float-right btn-confirm-delete" id="btn-delete-pilgan" disabled>Delete</button>
  	                  	</div>
                      	<?php foreach($modul as $data) : ?>
  	                    	<?php if($data->pengajar_nip == $this->session->userdata('nip')) : ?>
  		                    	<?php if($data->tipe_modul_id == 2) : ?>
  				                    <a href="<?= base_url('Pengajar/modul/pilgan/'.$data->id_modul) ?>" class="a-daftar-modul">
  					                    <div class="daftar-modul">
                                  <input hidden type="checkbox" name="materi_id[]" class="check-idModul-pilgan-<?= $j++ ?>" value="<?= $data->materi_id ?>">
                                  <input hidden type="checkbox" name="pilgan_id[]" class="check-idModul-pilgan-<?= $k++ ?>" value="<?= $data->pilgan_id ?>">
                                  <input hidden type="checkbox" name="essay_id[]" class="check-idModul-pilgan-<?= $l++ ?>" value="<?= $data->essay_id ?>">
          									      <h1><?= $data->judul_modul ?></h1>
                                  <div class="custom-checkbox custom-control float-right">
                                    <input type="checkbox" data-checkboxes="mygroup" class="custom-control-input check-item-pilgan check-id-pilgan-<?= $m++ ?>" id="pilgan-<?= $i++ ?>" value="<?= $data->id_modul; ?>" name="id_modul[]">
                                    <label for="pilgan-<?= $h++ ?>" class="custom-control-label">&nbsp;</label>
                                  </div>
          									    </div>
  				                    </a>
    				                <?php endif; ?>
    				              <?php endif; ?>
    		                <?php endforeach; ?>
  	                </div>
  	             </div>
                </form>
              </div>
              <div class="tab-pane fade" id="essay" role="tabpanel" aria-labelledby="essay-tab">
                <form method="POST" action="<?= base_url('Pengajar/hapusmodul') ?>" enctype="multipart/form-data" id="form-delete-essay">
                  <div class="card">
                    <div class="card-body">
  		                <div style="background-color: transparent; padding: 20px 25px 50px; text-align: center;">
  		                	<h4 style="font-size: 16px;">Modul Essay</h4>
                          <button class="btn btn-primary cursor-href float-right float-right btn-confirm-delete" id="btn-delete-essay" disabled>Delete</button>
  		                </div>
  	                  	<?php foreach($modul as $data) : ?>
  	                    	<?php if($data->pengajar_nip == $this->session->userdata('nip')) : ?>
  		                    	<?php if($data->tipe_modul_id == 3) : ?>
  				                    <a href="<?= base_url('Pengajar/modul/essay/'.$data->id_modul) ?>" class="a-daftar-modul">
  					                    <div class="daftar-modul">
                                  <input hidden type="checkbox" name="materi_id[]" class="check-idModul-essay-<?= $j++ ?>" value="<?= $data->materi_id ?>">
                                  <input hidden type="checkbox" name="pilgan_id[]" class="check-idModul-essay-<?= $k++ ?>" value="<?= $data->pilgan_id ?>">
                                  <input hidden type="checkbox" name="essay_id[]" class="check-idModul-essay-<?= $l++ ?>" value="<?= $data->essay_id ?>">
          									      <h1><?= $data->judul_modul ?></h1>
                                  <div class="custom-checkbox custom-control float-right">
                                    <input type="checkbox" data-checkboxes="mygroup" class="custom-control-input check-item-essay check-id-essay-<?= $m++ ?>" id="essay-<?= $i++ ?>" value="<?= $data->id_modul; ?>" name="id_modul[]">
                                    <label for="essay-<?= $h++ ?>" class="custom-control-label">&nbsp;</label>
                                  </div>
          									    </div>
  				                    </a>
  				                <?php endif; ?>
  				            <?php endif; ?>
  		                <?php endforeach; ?>
  	                </div>
                	</div>
                </form>
              </div>
            </div>
        </div>
      </div>
    </div>
  </section>
</div>

<script>
  $(document).ready(function(){
   
  // Add Soal Essay
    $("#addSoalPilgan").click(function() {
       $("#form-pilgan").append('<div id="input-group-remove"><div class="form-group row mb-2"><label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="soal_pilgan">2.</label><div class="col-sm-12 col-md-7 col-lg-7"><input type="text" class="form-control" name="soal_pilgan[]" id="soal_pilgan" placeholder="Soal Pilgan" autocomplete="off" value="<?= set_value('soal_pilgan') ?>" required autofocus><small class="text-danger"><i><?= form_error('soal_pilgan') ?></i></small></div><div class="col-sm-1"><i class="fas fa-minus" id="removeSoalPilgan" style="cursor: pointer;"></i></div></div><div class="form-group row mb-2"><label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label><label class="col-form-label text-md-right col-12 col-md-3 col-lg-1" for="a">A.</label> <div class="col-sm-12 col-md-3 col-lg-2"><input type="text" class="form-control form-control-sm" name="a[]" id="a" autocomplete="off"></div><label class="col-form-label text-md-right col-12 col-md-3 col-lg-1" for="b">B.</label><div class="col-sm-12 col-md-3 col-lg-2"><input type="text" class="form-control form-control-sm" name="b[]" id="b" autocomplete="off"></div></div><div class="form-group row mb-2"><label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label><label class="col-form-label text-md-right col-12 col-md-3 col-lg-1" for="c">C.</label><div class="col-sm-12 col-md-3 col-lg-2"><input type="text" class="form-control form-control-sm" name="c[]" id="c" autocomplete="off"></div><label class="col-form-label text-md-right col-12 col-md-3 col-lg-1" for="d">D.</label><div class="col-sm-12 col-md-3 col-lg-2"><input type="text" class="form-control form-control-sm" name="d[]" id="d" autocomplete="off"></div></div><div class="form-group row mb-4"><label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="Jawaban">Jawaban</label><div class="col-sm-12 col-md-7 col-lg-7"><select name="jawaban[]" id="jawaban" class="form-control form-control-sm"><option value="a" id="a">Pilih Jawaban</option><option value="a" id="a">A</option><option value="b" id="b">B</option><option value="c" id="c">C</option><option value="d" id="d">D</option></select></div></div></div>');
    });

  // Remove Soal Essay
    $("body").on("click","#removeSoalPilgan",function(e){
       $(this).parents('#input-group-remove').remove();
    });

  // Add Soal Essay
    $("#addSoalEssay").click(function() {
       $("#form-essay").append('<div class="form-group row mb-4" id="input-group-remove"><label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="soal_essay">2.</label><div class="col-sm-11 col-md-7"><input type="hidden" name="mapel_id" value="<?= $data->mapel_id ?>" readonly><input type="text" class="form-control" name="soal_essay[]" id="soal_essay" placeholder="Soal Essay" autocomplete="off" value="<?= set_value('soal_essay') ?>" required autofocus><small class="text-danger"><i><?= form_error('soal_essay') ?></i></small></div><div class="col-lg-1"><i class="fas fa-minus" id="removeSoalEssay" style="cursor: pointer;"></i></div></div>');
    });

  // Remove Soal Essay
    $("body").on("click","#removeSoalEssay",function(e){
       $(this).parents('#input-group-remove').remove();
    });

    <?php $n=1;$o=1;$p=1;foreach($modul as $data) : ?> 
      $(".check-id-semua-<?= $n++; ?>").click(function(){ 
        if($(this).is(":checked")) 
          $(".check-idModul-semua-<?= $o++; ?>").prop("checked", true); 
        else
          $(".check-idModul-semua-<?= $p++; ?>").prop("checked", false);
      });
    <?php endforeach; ?>
    <?php $n=1;$o=1;$p=1;foreach($modul as $data) : ?> 
      $(".check-id-materi-<?= $n++; ?>").click(function(){ 
        if($(this).is(":checked")) 
          $(".check-idModul-materi-<?= $o++; ?>").prop("checked", true); 
        else
          $(".check-idModul-materi-<?= $p++; ?>").prop("checked", false);
      });
    <?php endforeach; ?>
    <?php $n=1;$o=1;$p=1;foreach($modul as $data) : ?> 
      $(".check-id-pilgan-<?= $n++; ?>").click(function(){ 
        if($(this).is(":checked")) 
          $(".check-idModul-pilgan-<?= $o++; ?>").prop("checked", true); 
        else
          $(".check-idModul-pilgan-<?= $p++; ?>").prop("checked", false);
      });
    <?php endforeach; ?>
    <?php $n=1;$o=1;$p=1;foreach($modul as $data) : ?> 
      $(".check-id-essay-<?= $n++; ?>").click(function(){ 
        if($(this).is(":checked")) 
          $(".check-idModul-essay-<?= $o++; ?>").prop("checked", true); 
        else
          $(".check-idModul-essay-<?= $p++; ?>").prop("checked", false);
      });
    <?php endforeach; ?>

  // Disable Button
    $('#btn-delete-semua').prop("disabled", true);
    $('#btn-delete-materi').prop("disabled", true);
    $('#btn-delete-pilgan').prop("disabled", true);
    $('#btn-delete-essay').prop("disabled", true);

  // Disable / Enable Button If Checkbox
    $('.check-item-semua').click(function() {
      if ($('.check-item-semua:checked').length == 1) {
        $('#btn-delete-semua').prop("disabled", false);
      }else if($('.check-item-semua:checked').length == 0) {
          $('#btn-delete-semua').prop("disabled", true);      }
    });
    $('.check-item-materi').click(function() {
      if ($('.check-item-materi:checked').length == 1) {
        $('#btn-delete-materi').prop("disabled", false);
      }else if($('.check-item-materi:checked').length == 0) {
          $('#btn-delete-materi').prop("disabled", true);      }
    });
    $('.check-item-pilgan').click(function() {
      if ($('.check-item-pilgan:checked').length == 1) {
        $('#btn-delete-pilgan').prop("disabled", false);
      }else if($('.check-item-pilgan:checked').length == 0) {
          $('#btn-delete-pilgan').prop("disabled", true);      }
    });
    $('.check-item-essay').click(function() {
      if ($('.check-item-essay:checked').length == 1) {
        $('#btn-delete-essay').prop("disabled", false);
      }else if($('.check-item-essay:checked').length == 0) {
          $('#btn-delete-essay').prop("disabled", true);      }
    });

  // Confirm Delete
    $('#btn-delete-semua').click(function() {
      Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.value) {
          $('#form-delete-semua').submit();
        }
      });
    });
    $('#btn-delete-materi').click(function() {
      Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.value) {
          $('#form-delete-materi').submit();
        }
      });
    });
    $('#btn-delete-pilgan').click(function() {
      Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.value) {
          $('#form-delete-pilgan').submit();
        }
      });
    });
    $('#btn-delete-essay').click(function() {
      Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.value) {
          $('#form-delete-essay').submit();
        }
      });
    });

  });
</script>
