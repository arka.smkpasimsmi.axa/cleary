<div class="main-sidebar sidebar-style-2" style="overflow: hidden; outline: currentcolor none medium;" tabindex="1">
  <aside id="sidebar-wrapper">
    <div class="sidebar-brand">
      <a href="http://localhost/exStisla/dist/index">Cleary</a>
    </div>
    <div class="sidebar-brand sidebar-brand-sm">
      <a href="http://localhost/exStisla/dist/index">Cy</a>
    </div>
    <ul class="sidebar-menu">
      <li class="menu-header">Dashboard</li>
      <li class="active">
        <a href="<?= base_url('Admin/Dashboard') ?>" class="nav-link"><i class="fas fa-fire"></i><span>Dashboard</span></a>
      </li>
      <li class="menu-header">User</li>
      <li class="dropdown">
        <a href="#" class="nav-link has-dropdown"><i class="fas fa-user"></i> <span>Pengajar</span></a>
        <ul class="dropdown-menu">
          <li class=""><a class="nav-link" href="<?= base_url('Admin/master_data/Pengajar'); ?>">List Pengajar</a></li>
          <li class=""><a class="nav-link" href="<?= base_url('Admin/master_data/Pengajar/indexPengajuan'); ?>">Pengajuan Pengajar</a></li>
        </ul>
      </li>

      <li class="dropdown ">
        <a href="#" class="nav-link has-dropdown"><i class="fas fa-user"></i> <span>Siswa</span></a>
        <ul class="dropdown-menu">
          <li class=""><a class="nav-link" href="<?= base_url('Admin/master_data/Siswa'); ?>">Daftar Siswa</a></li>
        </ul>
      </li>

      <li class="menu-header">Data</li>
      <li class="dropdown ">
        <a href="#" class="nav-link has-dropdown"><i class="fas fa-th"></i> <span>Mata Pelajaran</span></a>
        <ul class="dropdown-menu">
          <li class=""><a class="nav-link" href="<?= base_url('Admin/master_data/Mapel'); ?>">Daftar Mapel</a></li>
        </ul>
      </li>
      <li class="dropdown ">
        <a href="#" class="nav-link has-dropdown"><i class="far fa-file-alt"></i> <span>Modul</span></a>
        <ul class="dropdown-menu">
          <li class=""><a class="nav-link" href="<?= base_url('Admin/master_data/Modul'); ?>">Daftar Modul</a></li>
          <li class=""><a class="nav-link" href="http://localhost/exStisla/dist/forms_editor">Daftar Nilai Modul</a></li>
        </ul>
      </li>
    </ul>

  </aside>
</div>