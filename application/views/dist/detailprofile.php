<?php if (!$this->session->userdata('nip')) {
  $this->session->set_flashdata('info', 'Silahkan login terlebih dahulu!');
  // Redirect('Auth/login');
}elseif (!$this->session->userdata('role') == 'pengajar' || !$this->session->userdata('role') == 'siswa') {
  $this->session->set_flashdata('info', 'Silahkan daftar sebagai pengajar/siswa terlebih dahulu!');
  Redirect('Auth/login');
} ?>


<!-- Style -->
<!-- <script src="https://kit.fontawesome.com/2592256b07.js" crossorigin="anonymous"></script> -->
<script src="https://use.fontawesome.com/d2a09b67ab.js"></script>
      <style>
        .rating{
           position: absolute;
           display: flex;
           transform: rotateY(180deg);
        }
        .rating input{
          display: none;
        }
        .rating label{
          display: block;
          cursor: pointer;
          width: 20px;
        }
        .rating label:before{
          content: '\f005';
          font-family: fontAwesome;
          position: relative;
          display: block;
          font-size: 15px;
          color: #40345c;
        }
        .rating label:after{
          content: '\f005';
          font-family: fontAwesome;
          position: absolute;
          display: block;
          font-size: 15px;
          color: orange;
          top: 0;
          opacity: 0;
          transition: .5s;
        }
        .rating label:hover:after,
        .rating label:hover ~ label:after,
        .rating input:checked ~ label:after{
          opacity: 1;
        }
      </style>
<!-- End Style -->

<!-- Detail Pengajar -->

    <?php foreach($profilepengajar as $data) : ?>
  <div class="main-content">
    <section class="section">
      <div class="section-header">
      <div class="section-header-back">
		      <a href="<?= base_url('Siswa') ?>" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
		    </div>
        <h1>Detail Profile</h1>
      </div>
      <div class="section-body">
        <div class="row mt-sm-4">
          <div class="col-12 col-md-12 col-lg-5">
            <div class="card profile-widget">
              <div class="profile-widget-header">              
                <img alt="image" width="50" height="100" src="<?= base_url('assets/img/avatar/'.$data->avatar); ?>" class="rounded-circle profile-widget-picture">
                <div class="profile-widget-items">
                  <div class="profile-widget-item">
                    <div class="profile-widget-item-label">Modul</div>
                    <div class="profile-widget-item-value">187</div>
                  </div>
                  <div class="profile-widget-item">
                    <div class="profile-widget-item-label">Pengikut</div>
                    <div class="profile-widget-item-value">6,8K</div>
                  </div>
                  <div class="profile-widget-item">
                    <div class="profile-widget-item-label">Rating</div>
                    <div class="profile-widget-item-value">2,1K</div>
                  </div>
                </div>
              </div>
              <div class="profile-widget-description">
                <div class="profile-widget-name">Pengajar <div class="slash"></div> <div class="text-muted d-inline font-weight-normal"><?= $data->nama_mapel ?></div></div>
                <?= $data->bio ?>
                <div class="rating">
                  <input type="radio" name="star" id="star1" value="1" onclick="kirim"><label for="star1">
                  </label>
                  <input type="radio" name="star" id="star2" value="1" onclick="kirim"><label for="star2">
                  </label>
                  <input type="radio" name="star" id="star3" value="1" onclick="kirim"><label for="star3">
                  </label>
                  <input type="radio" name="star" id="star4" value="1" onclick="kirim"><label for="star4">
                  </label>
                  <input type="radio" name="star" id="star5" value="1" onclick="kirim"><label for="star5">
                  </label>
                </div>
                <div id="hsl"></div>
              </div>
              <div class="card-footer text-center">
                <?= $this->session->userdata('email') ?>
              </div>
            </div>
          </div>
          <div class="col-12 col-md-12 col-lg-7">
            <div class="card">
                <div class="card-header">
                  <h4>Detail Lengkap Profile</h4>
                </div>
                <div class="card-body">
                    <div class="row">                               
                      <div class="form-group col-12">
                        <label for="nama_pengajar">Nama Lengkap</label>
                        <h6><?= $data->nama_pengajar; ?></h6>
                      </div>
                    </div>
                    <div class="row">
                      <?php $data = explode('| ', $data->value_system) ?>
                      <div class="form-group col-lg-4">
                        <label for="agama">Agama</label>
                        <h6><?php print_r($data[0]); ?></h6> 
                      </div>
                      <div class="form-group col-lg-4">
                        <label for="jenis_kelamin">Jenis Kelamin</label>
                        <h6><?php print_r($data[1]); ?></h6> 
                      </div>
                      <div class="form-group col-lg-4">
                        <label for="status_kawin">Status Kawin</label>
                            <h6><?php print_r($data[2]); ?></h6>
                      </div>
                    </div>
                <?php foreach($profilepengajar as $data) : ?>
                    <div class="row">
                      <div class="form-group col-lg-4">
                        <label for="tanggal_lahir">Tanggal Lahir</label>
                            <h6><?= $data->tanggal_lahir; ?></h6>  
                      </div>
                      <div class="form-group col-lg-8">
                        <label for="no_hp">No. Handphone</label>
                        <?php foreach($profilepengajar as $data) : ?>
                            <?php if($data->no_hp == "") : ?>
                            <h5>-</h5>
                            <?php else : ?>
                            <h6><?= $data->no_hp; ?></h6>
                            <?php endif; ?>
                            <?php endforeach; ?>
                      </div>
                    </div>
                    <div class="row">
                      <div class="form-group col-12">
                        <label for="alamat">Alamat</label>
                        <h6><?= $data->alamat; ?></h6>
                      </div>
                    </div>
                <?php endforeach; ?>
                </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
<?php endforeach ?>

<!-- Detail Pengajar -->

<script>
  $(document).ready(function(){
    function kirim(){
      const bintang1 = $('star1').val
      const bintang2 = $('star2').val
      const bintang3 = $('star3').val
      const bintang4 = $('star4').val
      const bintang5 = $('star5').val

      $.ajax({
        url: '<?= base_url('Siswa/kirimrating'); ?>',
        type: 'POST',
        dataType: 'html',
        data: {'bintang1':bintang1,'bintang2':bintang2,'bintang3':bintang3,'bintang4':bintang4,'bintang5':bintang5},
        success: function(hasil) {
            $('#hsl').append(`<h5>Terimakasih atas rating anda<h5>`);
        },
        error: function(hasil){
          $('#hsl').append(`<h5>Gagal mengirim rating<h5>`);
        }
      });
    }
  });
</script>