<?php if (!$this->session->userdata('nip') && !$this->session->userdata('nis')) {
  $this->session->set_flashdata('info', 'Silahkan login terlebih dahulu!');
  Redirect('Auth/login');
}elseif (!$this->session->userdata('role') == 'pengajar' || !$this->session->userdata('role') == 'siswa') {
  $this->session->set_flashdata('info', 'Silahkan daftar sebagai pengajar/siswa terlebih dahulu!');
  Redirect('Auth/login');
} ?>

<div class="main-content" style="min-height: 562px;">
  <section class="section">
    <div class="section-header">
      <h1>Daftar Pengajar</h1>
    </div>
  </section>
  <div class="row">
    <?php foreach($profilepengajar as $list) : ?>
      <input type="hidden" name="nip" value="<?= $list->nip ?>" readonly>
      <div class="col-lg-4 col-md-4">
        <div class="card card-primary">
          <div class="row">             
              <img alt="image" width="70" height="70" src="<?= base_url('assets/img/avatar/'.$list->avatar); ?>" class="rounded-circle profile-widget-picture ml-4 mt-3"> 
              <div class="mt-4 ml-4">
                <h5><?= $list->nama_pengajar; ?></h5>
                <div class="text-muted d-inline font-weight-normal"><h6><?= $list->nama_mapel; ?></h6></div>
            </div>
          </div>
            <div class="card-header">
              <h4>4,4 <i class="ion-ios-star" style="color: orange;"></i><br>
                  <div style="font-size: 10px;font-weight: 10;"> dari 160.000 ulasan</div></h4>
              <div class="card-header-action">
                <?php if($this->session->userdata('role') == 'siswa') : ?>
                <form action="<?= base_url('Siswa/ikuti'); ?>" method="POST">
                <?php $auth = $this->db->get_where('m_siswa', ['nis' => $this->session->userdata('nis')])->row_array(); ?>
                
                <input type="hidden" name="mengikuti" value="<?= $auth['mengikuti_nip'] ?>">
                <input type="hidden" name="pengikut" value="<?= $list->pengikut_nis; ?>">
                <input type="hidden" name="pengajar_nip" value="<?= $list->nip; ?>">
                
                
                <?php $arrayNip = explode('| ', $auth['mengikuti_nip']); ?>
                <?php if (in_array($list->nip, $arrayNip)) : ?>
                  <button type="button" class="btn btn-success" disabled>Diikuti</button>
                  <!-- Dropdown -->
                  <button type="button" class="btn btn-light dropdown-toggle dropdown-toggle-split ml-1" data-toggle="dropdown" aria-expanded="false">
                      <span class="sr-only">Toggle Dropdown</span>
                  </button>
                  <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(119px, 35px, 0px); top: 0px; left: 0px; will-change: transform;">
                        <a class="dropdown-item" href="<?= base_url('Siswa/detailprofile/'); ?><?= $list->nip; ?>">Lihat Profile</a>
                        <div class="dropdown-divider"></div>
                          <?php $nis = $this->session->userdata('nis'); ?>
                          <?php $ini = $auth['mengikuti_nip']; ?>
                          <?php $ono = $list->pengikut_nis; ?>
                          <?php $replacesiswa = str_replace($list->nip, "", $ini); ?>
                          <?php $replacepengajar = str_replace($nis, "", $ono); ?>
                          <input type="hidden" name="replacesiswa" value="<?= $replacesiswa; ?>">
                          <input type="hidden" name="replacepengajar" value="<?= $replacepengajar; ?>" >
                          <input type="hidden" name="nip" value="<?= $list->nip; ?>">
                          <input type="submit" class="dropdown-item" name="stop" style="color: red;font-size:13px;" value="Berhenti Mengikuti"></input>
                  </div>
                   <!-- end Dropdown  -->
                  <?php else : ?>
                    <a href="<?= base_url('Siswa/detailprofile/'); ?><?= $list->nip; ?>"class="btn btn-success">Profil</a>
                    <input type="submit" name="ikuti" class="btn btn-primary" value="Ikuti"></input>
                    <?php endif; ?>
                  </form>
                  <?php endif ?>
                  <?php if($this->session->userdata('role') == 'pengajar') : ?>
                    <a class="btn btn-success" href="<?= base_url('Siswa/detailprofile/'); ?><?= $list->nip; ?>">Profile</a>
                  <?php endif; ?>
                </div>
            </div>
        </div>
      </div>
    <?php endforeach; ?> 
  </div>      
</div>      