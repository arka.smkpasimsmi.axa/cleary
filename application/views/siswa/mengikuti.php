<?php if(!$this->session->userdata('nis')){
  $this->session->set_flashdata('info', 'Silahkan login terlebih dahulu!');
  Redirect('Auth/login');
}elseif(!$this->session->userdata('role') == 'siswa')  {
  $this->session->set_flashdata('info', 'Silahkan daftar sebagai siswa terlebih dahulu!');
}  ?>
<div class="main-content" style="min-height: 562px;">
<section class="section">
          <div class="section-header">
            <h1>Mengikuti</h1>
          </div>
</section>
<div class="row">
  <?php foreach($pengajar as $list) : ?>
    <div class="col-md-4">
        <div class="card card-primary">
            <div class="row">       
                <img alt="image" width="70" height="70" src="<?= base_url('assets/img/avatar/'.$list->avatar); ?>" class="rounded-circle profile-widget-picture ml-4 mt-3"> 
                <div class="mt-4 ml-4">
                    <h5><?= $list->nama_pengajar; ?></h5> 
                    <!-- <div class="text-muted d-inline font-weight-normal"><h6><?= $list->nama_mapel; ?></h6></div> -->
                </div>
            </div>
                <div class="card-header">
                <h4>4,4 <i class="ion-ios-star" style="color: orange;"></i><br>
                  <div style="font-size: 10px;font-weight: 10;"> dari 160.000 ulasan</div></h4>
                    <div class="card-header-action">
                          <a href="<?= base_url('Siswa/detailprofile/'); ?><?= $list->nip; ?>"class="btn btn-success">Profil</a>
                    </div>
                  </div>
                </div> 
            </div>
            <?php endforeach; ?>             
        </div>
    </div>
</div>              