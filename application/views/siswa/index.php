<?php if(!$this->session->userdata('nis')){
  $this->session->set_flashdata('info', 'Silahkan login terlebih dahulu!');
  Redirect('Auth/login');
}elseif(!$this->session->userdata('role') == 'siswa')  {
  $this->session->set_flashdata('info', 'Silahkan daftar sebagai siswa terlebih dahulu!');
}  ?>
<div class="main-content" style="min-height: 562px;">
<section class="section">
          <div class="section-header">
            <h1>Aktivitas</h1>
          </div>

<div class="section-body">
      <h2 class="section-title">Aktivitas Terbaru</h2>


        <?php foreach($datamengikuti as $list) : ?>
          <div class="card card-primary">
            <div class="card-header">
              <h5><?= $list->judul_modul ?></h5>
            </div>

            <div class="card-body" style="margin-top: -20px;">
              <div class="row"> 
                <div class="col-lg-6">
                    <div class="text-muted d-inline font-weight-normal" style="margin-top: -15px;"><h6><?= $list->nama_pengajar; ?></h6></div> 
                </div>

                <div class="col-lg-6 text-right">
                  <a href="<?= base_url('Siswa/modul/');?><?php if($list->tipe_modul_id == 1){echo 'materi/';}elseif($list->tipe_modul_id == 2){echo 'pilgan/';}elseif($list->tipe_modul_id == 3){echo 'essay/';} ?><?= $list->id_modul ?>" class="btn btn-success">Detail Soal</a>
                </div>
              </div>

            </div>
          </div>
        <?php endforeach; ?>    

    </div>
</div>    
</section>  
  </div>
