<?php if(!$this->session->userdata('nis')){
  $this->session->set_flashdata('info', 'Silahkan login terlebih dahulu!');
  Redirect('Auth/login');
}elseif(!$this->session->userdata('role') == 'siswa')  {
    $this->session->set_flashdata('info', 'Silahkan daftar sebagai siswa terlebih dahulu!');
} ?>
<?php foreach($profilesiswa as $data) : ?>
<div class="main-content" style="min-height: 562px;">
        <section class="section">
          <div class="section-header">
            <h1>Profile</h1>
          </div>
          <div class="section-body">
            <h2 class="section-title">Hi, <?= $data->nama_siswa; ?>!</h2>
            <p class="section-lead">
              Ubah informasi tentang anda di halaman ini.
            </p>

            <div class="row mt-sm-4">
              <div class="col-12 col-md-12 col-lg-5">
                <div class="card profile-widget">
                  <div class="profile-widget-header">                     
                    <img alt="image" src="<?= base_url('assets/img/avatar/'.$data->avatar); ?>" class="rounded-circle profile-widget-picture" width="50" height="100">
                    <div class="profile-widget-items">
                      <div class="profile-widget-item">
                        <div class="profile-widget-item-label">Total Modul</div>
                        <div class="profile-widget-item-value"></div>
                      </div>
                      <div class="profile-widget-item">
                        <div class="profile-widget-item-label">Mengikuti</div>
                        <div class="profile-widget-item-value"></div>
                      </div>
                    </div>
                  </div>
                  <div class="profile-widget-description">
                    <div class="profile-widget-name"><?php if($this->session->userdata('role')== 'siswa'){echo "Siswa";} ?></div>
                     <?= $data->bio; ?>
                  </div>
                  <div class="card-footer text-center">
                      <?= $this->session->userdata('email') ?>
                  </div>
                </div>
              </div>
              <div class="col-12 col-md-12 col-lg-7">
                <div class="card">
                  <form method="post" class="needs-validation" novalidate="" action="<?= base_url('Siswa/postprofile/') ?><?= $data->nis; ?>">
                    <div class="card-header">
                      <h4>Edit Profile</h4>
                    </div>
                    <div class="card-body">
                        <div class="row">                               
                          <div class="form-group col-6">
                            <label>Nama Lengkap</label>
                            <input type="text" class="form-control" name="nama_siswa" required="" value="<?= $data->nama_siswa; ?>" placeholder="Nama Lengkap" autocomplete="off">
                            <div class="invalid-feedback">
                              Tolong isi kolom nama
                            </div>
                          </div>
                          <div class="form-group col-6">
                            <label>Tanggal Lahir</label>
                            <input type="date" class="form-control" name="tanggal_lahir" required="" value="<?= $data->tanggal_lahir; ?>">
                            <div class="invalid-feedback">
                              Tolong isi kolom Tanggal lahir
                            </div>  
                        </div>
                        </div>
                        <div class="row">
                            <?php $data = explode('| ', $data->value_system) ?>
                        <div class="form-group col-4">
                      <label for="jenis_kelamin" class="d-block">Jenis Kelamin</label>
                      <select class="form-control" id="jenis_kelamin" name="jenis_kelamin">
                      <?php foreach($system as $sys) : ?>
                          <?php if($sys->system_type == 'JENIS_KELAMIN') : ?>
                          <option value="<?= $sys->system_value_txt; ?>"  <?php if($sys->system_value_txt == $data[1]){echo 'selected';} ?>>
                            <?= $sys->system_value_txt; ?>
                        </option>
                          <?php endif; ?>
                        <?php endforeach; ?>
                      </select>
                      <small class="form-text text-danger" style="font-style: italic;"><?= form_error('jenis_kelamin'); ?></small>
                    </div>
                    <div class="form-group col-4">
                      <label for="agama" class="d-block">Agama</label>
                      <select class="form-control" id="agama" name="agama">
                      <?php foreach($system as $sys) : ?>
                          <?php if($sys->system_type == 'AGAMA') : ?>
                          <option value="<?= $sys->system_value_txt; ?>"  <?php if($sys->system_value_txt == $data[0]){echo 'selected';} ?>>
                          <?= $sys->system_value_txt; ?>
                        </option>
                          <?php endif ?>
                        <?php endforeach; ?>
                      </select>
                      <small class="form-text text-danger" style="font-style: italic;"><?= form_error('agama'); ?></small>
                    </div>
                          <div class="form-group col-4">
                          <?php foreach($profilesiswa as $data) : ?>
                            <label>No.Handphone</label>
                            <input type="number" name="no_hp" class="form-control" value="<?= $data->no_hp; ?>" placeholder="No. Handphone" autocomplete="off"> 
                          <?php endforeach; ?>
                        </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                            <?php foreach($profilesiswa as $data) : ?>
                                <label for="alamat">Alamat</label>
                                <textarea id="alamat" class="form-control" name="alamat"><?= $data->alamat; ?></textarea>
                                <small class="form-text text-danger" style="font-style: italic;"></small>
                                <div class="invalid-feedback">
                                Tolong isi kolom Alamat
                                    </div>
                            <?php endforeach; ?>        
                            </div>
                        </div>
                        <div class="row">
                          <div class="form-group col-12">
                            <label>Bio</label>
                            <textarea class="form-control summernote-simple" style="display: none;" name="bio"><?= $data->bio; ?>
                            </textarea>
                          </div>
                        </div>
                    </div>
                    <div class="card-footer text-right">
                      <button class="btn btn-primary">Simpan Perubahan</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
<?php endforeach; ?>    