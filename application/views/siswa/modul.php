<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<?php if (!$this->session->userdata('nis')) {
  $this->session->set_flashdata('info', 'Silahkan login terlebih dahulu!');
  Redirect('Auth/login');
}else {

}?>
<div class="flash-data" data-flashdata="<?php echo $this->session->flashdata('successnilai'); ?>">
  <?php if ($this->session->flashdata('successnilai')) : ?>
     <?php echo $this->session->flashdata('successnilai'); ?>
  <?php endif; ?>
</div>

<a id="href" href="<?= base_url('Siswa'); ?>"></a>

<?php foreach ($modul as $data) : ?>
	<div class="main-content" style="min-height: 405px;">
	  <section class="section">
	    <div class="section-header">
			<div class="section-header-back">
		      <a href="<?= base_url('Siswa/mulai') ?>" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
		    </div>
		    <h1>Modul &mdash; <?= $data->judul_modul ?></h1>
	    </div>

	    <div class="section-body">
	      <div class="card">
	        <div class="card-body">
	        	<?php if($this->uri->segment(3) == 'materi') : ?>
	              <form method="POST" action="<?= base_url('Pengajar/editmateri/'.$data->id_modul) ?>" enctype="multipart/form-data">
	              	<input type="hidden" name="materi_id" value="<?= $data->materi_id ?>" readonly hidden>
	                <div class="card">
	                  <div class="card-body">
	                  <div style="background-color: transparent; padding: 20px 25px; text-align: center;">
	                    <h4 style="font-size: 16px;">Materi</h4>
	                  </div>
	                    <div class="form-group row mb-4">
	                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="judul_materi">Judul</label>
	                      <div class="col-sm-12 col-md-7">
	                        <h6 style="margin-top:1.3%;"><?= $data->judul_modul ?></h6>
	                      </div>
	                    </div>
	                    <div class="form-group row mb-4">
	                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="nama_mapel">Mapel</label>
	                      <div class="col-sm-12 col-md-7">
	                          <h6 style="margin-top:1.3%;"><?= $data->nama_mapel; ?></h6>
	                      </div>
	                    </div>
	                    <div class="form-group row mb-4">

						  <div class="col">
							<div class="card">
							<div class="card-body">
									<p><?= $data->deskripsi_materi ?></p>
								</div>
								</div>
							</div>
							</div>
							</div>
	                      </div>
	                    </div>
	                  </div>
	                </div>
	              </form>
          		<?php endif; ?>
          		<?php if($this->uri->segment(3) == 'pilgan') : ?>
	              	<input type="hidden" name="pilgan_id" value="<?= $data->pilgan_id ?>" readonly hidden>
	                <div class="card">
	                  <div class="card-body">
	                  <div style="background-color: transparent; padding: 20px 25px; text-align: center;">
	                    <h4 style="font-size: 16px;">Pilihan Ganda</h4>
					  </div>
					  <div class="form-group row mb-4">
	                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="judul_essay">Judul</label>
	                    <div class="col-sm-12 col-md-7">
	                      <p style=""><?= $data->judul_modul ?></p>
	                    </div>
	                  </div>
					  <div class="form-group row mb-4">
	                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="waktu_pengerjaan">Nama Pengajar</label>
	                        <div class="col-sm-12 col-md-7">
	                          <p style="margin-top:1%;"><?= $data->nama_pengajar ?></p>
	                        </div>
	                    </div> 	
						<div class="form-group row mb-4">
	                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="waktu_pengerjaan">Mata Pelajaran</label>
	                        <div class="col-sm-12 col-md-7">
	                          <p style="margin-top:1%;"><?= $data->nama_mapel ?></p>
	                        </div>
	                    </div>
					  <div class="form-group row mb-4">
	                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="waktu_pengerjaan">Waktu Pengerjaan</label>
	                        <div class="col-sm-12 col-md-7">
	                          <p style="margin-top:1%;"><?= $data->waktu_pengerjaan ?></p>
	                        </div>
	                    </div>
						<div class="form-group row mb-4">
	                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="waktu_pengerjaan">Batas Waktu</label>
	                        <div class="col-sm-12 col-md-7">
	                          <p style="margin-top:1%;"><?= $data->batas_waktu ?></p>
	                        </div>
	                    </div>
	            	<?php $no = 1 ?>
	                <?php $a=1;$b=1;$c=1;$g=1;$h=1;$i=1;$j=1;$k=1;$l=1;$m=1;$n=1;$z=1; foreach($pilgan as $pil) : ?>
				  <form action="<?= base_url('Siswa/prosesnilai/'.$pil->id_pilgan); ?>" method="POST">
				  <input type="hidden" name="id_modul" value="<?= $data->id_modul ?>" readonly hidden>
				  <input type="hidden" name="mapel_id" value="<?= $data->mapel_id ?>" readonly hidden>
	              <input type="hidden" name="pengajar_nip" value="<?= $data->pengajar_nip ?>" readonly hidden>	
	                      <div class="form-group row mb-1">
	                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
	                        <!-- <div class="col-sm-12 col-md-7">Soal</div> -->
	                      </div>
	                      <div class="form-group row mb-2">
	                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="soal_pilgan"><?= $no++ ?></label>
	                        <div class="col-sm-12 col-md-7 col-lg-7">
	                          <p style="margin-top:0.5%;"><?= $pil->soal_pilgan ?></p>
	                        </div>
	                      </div>
	                      <div class="form-group row mb-2">
	                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
							<div class="custom-control custom-radio">
								<input type="radio" id="a-<?= $g++ ?>" name="isijawaban-<?= $h++ ?>" class="custom-control-input check-a-<?= $b++ ?>" value="a">
								<label class="custom-control-label" for="a-<?= $a++ ?>">
								<b>A. </b><?= $pil->a ?></label>
								<input type="radio" name="jawaban-<?= $l++ ?>" class="box-a-<?= $c++ ?>" value="<?= $pil->jawaban ?>" hidden readonly>
							</div>
	                      </div>
	                      <div class="form-group row mb-2">
						  	<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
							<div class="custom-control custom-radio">
								<input type="radio" id="b-<?= $g++ ?>" name="isijawaban-<?= $i++ ?>" class="custom-control-input check-b-<?= $b++ ?>" value="b">
								<label class="custom-control-label" for="b-<?= $a++ ?>">
								<b>B. </b><?= $pil->b ?></label>
								<input type="radio" name="jawaban-<?= $m++ ?>" class="box-b-<?= $c++ ?>" value="<?= $pil->jawaban ?>" hidden readonly>
							</div>
	                      </div>
						  <div class="form-group row mb-2">
						  	<label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
							<div class="custom-control custom-radio">
								<input type="radio" id="c-<?= $g++ ?>" name="isijawaban-<?= $j++ ?>" class="custom-control-input check-c-<?= $b++ ?>" value="c">
								<label class="custom-control-label" for="c-<?= $a++ ?>">
								<b>C. </b><?= $pil->c ?></label>
								<input type="radio" name="jawaban-<?= $n++ ?>" class="box-c-<?= $c++ ?>" value="<?= $pil->jawaban ?>" hidden readonly>
							</div>
	                      </div>
						  <div class="form-group row mb-2">
						  <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
							<div class="custom-control custom-radio">
								<input type="radio" id="d-<?= $g++ ?>" name="isijawaban-<?= $k++ ?>" class="custom-control-input check-d-<?= $b++ ?>" value="d">
								<label class="custom-control-label" for="d-<?= $a++ ?>">
								<b>D. </b><?= $pil->d ?></label>
								<input type="radio" name="jawaban-<?= $z++ ?>" class="box-d-<?= $c++ ?>" value="<?= $pil->jawaban ?>" hidden readonly>
							</div>
	                      </div>
	                <?php endforeach; ?>

	                    </div>
	                    <div class="form-group row mb-4">
	                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
	                      <div class="col-sm-12 col-md-7">
	                        <button class="btn btn-primary" type="submit">Kirim</button>
	                      </div>
	                    </div>
	                  </div>
	                </form>
	                </div>
          		<?php endif; ?>
          		<?php if($this->uri->segment(3) == 'essay') : ?>
	              	<div class="card">
	                 <div class="card-body">
						<hr>
	                  <div style="background-color: transparent; padding: 20px 25px; text-align: center;">
	                    <h4 style="font-size: 16px;">Essay</h4>
	                  </div>
	                  <div class="form-group row mb-4">
	                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="judul_essay">Judul</label>
	                    <div class="col-sm-12 col-md-7">
	                      <p style=""><?= $data->judul_modul ?></p>
	                    </div>
	                  </div>
					  <div class="form-group row mb-4">
	                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="waktu_pengerjaan">Waktu Pengerjaan</label>
	                        <div class="col-sm-12 col-md-7">
	                          <p style="margin-top:1%;"><?= $data->waktu_pengerjaan ?></p>
	                        </div>
	                    </div>
	                    <div class="form-group row mb-4">
	                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="batas_waktu">Batas Waktu</label>
	                        <div class="col-sm-12 col-md-7">
							<p style="margin-top:1%;"><?= $data->batas_waktu ?></p>
	                        </div>
	                    </div>
	                  <div class="form-group row mb-1">
	                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
	                  </div>
	            	<?php $no = 1 ?>
	                  <div id="form-essay">
	           		<?php foreach($essay as $ess) : ?>
	              <form method="POST" action="<?= base_url('Siswa/postessay/') ?>" enctype="multipart/form-data">
	                    <div class="form-group row mb-4">
	                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="soal_essay"><?= $no++ ?></label>
	                      <div class="col-sm-11 col-md-7">
	                        <input type="hidden" name="mapel_id" value="<?= $data->mapel_id ?>" readonly>
	                        <p style="margin-top:0.5%;"><?= $ess->soal_essay ?></p>
							<textarea class="form-control" name="jawaban[]" placeholder="Jawaban"></textarea>
	                      </div>
	                      <div class="col-lg-1">
	                      </div>
	                    </div>
	           		<?php endforeach; ?>
	                  </div>
		                <div class="form-group row mb-4">
		                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3" for="jawaban"></label>
		                    <div class="col-sm-12 col-md-7 col-lg-7">
	                    		
		                    </div>
		                </div>
	                  <div class="form-group row mb-4">
	                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
	                    <div class="col-sm-12 col-md-7">
	                      <button class="btn btn-primary" type="submit">Kirim</button>
	                    </div>
	              </form>
	                  </div>
	                  </div>
	                </div>
          		<?php endif; ?>
	        </div>
	      </div>
	    </div>
	  </section>
	</div>
<?php endforeach; ?>

<script>
  $(document).ready(function(){

	<?php $d=1;$e=1;$f=1; foreach($pilgan as $pil) : ?> 
		$(".check-a-<?=$d++ ?>").click(function(){ 
		if($(this).is(":checked"))
			$(".box-a-<?=$e++ ?>").prop("checked", true); 
		else
			$(".box-a-<?=$f++ ?>").prop("checked", false);
		});

		$(".check-b-<?=$d++ ?>").click(function(){ 
		if($(this).is(":checked"))
			$(".box-b-<?=$e++ ?>").prop("checked", true); 
		else
			$(".box-a-<?=$f++ ?>").prop("checked", false);
		});

		$(".check-c-<?=$d++ ?>").click(function(){ 
		if($(this).is(":checked"))
			$(".box-c-<?=$e++ ?>").prop("checked", true); 
		else
			$(".box-a-<?=$f++ ?>").prop("checked", false);
		});

		$(".check-d-<?=$d++ ?>").click(function(){ 
		if($(this).is(":checked"))
			$(".box-d-<?=$e++ ?>").prop("checked", true); 
		else
			$(".box-d-<?=$f++ ?>").prop("checked", false);
		});
	<?php endforeach; ?>
  //Flash Success
  const flashData = $('.flash-data').data('flashdata');

	if (flashData) {
	Swal.fire({
		icon: 'success',
		title: '' + flashData,
		confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'OK',
	}).then((result) => {
		var href = $('#href').attr('href');
          window.location = href;
	});
	}

  });
</script>
