<?php if(!$this->session->userdata('nis')){
  $this->session->set_flashdata('info', 'Silahkan login terlebih dahulu!');
  Redirect('Auth/login');
}elseif(!$this->session->userdata('role') == 'siswa')  {
  $this->session->set_flashdata('info', 'Silahkan daftar sebagai siswa terlebih dahulu!');
}  ?>
<div class="main-content" style="min-height: 562px;">
<section class="section">
          <div class="section-header">
          <div class="section-header-back">
		      <a href="http://localhost/Cleary/Siswa/mulai" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
		      </div>
            <h1>Daftar Nilai</h1>
          </div>
</section>
<div class="row">
    <?php foreach($listnilai as $list) : ?>
      <?php if($list->siswa_nis == $this->session->userdata('nis')) : ?>
          <div class="col-md-4">
            <div class="card card-primary">
              <div class="row">
              <form action="<?= base_url('Siswa/prosesnilai'); ?>" method="POST">
              <?php $auth = $this->db->get_where('m_siswa', ['nis' => $this->session->userdata('nis')])->row_array(); ?>
              <input type="hidden" name="daftar_nilai" value="<?= $auth['daftar_nilai'] ?>"> 
              </form> 
                  <div class="mt-4 ml-4">
                      <h5 class="ml-3"><?= $list->nama_mapel; ?></h5>
                      <div class="text-muted d-inline font-weight-normal"><h6 class="ml-3"><?= $list->nama_pengajar; ?></h6></div>
                  </div>
              </div>
              <div class="card-header">
                <h4>Nilai Anda <i style="color:<?php if($list->nilai <= 60){echo 'red';}else{ echo 'green';} ?>"><?= $list->nilai; ?></i><br></h4>
                  <div class="card-header-action"></div>
              </div>
              </div>
            </div>
      <?php endif; ?>    
  <?php endforeach; ?>

    </div>
</div>

