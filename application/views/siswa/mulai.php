<?php if(!$this->session->userdata('nis')){
  $this->session->set_flashdata('info', 'Silahkan login terlebih dahulu!');
  Redirect('Auth/login');
}elseif(!$this->session->userdata('role') == 'siswa')  {
    $this->session->set_flashdata('info', 'Silahkan daftar sebagai siswa terlebih dahulu!');
}  ?>

<div class="main-content" style="min-height: 405px;">
  <section class="section">
    <div class="section-header">
      <h1>Mulai</h1>
    </div>

    <div class="section-body">
      <h2 class="section-title">Daftar Modul</h2>
      <p class="section-lead">daftar semua modul.</p>
      
      <div class="card">
        <div class="card-header">
          <ul class="nav nav-tabs" id="myTab" role="tablist">
          	<!-- <li class="nav-item">
              <a class="nav-link active" id="semua-tab" data-toggle="tab" href="#semua" role="tab" aria-controls="semua" aria-selected="true">Semua</a>
            </li> -->
            <li class="nav-item">
              <a class="nav-link active" id="materi-tab" data-toggle="tab" href="#materi" role="tab" aria-controls="materi" aria-selected="true">Materi</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="pilgan-tab" data-toggle="tab" href="#pilgan" role="tab" aria-controls="pilgan" aria-selected="false">Pilihan Ganda</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="essay-tab" data-toggle="tab" href="#essay" role="tab" aria-controls="essay" aria-selected="false">Essay</a>
            </li>
          </ul>
          <div class="col-md-4"></div>
          <div class="col-md-4" >
            <form action="<?= base_url('Siswa/search'); ?>" method="GET">
            <div class="form-group mt-4">
              <div class="row text-right">
              <div class="input-group">
                  <input type="text" class="form-control" name="keyword" placeholder="Cari . . ." aria-label="">
                    <div class="input-group-append">
                      <button type="submit" class="btn btn-primary ml-2" type="button">Cari</button>
                    </div>
                </div>
            </div>
          </div>
        </form>
          </div>
        </div>
        <div class="card-body">
          <div class="tab-content" id="myTabContent">
          	<div class="tab-pane fade" id="semua" role="tabpanel" aria-labelledby="semua-tab">
                <div class="card">
                    <div class="card-body">
	                  	<div style="background-color: transparent; padding: 20px 25px; text-align: center;">
	                    	<h4 style="font-size: 16px;">Semua Modul</h4>
	                    </div>

                      <?php $nilai = $this->m_modul->getNilai(); ?>
                    <?php foreach($modul as $data) : ?> 
                         <a href="<?= base_url('Siswa/modul/');?><?php if($data->tipe_modul_id == 1){echo 'materi/';}elseif($data->tipe_modul_id == 2){echo 'pilgan/';}elseif($data->tipe_modul_id == 3){echo 'essay/';} ?><?= $data->id_modul ?>" class="a-daftar-modul disabled-href"> 
                      <?php foreach($nilai as $nil) : ?>
                        <?php if($nil->nis == $this->session->userdata('nis')) : ?>
                          <?php if($nil->modul_id == $data->id_modul) : ?>  
                            style="pointer-events: none;"
                          <?php endif; ?>
                        <?php endif; ?>    
                      <?php endforeach; ?> >
                            <div class="daftar-modul">
                                <div class="row" style="font-size: 13px;">
                                  <div class="profile-widget-name ml-3"><?= $data->nama_mapel; ?> /</div>
                                  <div class="profile-widget-name ml-1"><?= $data->nama_pengajar; ?> /</div>
                                  <div class="profile-widget-name ml-1"><?= $data->tipe_modul; ?></div>
                                </div>
                              <h1><?= $data->judul_modul; ?></h1>
                                <?php foreach($nilai as $nil) : ?>
                                  <?php if($nil->nis == $this->session->userdata('nis')) : ?>
                                    <?php if($nil->modul_id == $data->id_modul) : ?>  
                                      <i style="font-size: 13px; float: right;">Modul sudah diisi!</i>
                                    <?php endif; ?>
                                  <?php endif; ?>    
                                <?php endforeach; ?> 
        								    </div>
			                    </a>  
		                <?php endforeach; ?>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade show active" id="materi" role="tabpanel" aria-labelledby="materi-tab">
                <div class="card">
                    <div class="card-body">
	                  	<div style="background-color: transparent; padding: 20px 25px; text-align: center;">
	                    	<h4 style="font-size: 16px;">Modul Materi</h4>
	                    </div>
	                    <?php foreach($modul as $data) : ?>
		                    	<?php if($data->tipe_modul_id == 1) : ?>
                            <a href="<?= base_url('Siswa/modul/materi/'.$data->id_modul) ?>" class="a-daftar-modul" 
                              <?php foreach($nilai as $nil) : ?>
                                <?php if($nil->nis == $this->session->userdata('nis')) : ?>
                                  <?php if($nil->modul_id == $data->id_modul) : ?>  
                                    style="pointer-events: none;"
                                  <?php endif; ?>
                                <?php endif; ?>    
                              <?php endforeach; ?> >
				                    <div class="daftar-modul">
                                <div class="row" style="font-size: 13px;">
                                  <div class="profile-widget-name ml-3"><?= $data->nama_mapel; ?> /</div>
                                  <div class="profile-widget-name ml-1"><?= $data->nama_pengajar; ?></div>
                                </div>
                              <h1><?= $data->judul_modul; ?></h1>
                                <?php foreach($nilai as $nil) : ?>
                                  <?php if($nil->nis == $this->session->userdata('nis')) : ?>
                                    <?php if($nil->modul_id == $data->id_modul) : ?>  
                                      <i style="font-size: 13px; float: right;">Modul sudah diisi!</i>
                                    <?php endif; ?>
                                  <?php endif; ?>    
                                <?php endforeach; ?> 
                            </div>
			                    </a>
				                <?php endif; ?>
		                <?php endforeach; ?>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="pilgan" role="tabpanel" aria-labelledby="pilgan-tab">
                <div class="card">
                  	<div class="card-body">
	                  	<div style="background-color: transparent; padding: 20px 25px; text-align: center;">
	                    	<h4 style="font-size: 16px;">Modul Pilihan Ganda</h4>
	                  	</div>
                    	<?php foreach($modul as $data) : ?>
		                    	<?php if($data->tipe_modul_id == 2) : ?>
				                    <a href="<?= base_url('Siswa/modul/pilgan/'.$data->id_modul) ?>" class="a-daftar-modul" 
                              <?php foreach($nilai as $nil) : ?>
                                <?php if($nil->nis == $this->session->userdata('nis')) : ?>
                                  <?php if($nil->modul_id == $data->id_modul) : ?>  
                                    style="pointer-events: none;"
                                  <?php endif; ?>
                                <?php endif; ?>    
                              <?php endforeach; ?> >
				                    <div class="daftar-modul">
                                <div class="row" style="font-size: 13px;">
                                  <div class="profile-widget-name ml-3"><?= $data->nama_mapel; ?> /</div>
                                  <div class="profile-widget-name ml-1"><?= $data->nama_pengajar; ?> </div>
                                </div>
                              <h1><?= $data->judul_modul; ?></h1>
                                <?php foreach($nilai as $nil) : ?>
                                  <?php if($nil->nis == $this->session->userdata('nis')) : ?>
                                    <?php if($nil->modul_id == $data->id_modul) : ?>  
                                      <i style="font-size: 13px; float: right;">Modul sudah diisi!</i>
                                    <?php endif; ?>
                                  <?php endif; ?>    
                                <?php endforeach; ?> 
                            </div>
			                    </a>
				                <?php endif; ?>
		                <?php endforeach; ?>
	                </div>
	            </div>
            </div>
            <div class="tab-pane fade" id="essay" role="tabpanel" aria-labelledby="essay-tab">
                <div class="card">
                  <div class="card-body">
		                <div style="background-color: transparent; padding: 20px 25px; text-align: center;">
		                	<h4 style="font-size: 16px;">Modul Essay</h4>
		                </div>
	                  	<?php foreach($modul as $data) : ?>
		                    	<?php if($data->tipe_modul_id == 3) : ?>
				                    <a href="<?= base_url('Siswa/modul/essay/'.$data->id_modul) ?>" class="a-daftar-modul" 
                              <?php foreach($nilai as $nil) : ?>
                                <?php if($nil->nis == $this->session->userdata('nis')) : ?>
                                  <?php if($nil->modul_id == $data->id_modul) : ?>  
                                    style="pointer-events: none;"
                                  <?php endif; ?>
                                <?php endif; ?>    
                              <?php endforeach; ?> >
				                    <div class="daftar-modul">
                                <div class="row" style="font-size: 13px;">
                                  <div class="profile-widget-name ml-3"><?= $data->nama_mapel; ?> /</div>
                                  <div class="profile-widget-name ml-1"><?= $data->nama_pengajar; ?></div>
                                </div>
                              <h1><?= $data->judul_modul; ?></h1>
                                <?php foreach($nilai as $nil) : ?>
                                  <?php if($nil->nis == $this->session->userdata('nis')) : ?>
                                    <?php if($nil->modul_id == $data->id_modul) : ?>  
                                      <i style="font-size: 13px; float: right;">Modul sudah diisi!</i>
                                    <?php endif; ?>
                                  <?php endif; ?>    
                                <?php endforeach; ?> 
                            </div>
			                    </a>
				                <?php endif; ?>
		                <?php endforeach; ?>
	                </div>
            	</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>