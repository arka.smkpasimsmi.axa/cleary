 <?php 
  $status = $this->session->userdata('role') == 'admin';
    if(!$status)  {
    Redirect('Auth/pageNotFound');
  } ?>
 ?>

<div class="main-wrapper main-wrapper-1">
  <div class="main-content">
    <section class="section">
      <div class="section-header">
        <h1>Dashboard</h1>
      </div>
      <div class="row">
        <div class="col-lg-3">
          <div class="card card-statistic-1">
            <div class="card-icon bg-success">
              <i class="far fa-user"></i>
            </div>
            <div class="card-wrap">
              <div class="card-header">
                <h4>Pengajar Aktif</h4>
              </div>
              <div class="card-body">
                <?= $countPengajar ?>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-3">
          <div class="card card-statistic-1">
            <div class="card-icon bg-danger">
              <i class="far fa-newspaper"></i>
            </div>
            <div class="card-wrap">
              <div class="card-header">
                <h4>Total Siswa</h4>
              </div>
              <div class="card-body">
                <?= $countSiswa ?>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-3 ">
          <div class="card card-statistic-1">
            <div class="card-icon bg-warning">
              <i class="far fa-file"></i>
            </div>
            <div class="card-wrap">
              <div class="card-header">
                <h4>Total Modul</h4>
              </div>
              <div class="card-body">
                <?= $countModul ?>
              </div>
            </div>
          </div>
        </div>
        <!-- <div class="col-lg-3 col-md-6 col-sm-6 col-12">
          <div class="card card-statistic-1">
            <div class="card-icon bg-success">
              <i class="fas fa-circle"></i>
            </div>
            <div class="card-wrap">
              <div class="card-header">
                <h4>Online Users</h4>
              </div>
              <div class="card-body">
                47
              </div>
            </div>
          </div>
        </div>  -->                 
      </div>
      <div class="row">
        <div class="col-lg-8 col-md-12 col-12 col-sm-12">
          <div class="card">
            <div class="card-header">
              <h4>Pengajuan Pengajar</h4>
              <div class="card-header-action">
                <a href="<?= base_url('Admin/master_data/Pengajar/indexPengajuan') ?>" class="btn btn-primary">View All</a>
              </div>
            </div>
            <div class="card-body p-0">
              <div class="table-responsive">
                <table class="table table-striped" id="table-2">
                        <thead>
                          <tr>
                            <th>NIP</th>
                            <th>Nama</th>
                            <th>Mapel</th>
                            <th>Status Aktivasi</th>
                            <th>Email</th>
                            <th>Avatar</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                          $i = 1;
                          $h = 1;
                          foreach ($pengajar as $d): 
                          $stat = $d->status=="dikonfirmasi"; 
                            if (!$stat):
                              ?>
                          <tr>
                            <td><?= $d->nip ?></td>
                            <td><?= $d->nama_pengajar ?></td>
                            <td><?= $d->namapel ?></td>
                            <?php if ($d->status == "dikonfirmasi"): ?>
                              <td><div class="badge badge-success">Konfirmasi</div></td>
                            <?php elseif($d->status == "menunggu"): ?>
                              <td><div class="badge badge-secondary">Menunggu</div></td>
                            <?php elseif($d->status == "ditolak"): ?>
                              <td><div class="badge badge-danger">Ditolak</div></td>
                            <?php elseif($d->status == "dinonaktifkan"): ?>
                              <td><div class="badge badge-warning">Dinonaktifkan</div></td>
                            <?php endif ?>
                            <td><?= $d->email_pengajar ?></td>
                            <td>
                              <div class="text-center">
                                <img alt="no avatar" src="<?= base_url('assets/img/avatar/'.$d->avatar); ?> " class="rounded-circle" width="70" height="70" data-toggle="tooltip" title="<?= $d->nama_pengajar ?>">
                                 <div class="table-links">
                                <a href="<?= base_url('Admin/master_data/Pengajar/tampilEdit/'.$d->nip) ?>" class="text-info" target="_blank">View</a>
                              </div>
                              </div>
                            </td>
                          </tr>
                          <?php endif ?>
                          <?php endforeach; ?>
                        </tbody>
                      </table>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-12 col-12 col-sm-12">
          <div class="card">
            <div class="card-header">
              <h4>Modul Baru Pengajar</h4>
            </div>
            <div class="card-body">             
              <ul class="list-unstyled list-unstyled-border">
                <li class="media">
                  <img class="mr-3 rounded-circle" width="50" src="<?php echo base_url(); ?>assets/img/avatar/avatar-1.png" alt="avatar">
                  <div class="media-body">
                    <div class="float-right text-primary">Now</div>
                    <div class="media-title">Farhan A Mujib</div>
                    <span class="">Matematika</span>
                    <span>Pilgan</span>
                  </div>
                </li>
                <li class="media">
                  <img class="mr-3 rounded-circle" width="50" src="<?php echo base_url(); ?>assets/img/avatar/avatar-2.png" alt="avatar">
                  <div class="media-body">
                    <div class="float-right">12m</div>
                    <div class="media-title">Ujang Maman</div>
                    <span class="text-small text-muted">Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.</span>
                  </div>
                </li>
                <li class="media">
                  <img class="mr-3 rounded-circle" width="50" src="<?php echo base_url(); ?>assets/img/avatar/avatar-3.png" alt="avatar">
                  <div class="media-body">
                    <div class="float-right">17m</div>
                    <div class="media-title">Rizal Fakhri</div>
                    <span class="text-small text-muted">Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.</span>
                  </div>
                </li>
                <li class="media">
                  <img class="mr-3 rounded-circle" width="50" src="<?php echo base_url(); ?>assets/img/avatar/avatar-4.png" alt="avatar">
                  <div class="media-body">
                    <div class="float-right">21m</div>
                    <div class="media-title">Alfa Zulkarnain</div>
                    <span class="text-small text-muted">Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.</span>
                  </div>
                </li>
              </ul>
              <div class="text-center pt-1 pb-1">
                <a href="#" class="btn btn-primary btn-lg btn-round">
                  View All
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
</div>
