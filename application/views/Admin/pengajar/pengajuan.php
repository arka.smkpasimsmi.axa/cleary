 <?php 
  $status = $this->session->userdata('role') == 'admin';
    if(!$status)  {
    Redirect('Auth/pageNotFound');
  } ?>
 ?>
      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <div class="section-header">
            <h1>List Pengajuan Pengajar</h1>
            <div class="section-header-breadcrumb">
              <div class="breadcrumb-item active"><a href="#">Pengajar</a></div>
              <div class="breadcrumb-item">List Pengajuan Pengajar</div>
            </div>
          </div>

          <div class="section-body">

            <div class="row">
              <div class="col-12">
                <div class="card">
                  <div class="card-header">
                    <h4>Pengajar</h4>

                    <div class="card-header-action">
                      <div>
                        <button class="btn btn-primary" data-toggle="modal" data-target="#modal-sm">Create</button>
                        <a href="#" class="cursor-href" id="href-edit"><button class="btn btn-primary" id="btn-edit" disabled>Edit</button></a>
                        <button href="#" class="btn btn-primary cursor-href" id="btn-delete" disabled>Delete</button>
                      </div>
                    </div> 
                  </div>
                  <div class="card-body">
                    <div class="table-responsive">
                      <form method="POST" action="<?= base_url('Admin/master_data/Pengajar/deletePengajar')?>" id="form-delete"></form>
                      <table class="table table-striped" id="table-2">
                        <thead>
                          <tr>
                            <th>Aksi</th>
                            <th>NIP</th>
                            <th>Nama</th>
                            <th>Mapel</th>
                            <th>Jenis Kelamin</th>
                            <th>Status Aktivasi</th>
                            <th>Email</th>
                            <th>Avatar</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                          $i = 1;
                          $h = 1;
                          foreach ($pengajar as $d): 
                          $stat = $d->status=="dikonfirmasi"; 
                            if (!$stat):
                              ?>
                          <tr>
                            <td>
                              <div class="btn-group-vertical">
                                <a href="<?= base_url('Admin/master_data/Pengajar/pengajarAksi/'.$d->nip.'/dikonfirmasi')?>" class="btn btn-icon btn-sm btn-success" data-toggle="tooltip" data-original-title="Konfirmasi"><i class="fas fa-check-circle" ></i></a>
                                <a href="<?= base_url('Admin/master_data/Pengajar/pengajarAksi/'.$d->nip.'/ditolak')?>" class="btn btn-icon btn-sm btn-danger" data-toggle="tooltip" data-original-title="Tolak"><i class="fas fa-times-circle"></i></a>
                                <a href="<?= base_url('Admin/master_data/Pengajar/pengajarAksi/'.$d->nip.'/dinonaktifkan')?>" class="btn btn-icon btn-sm btn-warning" data-toggle="tooltip" data-original-title="Nonaktifkan"><i class="fas fa-exclamation-triangle"></i></a>
                              </div>
                            </td>
                            <td><?= $d->nip ?></td>
                            <td><?= $d->nama_pengajar ?></td>
                            <td><?= $d->namapel ?></td>
                            <?php $value = explode('|', $d->value_system); ?>
                            <td><?= $value[1] ?></td>
                            <?php if ($d->status == "dikonfirmasi"): ?>
                              <td><div class="badge badge-success">Konfirmasi</div></td>
                            <?php elseif($d->status == "menunggu"): ?>
                              <td><div class="badge badge-secondary">Menunggu</div></td>
                            <?php elseif($d->status == "ditolak"): ?>
                              <td><div class="badge badge-danger">Ditolak</div></td>
                            <?php elseif($d->status == "dinonaktifkan"): ?>
                              <td><div class="badge badge-warning">Dinonaktifkan</div></td>
                            <?php endif ?>
                            <td><?= $d->email_pengajar ?></td>
                            <td>
                              <div class="text-center">
                                <img alt="no avatar" src="<?= base_url('assets/img/avatar/'.$d->avatar); ?> " class="rounded-circle" width="70" height="70" data-toggle="tooltip" title="<?= $d->nama_pengajar ?>">
                                 <div class="table-links">
                                <a href="<?= base_url('Admin/master_data/Pengajar/tampilEdit/'.$d->nip) ?>" class="text-info" target="_blank">View</a>
                              </div>
                              </div>
                            </td>
                          </tr>
                          <?php endif ?>
                          <?php endforeach; ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>

    <div class="modal fade" id="modal-sm">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Tambah Data</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="card card-default">
              <div class="card-body">
                <form method="POST" action="<?= base_url('Admin/master_data/Pengajar/insertPengajar') ?>" enctype="multipart/form-data">
                  <div class="row">
                    <div class="form-group col-6">
                      <label for="nama_pengajar">Nama Pengajar</label>
                      <input id="nama_pengajar" type="text" class="form-control" name="nama_pengajar" autofocus="" placeholder="Nama Lengkap" autocomplete="off" autofocus value="<?= set_value('nama_pengajar') ?>">
                      <small class="text-danger"><i><?= form_error('nama_pengajar') ?></i></small>
                    </div>
                    <div class="form-group col-6">
                      <label for="nip">NIP</label>
                      <input id="nip" type="number" class="form-control" name="nip" placeholder="NIP" autocomplete="off" value="<?= set_value('nip') ?>">
                      <small class="text-danger"><i><?= form_error('nip') ?></i></small>
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="email">Email</label>
                    <input id="email" type="email" class="form-control" name="email" placeholder="Email" autocomplete="off" value="<?= set_value('email') ?>">
                      <small class="text-danger"><i><?= form_error('email') ?></i></small>
                  </div>

                  <div class="row">
                    <div class="form-group col">
                      <label for="password" class="d-block">Password</label>
                      <input id="password" type="password" class="form-control pwstrength" data-indicator="pwindicator" name="password" placeholder="Password">
                      <small class="text-danger"><i><?= form_error('password') ?></i></small>
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="alamat">Alamat</label>
                      <textarea id="alamat" class="form-control" name="alamat" placeholder="Alamat Lengkap"><?= set_value('alamat') ?></textarea>
                      <small class="text-danger"><i><?= form_error('alamat') ?></i></small>
                  </div>

                  <div class="row">
                    <div class="form-group col-6">
                      <label for="mapel_id">Mata Pelajaran</label>
                      <select class="form-control" name="mapel_id" id="mapel_id">
                        <?php foreach($mapel as $data) : ?>
                          <option value="<?= $data->id ?>"><?= $data->nama_mapel ?></option>
                        <?php endforeach; ?>
                      </select>
                      <small class="text-danger"><i><?= form_error('mapel_id') ?></i></small>
                    </div>
                    <div class="form-group col-6">
                      <label for="agama">Agama</label>
                      <select class="form-control" name="agama" id="agama">
                        <?php foreach($system as $data) : ?>
                          <?php if($data->system_type == 'AGAMA') : ?>
                            <option value="<?= $data->system_value_txt ?>"><?= $data->system_value_txt ?></option>
                          <?php endif; ?>
                        <?php endforeach; ?>
                      </select>
                      <small class="text-danger"><i><?= form_error('agama') ?></i></small>
                    </div>
                  </div>

                  <div class="row">
                    <div class="form-group col-6">
                      <label for="tanggal_lahir">Tanggal Lahir</label>
                      <input type="date" class="form-control datepicker" name="tanggal_lahir" id="tanggal_lahir" value="<?= set_value('tanggal_lahir') ?>">
                      <small class="text-danger"><i><?= form_error('tanggal_lahir') ?></i></small>
                    </div>
                    <div class="form-group col-6">
                      <label for="jenis_kelamin">Jenis Kelamin</label>
                      <select class="form-control" id="jenis_kelamin" name="jenis_kelamin">
                        <?php foreach($system as $data) : ?>
                          <?php if($data->system_type == 'JENIS_KELAMIN') : ?>
                            <option value="<?= $data->system_value_txt ?>"><?= $data->system_value_txt ?></option>
                          <?php endif; ?>
                        <?php endforeach; ?>
                      </select>
                      <small class="text-danger"><i><?= form_error('jenis_kelamin') ?></i></small>
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-6">
                      <label for="avatar">Avatar</label>
                      <input type="file" name="avatar" id="avatar" class="form-control" required>
                      <small class="text-danger"><i><?= form_error('avatar') ?></i></small>
                    </div>
                    <div class="form-group col-6">
                      <label for="status_kawin">Status Kawin</label>
                      <select class="form-control" name="status_kawin" id="status_kawin">
                        <?php foreach($system as $data) : ?>
                          <?php if($data->system_type == 'STATUS_KAWIN') : ?>
                            <option value="<?= $data->system_value_txt ?>"><?= $data->system_value_txt ?></option>
                          <?php endif; ?>
                        <?php endforeach; ?>
                      </select>
                      <small class="text-danger"><i><?= form_error('status_kawin') ?></i></small>
                    </div>
                  </div>
              </div>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Create</button>
        </div> 
        </form>
    </div>
  </div>
</div>
<script>
  $(document).ready(function(){
    
    // Redirect Edit
    $('.check-item').click(function() {
      var isChecked = $('.check-item').is(':checked'); 
      if (isChecked) {
        var id_edit = $(this).val();
        $('#href-edit').prop('href', 'Pengajar/tampilEdit/'+id_edit);
      }else if(!isChecked) {
        $('#href-edit').prop('href', '#');
      }
    });

    //tes button
    $('#btn-confirm-submit').click(function(){
      var idnih = $(this).val();
      console.log(idnih);
    })

  });
</script>
   