 <?php 
    if(!$this->session->userdata('role') == 'admin')  {
    Redirect('Auth/pageNotFound');
  } ?>
 ?>
      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <div class="section-header">
            <h1>Daftar Siswa</h1>
            <div class="section-header-breadcrumb">
              <div class="breadcrumb-item active"><a href="#">Siswa</a></div>
              <div class="breadcrumb-item">Daftar Siswa</div>
            </div>
          </div>

          <div class="section-body">

            <div class="row">
              <div class="col-12">
                <div class="card">
                  <div class="card-header">
                    <h4>Siswa</h4>
                    <div class="card-header-action">
                      <div>
                        <button class="btn btn-primary" data-toggle="modal" data-target="#modal-sm">Create</button>
                        <a href="#" class="cursor-href" id="href-edit"><button class="btn btn-primary" id="btn-edit" disabled>Edit</button></a>
                        <button href="#" class="btn btn-primary cursor-href" id="btn-delete" disabled>Delete</button>
                      </div>
                    </div>
                  </div>
                  <div class="card-body">
                    <div class="table-responsive">
                       <form method="POST" action="<?= base_url('Admin/master_data/Siswa/deleteSiswa')?>" id="form-delete"></form>
                      <table class="table table-striped" id="table-2">
                        <thead>
                          <tr>
                            <th class="text-center">
                              <div class="custom-checkbox custom-control">
                                <input type="checkbox" data-checkboxes="mygroup" data-checkbox-role="dad" class="custom-control-input check-all" id="checkbox-all">
                                <label for="checkbox-all" class="custom-control-label check-all">&nbsp;</label>
                              </div>
                            </th>
                            <th>NIS</th>
                            <th>Nama</th>
                            <th>Agama</th>
                            <th>Jenis Kelamin</th>
                            <th>Tanggal Lahir</th>
                            <th>Alamat</th>
                            <th>Avatar</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                          $i = 1;
                          $h = 1;
                          foreach ($siswa as $d): ?>
                          <tr>
                            <td>
                              <div class="custom-checkbox custom-control">
                                <input type="checkbox" data-checkboxes="mygroup" class="custom-control-input check-item" id="<?= $i++ ?>" value="<?= $d->nis; ?>" name="nis[]">
                                <label for="<?= $h++ ?>" class="custom-control-label">&nbsp;</label>
                              </div>
                            </td>
                            <td><?= $d->nis ?></td>
                            <td><?= $d->nama_siswa ?></td>
                            <?php $value = explode('|', $d->value_system); ?>
                            <td><?= $value[0] ?></td>
                            <td><?= $value[1] ?></td>
                            <td><?= $d->tanggal_lahir ?></td>
                            <td><?= $d->alamat ?></td>
                            <td>          
                              <div class="text-center">
                                <img alt="no avatar" src="<?= base_url('assets/img/avatar/'.$d->avatar); ?> " class="rounded-circle" width="70" height="70" data-toggle="tooltip" title="<?= $d->nama_siswa ?>">       
                                <div class="table-links">
                                  <a href="<?= base_url('Admin/master_data/Siswa/tampilEdit/'.$d->nis) ?>" class="text-info" target="_blank">View</a>
                                </div>
                              </div>
                            </td>
                          </tr>
                          <?php endforeach; ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>

    <div class="modal fade" id="modal-sm">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Tambah Data</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="card card-default">
              <div class="card-body">
                <form method="POST" action="<?= base_url('Admin/master_data/Siswa/insertSiswa'); ?>" enctype="multipart/form-data">
                  <div class="row">
                    <div class="form-group col-6">
                      <label for="nama">Nama Siswa</label>
                      <input autofocus id="nama" type="text" class="form-control" name="nama_siswa"  placeholder="Nama Lengkap" autocomplete="off" value="<?= set_value('nama_siswa'); ?>">
                      <small class="form-text text-danger" style="font-style: italic;"><?= form_error('nama_siswa'); ?></small>
                    </div>
                    <div class="form-group col-6">
                      <label for="nis">NIS</label>
                      <input id="nis" type="number" class="form-control" name="nis" placeholder="NIS" autocomplete="off" value="<?= set_value('nis'); ?>">
                      <small class="form-text text-danger" style="font-style: italic;"><?= form_error('nis'); ?></small>
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="email">Email</label>
                    <input id="email" type="email" class="form-control" name="email" placeholder="Alamat email" autocomplete="off" value="<?= set_value('email'); ?>"> 
                    <small class="form-text text-danger" style="font-style: italic;"><?= form_error('email'); ?></small>
                    <div class="invalid-feedback">
                    </div>
                  </div>

                  <div class="row">
                    <div class="form-group col">
                      <label for="password" class="d-block">Password</label>
                      <input id="password" type="password" class="form-control pwstrength"  name="password" placeholder="Password">
                      <small class="form-text text-danger" style="font-style: italic;"><?= form_error('password'); ?></small>
                    </div>
                  </div>

                  <div class="row">
                  <div class="form-group col-6">
                      <label for="jenis_kelamin" class="d-block">Jenis Kelamin</label>
                      <select class="form-control" id="jenis_kelamin" name="jenis_kelamin">
                      <?php foreach($system as $data) : ?>
                          <?php if($data->system_type == 'JENIS_KELAMIN') : ?>
                          <option value="<?= $data->system_value_txt; ?>  <?php if(set_value('jenis_kelamin')){echo "selected";} ?>"><?= $data->system_value_txt; ?></option>
                          <?php endif ?>
                        <?php endforeach; ?>
                      </select>
                      <small class="form-text text-danger" style="font-style: italic;"><?= form_error('jenis_kelamin'); ?></small>
                    </div>
                    <div class="form-group col-6">
                      <label for="agama" class="d-block">Agama</label>
                      <select class="form-control" id="agama" name="agama">
                        <?php foreach($system as $data) : ?>
                          <?php if($data->system_type == 'AGAMA') : ?>
                          <option value="<?= $data->system_value_txt; ?> <?php if(set_value('agama')){echo "selected";} ?>"><?= $data->system_value_txt; ?></option>
                          <?php endif ?>
                        <?php endforeach; ?>
                      </select>
                      <small class="form-text text-danger" style="font-style: italic;"><?= form_error('agama'); ?></small>
                    </div>
                  </div>  

                  <div class="form-group">
                    <label for="alamat">Alamat</label>
                    <textarea id="alamat" class="form-control" name="alamat" ><?= set_value('alamat') ?></textarea>
                    <small class="form-text text-danger" style="font-style: italic;"><?= form_error('alamat'); ?></small>
                    <div class="invalid-feedback">
                    </div>
                  </div>

                  <div class="row">
                    <div class="form-group col-6">
                      <label for="avatar">Avatar</label>
                      <input id="avatar" type="file" name="avatar" class="form-control" required>
                      <small class="form-text text-danger" style="font-style: italic;"><?= form_error('avatar'); ?></small>
                    </div>
                    <div class="form-group col-6">
                      <label for="tanggal_lahir">Tanggal Lahir</label>
                      <input id="tanggal_lahir" type="date" class="form-control datepicker" name="tanggal_lahir" placeholder="Tanggal Lahir" value="<?= set_value('tanggal_lahir'); ?>">
                      <small class="form-text text-danger" style="font-style: italic;"><?= form_error('tanggal_lahir'); ?></small>
                    </div>
                  </div>

                  <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-lg btn-block">
                      Daftar
                    </button>
                  </div>
              </div>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Create</button>
        </div> 
        </form>
    </div>
  </div>
</div>
<script>
  $(document).ready(function(){
    
    // Redirect Edit
    $('.check-item').click(function() {
      if ($('.check-item').is(':checked')) {
        var id_edit = $(this).val();
        $('#href-edit').prop('href', 'Siswa/tampilEdit/'+id_edit);
      }
    });

  });
</script>