<div class="table-responsive">
  <form method="POST" action="<?= base_url('Admin/master_data/Modul/deleteModul')?>" id="form-delete">
  <table class="table table-striped" id="table-2">
    <thead>
      <tr>
        <th>ID</th>
        <th>Judul Modul</th>
        <th>Nama Pengajar</th>
        <th>Mata Pelajaran</th>
        <th>Tipe Modul</th>
        <th>Isi Modul</th>
        <th>Aksi</th>
      </tr>
    </thead>
    <tbody>
      <?php 
      $i = 1;
      $h = 1;
      foreach ($modul as $d) : ?>
      <tr>
        <td><?= $d->id_modul ?></td>
        <td><?= $d->judul_modul ?></td>
        <td><?= $d->nama_pengajar ?></td>
        <td><?= $d->nama_mapel ?></td>
        <td><?= $d->nama_modul ?></td>
        <?php if ($d->tipe_modul_id == 1): ?>
          <td><a href="<?= base_url('Admin/master_data/Modul/tampilIsiModul/materi/'.$d->id_modul) ?>" target="_blank">View</a></td>
        <?php elseif($d->tipe_modul_id == 2): ?>
          <td><a href="<?= base_url('Admin/master_data/Modul/tampilIsiModul/pilgan/'.$d->id_modul) ?>" target="_blank">View</a></td>
        <?php elseif($d->tipe_modul_id == 3): ?>
          <td><a href="<?= base_url('Admin/master_data/Modul/tampilIsiModul/essay/'.$d->id_modul) ?>" target="_blank">View</a></td>
        <?php elseif($d->tipe_modul_id == 4): ?>
          <td><a href="<?= base_url('Admin/master_data/Modul/tampilIsiModul/essayPilgan/'.$d->materi_id) ?>" target="_blank">View</a></td>
        <?php endif ?>
        <td>
          <!-- <a class="btn btn-danger" id="delete_modul" data-id="<?= $d->materi_id ?>" href="javascript:void(0)"><i class="fa fa-trash"></i></a> -->
          <?php if ($d->tipe_modul_id == 1): ?>
            <a class="btn btn-danger" href="<?= base_url('Admin/master_data/Modul/deleteModul/materi/'.$d->materi_id.'/'.$d->id_modul) ?>"><i class="fas fa-trash"></i></a>
            <?php elseif($d->tipe_modul_id == 2): ?>
            <a class="btn btn-danger" href="<?= base_url('Admin/master_data/Modul/deleteModul/pilgan/'.$d->pilgan_id.'/'.$d->id_modul) ?>"><i class="fas fa-trash"></i></a>
            <?php elseif($d->tipe_modul_id == 3): ?>
            <a class="btn btn-danger" href="<?= base_url('Admin/master_data/Modul/deleteModul/essay/'.$d->essay_id.'/'.$d->id_modul) ?>"><i class="fas fa-trash"></i></a>
            <?php elseif($d->tipe_modul_id == 4): ?>
            <a class="btn btn-danger" href="<?= base_url('Admin/master_data/Modul/deleteModul/essayPilgan/'.$d->materi_id.'-'.$d->essay_id.'/'.$d->id_modul) ?>"><i class="fas fa-trash"></i></a>
          <?php endif ?>
        </td>
      </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
  </form>
</div>
