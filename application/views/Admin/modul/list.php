 <?php 
  $status = $this->session->userdata('role') == 'admin';
    if(!$status)  {
    Redirect('Auth/pageNotFound');
  } ?>
 ?>      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <div class="section-header">
            <h1>Daftar Modul</h1>
            <div class="section-header-breadcrumb">
              <div class="breadcrumb-item active"><a href="#">Modul</a></div>
              <div class="breadcrumb-item">Daftar Modul</div>
            </div>
          </div>

          <div class="section-body">

            <div class="row">
              <div class="col-12">
                <div class="card">
                  <div class="card-header">
                    <h4>Pengajar</h4>
                  </div>
                  <div class="card-body" id="load-modul">
                    
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
</div>
<script>
  $(document).ready(function(){

    readModul();
    
   
    $(document).on('click', '#delete_modul', function(e){

      var modulId = $(this).data('id');
      console.log(modulId);
      SwalDelete(modulId);
      e.preventDefault();
    });


    function SwalDelete(modulId){

      Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33', 
        confirmButtonText: 'Yes, delete it!',
        showLoaderOnConfirm: 'true',

        preConfirm: function(){
          return new Promise(function(resolve){
            var urlModul = base_url("Admin/master_data/Modul/deleteModul");
            console.log(urlModul)
            $.ajax({
              url: urlModul,
              type: "POST",
            })
            .done(function(response){
              Swal.fire('Deleted!', response.message, response.status);
              readModul();
            })
            .fail(function(){
              Swal.fire('Oops...','Something went wrong !','error');
            });
          });
        },
        allowOutsideClick: false
      });
    }

    function readModul() {
      $('#load-modul').load('<?= base_url("Admin/master_data/Modul/loadTableModul") ?>');
    }

  });
</script>