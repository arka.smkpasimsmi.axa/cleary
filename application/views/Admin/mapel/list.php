 <?php 
  $status = $this->session->userdata('role') == 'admin';
    if(!$status)  {
    Redirect('Auth/pageNotFound');
  } ?>
 ?>

      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <div class="section-header">
            <h1>Daftar Mata Pelajaran</h1>
            <div class="section-header-breadcrumb">
              <div class="breadcrumb-item active"><a href="#">Mata Pelajaran</a></div>
              <div class="breadcrumb-item">Daftar Mata Pelajaran</div>
            </div>
          </div>

          <div class="section-body">

            <div class="row">
              <div class="col-12">
                <div class="card">
                  <div class="card-header">
                    <h4>Mata Pelajaran</h4>
                    <div class="card-header-action">
                      <div>
                        <button class="btn btn-primary" data-toggle="modal" data-target="#modal-sm">Create</button>
                        <a href="#" class="cursor-href" id="href-edit"><button class="btn btn-primary" id="btn-edit" disabled>Edit</button></a>
                        <button href="#" class="btn btn-primary cursor-href" id="btn-delete" disabled>Delete</button>
                      </div>
                    </div>
                  </div>
                  <div class="card-body">
                    <div class="table-responsive">
                      <form method="POST" action="<?= base_url('Admin/master_data/Mapel/deleteMapel')?>" id="form-delete">
                      <table class="table table-striped" id="table-2">
                        <thead>
                          <tr>
                            <th class="text-center">
                              <div class="custom-checkbox custom-control">
                                <input type="checkbox" data-checkboxes="mygroup" data-checkbox-role="dad" class="custom-control-input check-all" id="checkbox-all">
                                <label for="checkbox-all" class="custom-control-label">&nbsp;</label>
                              </div>
                            </th>
                            <th>ID</th>
                            <th>Nama Mata Pelajaran</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                          $i = 1;
                          $h = 1;
                          foreach ($mapel as $d): ?>
                          <tr>
                            <td class="text-center">
                              <div class="custom-checkbox custom-control">
                                <input type="checkbox" data-checkboxes="mygroup" class="custom-control-input check-item" id="<?= $i++ ?>" value="<?= $d->id; ?>" name="id[]">
                                <label for="<?= $h++ ?>" class="custom-control-label">&nbsp;</label>
                              </div>
                            </td>
                            <td><?= $d->id ?></td>
                            <td><?= $d->nama_mapel ?></td>
                          </tr>
                          <?php endforeach; ?>
                        </tbody>
                      </table>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>

    <div class="modal fade" id="modal-sm">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Tambah Data</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="card card-default">
              <div class="card-body">
                <form method="POST" action="<?= base_url('Admin/master_data/Mapel/insertMapel') ?>">
                
                  <div class="form-group">
                    <label for="mapel">Nama Mata Pelajaran</label>
                      <input id="mapel" type="text" class="form-control" name="mapel" placeholder="Mata Pelajaran" autocomplete="off" value="<?= set_value('mapel') ?>">
                      <small class="text-danger"><i><?= form_error('mapel') ?></i></small>
                  </div>

        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Create</button>
        </div> 
        </form>
    </div>
  </div>
</div>
<script>
  $(document).ready(function(){
    
    // Redirect Edit
    $('.check-item').click(function() {
      var isChecked = $('.check-item').is(':checked'); 
      if (isChecked) {
        var id_edit = $(this).val();
        $('#href-edit').prop('href', 'Mapel/tampilEdit/'+id_edit);
      }else if(!isChecked) {
        $('#href-edit').prop('href', '#');
      }
    });

  });
</script>
   