 <?php 
  $status = $this->session->userdata('role') == 'admin';
    if(!$status)  {
    Redirect('Auth/pageNotFound');
  } ?>
 ?>
<?php foreach ($mapel as $d): ?>
<div class="main-content" style="min-height: 559px;">
  <section class="section">
    <div class="section-header">
        <h1>Edit Page</h1>
        <div class="section-header-breadcrumb">
          <a href="<?= base_url('Admin/master_data/Mapel') ?>" class="btn btn-info"><i class="fas fa-arrow-circle-left"></i>Back</a>
        </div>
      </div>

    <div class="section-body">
      <h2 class="section-title">Edit Mapel</h2>
      <p class="section-lead">
        Anda bisa mengganti data mapel di sini.
      </p>

      <div id="output-status"></div>
              <div class="col-md-12">
        <form action="<?= base_url('Admin/master_data/Mapel/postEdit/'.$d->id ) ?>" method="POST" id="form-edit">
          <input type="hidden" name="id" value="17">
          <div class="card" id="settings-card">
            <div class="card-header">
              <h4>Ganti Nama Mapel</h4>
            </div>
            <div class="card-body">
              <div class="form-group">
                <label for="mapel">Nama Mata Pelajaran</label>
                  <input id="mapel" type="text" class="form-control" name="mapel" placeholder="Mata Pelajaran" autocomplete="off" value="<?= $d->nama_mapel ?>">
                  <small class="text-danger"><i><?= form_error('mapel') ?></i></small>
              </div> 
            </div>
          </form>
            <div class="card-footer bg-whitesmoke text-md-right">
              <a href="<?= base_url('Admin/master_data/Mapel') ?>" class="btn btn-secondary">Batal</a>
              <button type="submit" class="btn btn-primary" id="btn-edit-submit">Simpan Perubahan</button>
            </div>
          </div>
       </div>
    </div>
  </section>
</div>
<?php endforeach ?>