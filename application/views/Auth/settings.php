<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1>Pengaturan</h1>
    </div>

    <div class="section-body">
      <h2 class="section-title">Pengaturan Akun</h2>
      <p class="section-lead">
        Anda bisa mengganti pengaturan akun di sini.
      </p>

      <div id="output-status"></div>
      <div class="row">
        <div class="col-md-4">
          <div class="card">
            <div class="card-header">
              <h4>Pengaturan Akun</h4>
            </div>
            <div class="card-body">
              <ul class="nav nav-pills flex-column">
                <li class="nav-item">
                  <a href="<?= base_url('Auth/settings/gantiemail') ?>" class="nav-link <?php if($this->uri->segment(3) == '' | $this->uri->segment(3) == 'gantiemail'){echo 'active';} ?>">Ganti Email
                  </a>
                </li>
                <li class="nav-item">
                  <a href="<?= base_url('Auth/settings/gantipassword') ?>" class="nav-link <?php if($this->uri->segment(3) == 'gantipassword'){echo 'active';} ?>">Ganti Password
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <?php if($this->uri->segment(3) == '' | $this->uri->segment(3) == 'gantiemail') : ?>
          <div class="col-md-8">
            <form id="<?= base_url('Auth/settings/gantiemail') ?>" method="POST">
              <input type="hidden" name="id" value="<?= $this->session->userdata('id') ?>">
              <div class="card" id="settings-card">
                <div class="card-header">
                  <h4>Ganti Email</h4>
                </div>
                <div class="card-body">
                  <p class="text-muted">Ganti email lama dengan email baru.</p>
                  <div class="form-group row align-items-center">
                    <label for="emailLama" class="form-control-label col-sm-3 text-md-right">Email Lama</label>
                    <div class="col-sm-6 col-md-9">
                      <?php $auth = $this->db->get_where('m_auth', ['id' => $this->session->userdata('id')])->row_array(); ?>
                      <input type="email" name="emailLama" class="form-control" id="emailLama" value="<?= $auth['email'] ?>" readonly>
                    </div>
                  </div>
                  <div class="form-group row align-items-center">
                    <label for="emailBaru" class="form-control-label col-sm-3 text-md-right">Email Baru</label>
                    <div class="col-sm-6 col-md-9">
                      <input type="email" name="emailBaru" class="form-control" id="emailBaru" autocomplete="off">
                      <small class="text-danger"><i><?= form_error('emailBaru') ?></i></small>
                    </div>
                  </div>  
                </div>
                <div class="card-footer bg-whitesmoke text-md-right">
                  <button type="submit" class="btn btn-primary" id="save-btn">Simpan Perubahan</button>
                </div>
              </div>
            </form>
          </div>
        <?php elseif($this->uri->segment(3) == 'gantipassword') : ?>
          <div class="col-md-8">
            <form id="<?= base_url('Auth/settings/gantipassword') ?>" method="POST">
              <input type="hidden" name="id" value="<?= $this->session->userdata('id') ?>">
              <div class="card" id="settings-card">
                <div class="card-header">
                  <h4>Ganti Password</h4>
                </div>
                <div class="card-body">
                  <p class="text-muted">Ganti password lama dengan password baru.</p>
                  <div class="form-group row align-items-center">
                    <label for="passwordLama" class="form-control-label col-sm-3 text-md-right">Password Lama</label>
                    <div class="col-sm-6 col-md-9">
                      <input type="password" name="passwordLama" class="form-control" id="passwordLama">
                      <small class="text-danger"><i><?= form_error('passwordLama') ?></i></small>
                      <small class="text-danger"><i><?= $this->session->flashdata('error') ?></i></small>
                    </div>
                  </div>
                  <div class="form-group row align-items-center">
                    <label for="passwordBaru" class="form-control-label col-sm-3 text-md-right">Password Baru</label>
                    <div class="col-sm-6 col-md-9">
                      <input type="password" name="passwordBaru" class="form-control" id="passwordBaru">
                      <small class="text-danger"><i><?= form_error('passwordBaru') ?></i></small>
                    </div>
                  </div>  
                </div>
                <div class="card-footer bg-whitesmoke text-md-right">
                  <button type="submit" class="btn btn-primary" id="save-btn">Simpan Perubahan</button>
                </div>
              </div>
            </form>
          </div>
        <?php endif; ?>
        <?php $this->session->flashdata('fail') ?>
      </div>
    </div>
  </section>
</div>