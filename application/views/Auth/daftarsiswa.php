<body class="sidebar-gone">
  <div id="app">
    <section class="section">
      <div class="container mt-5">
        <div class="row">
          <div class="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-8 offset-lg-2 col-xl-8 offset-xl-2">
            <div class="login-brand">
              <img src="<?= base_url('assets/img/cleary_lores.png'); ?>" alt="logo" width="100">
            </div>

            <div class="card card-primary">
              <div class="card-header"><h4>Daftar Siswa</h4></div>

              <div class="card-body">
                <form method="POST" action="<?= base_url('Auth/daftarsiswa'); ?>" enctype="multipart/form-data">
                  <div class="row">
                    <div class="form-group col-6">
                      <label for="nama">Nama Siswa</label>
                      <input autofocus id="nama" type="text" class="form-control" name="nama_siswa"  placeholder="Nama Lengkap" autocomplete="off" value="<?= set_value('nama_siswa'); ?>">
                      <small class="form-text text-danger" style="font-style: italic;"><?= form_error('nama_siswa'); ?></small>
                    </div>
                    <div class="form-group col-6">
                      <label for="nis">NIS</label>
                      <input id="nis" type="number" class="form-control" name="nis" placeholder="NIS" autocomplete="off" value="<?= set_value('nis'); ?>">
                      <small class="form-text text-danger" style="font-style: italic;"><?= form_error('nis'); ?></small>
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="email">Email</label>
                    <input id="email" type="email" class="form-control" name="email" placeholder="Alamat email" autocomplete="off" value="<?= set_value('email'); ?>"> 
                    <small class="form-text text-danger" style="font-style: italic;"><?= form_error('email'); ?></small>
                  </div>

                  <div class="row">
                    <div class="form-group col">
                      <label for="password" class="d-block">Password</label>
                      <input id="password" type="password" class="form-control pwstrength"  name="password" placeholder="Password">
                      <small class="form-text text-danger" style="font-style: italic;"><?= form_error('password'); ?></small>
                    </div>
                  </div>

                  <div class="row">
                  <div class="form-group col-6">
                      <label for="jenis_kelamin" class="d-block">Jenis Kelamin</label>
                      <select class="form-control" id="jenis_kelamin" name="jenis_kelamin">
                      <?php foreach($system as $data) : ?>
                          <?php if($data->system_type == 'JENIS_KELAMIN') : ?>
                          <option value="<?= $data->system_value_txt; ?>  <?php if(set_value('jenis_kelamin')){echo "selected";} ?>"><?= $data->system_value_txt; ?></option>
                          <?php endif ?>
                        <?php endforeach; ?>
                      </select>
                      <small class="form-text text-danger" style="font-style: italic;"><?= form_error('jenis_kelamin'); ?></small>
                    </div>
                    <div class="form-group col-6">
                      <label for="agama" class="d-block">Agama</label>
                      <select class="form-control" id="agama" name="agama">
                        <?php foreach($system as $data) : ?>
                          <?php if($data->system_type == 'AGAMA') : ?>
                          <option value="<?= $data->system_value_txt; ?> <?php if(set_value('agama')){echo "selected";} ?>"><?= $data->system_value_txt; ?></option>
                          <?php endif ?>
                        <?php endforeach; ?>
                      </select>
                      <small class="form-text text-danger" style="font-style: italic;"><?= form_error('agama'); ?></small>
                    </div>
                  </div>  

                  <div class="form-group">
                    <label for="alamat">Alamat</label>
                    <textarea id="alamat" class="form-control" name="alamat" ><?= set_value('alamat') ?></textarea>
                    <small class="form-text text-danger" style="font-style: italic;"><?= form_error('alamat'); ?></small>
                    <div class="invalid-feedback">
                    </div>
                  </div>

                  <div class="row">
                  <div class="form-group col-6">
                      <label for="avatar">Avatar</label>
                      <input type="file" name="avatar" id="avatar" class="form-control" required>
                      <small class="text-danger"><i><?= form_error('avatar') ?></i></small>
                    </div>
                    <div class="form-group col-6">
                      <label for="tanggal_lahir">Tanggal Lahir</label>
                      <input id="tanggal_lahir" type="date" class="form-control datepicker" name="tanggal_lahir" placeholder="Tanggal Lahir" value="<?= set_value('tanggal_lahir'); ?>">
                      <small class="form-text text-danger" style="font-style: italic;"><?= form_error('tanggal_lahir'); ?></small>
                    </div>
                  </div>

                  <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-lg btn-block">
                      Daftar
                    </button>
                  </div>
                </form>
              </div>
            </div>
            <div class="simple-footer">
              Copyright © Cleary 2020
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
