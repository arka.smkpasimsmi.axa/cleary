<body class="sidebar-gone">
  <div id="app">
    <section class="section">
      <div class="container mt-5">
        <div class="row">
          <div class="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-8 offset-lg-2 col-xl-8 offset-xl-2">
            <div class="login-brand">
              <img src="<?= base_url('assets/img/cleary_lores.png'); ?>" alt="logo" width="100">
            </div>

            <div class="card card-primary">
              <div class="card-header"><h4>Daftar Pengajar</h4></div>

              <div class="card-body">
                <form method="POST" action="daftarpengajar" enctype="multipart/form-data">
                  <div class="row">
                    <div class="form-group col-6">
                      <label for="nama_pengajar">Nama Pengajar</label>
                      <input id="nama_pengajar" type="text" class="form-control" name="nama_pengajar" autofocus="" placeholder="Nama Lengkap" autocomplete="off" autofocus value="<?= set_value('nama_pengajar') ?>">
                      <small class="text-danger"><i><?= form_error('nama_pengajar') ?></i></small>
                    </div>
                    <div class="form-group col-6">
                      <label for="nip">NIP</label>
                      <input id="nip" type="number" class="form-control" name="nip" placeholder="NIP" autocomplete="off" value="<?= set_value('nip') ?>">
                      <small class="text-danger"><i><?= form_error('nip') ?></i></small>
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="email">Email</label>
                    <input id="email" type="email" class="form-control" name="email" placeholder="Email" autocomplete="off" value="<?= set_value('email') ?>">
                      <small class="text-danger"><i><?= form_error('email') ?></i></small>
                  </div>

                  <div class="row">
                    <div class="form-group col">
                      <label for="password" class="d-block">Password</label>
                      <input id="password" type="password" class="form-control pwstrength" data-indicator="pwindicator" name="password" placeholder="Password">
                      <small class="text-danger"><i><?= form_error('password') ?></i></small>
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="alamat">Alamat</label>
                      <textarea id="alamat" class="form-control" name="alamat" placeholder="Alamat Lengkap"><?= set_value('alamat') ?></textarea>
                      <small class="text-danger"><i><?= form_error('alamat') ?></i></small>
                  </div>

                  <div class="row">
                    <div class="form-group col-6">
                      <label for="mapel_id">Mata Pelajaran</label>
                      <select class="form-control" name="mapel_id" id="mapel_id">
                        <?php foreach($mapel as $data) : ?>
                          <option value="<?= $data->id ?>"><?= $data->nama_mapel ?></option>
                        <?php endforeach; ?>
                      </select>
                      <small class="text-danger"><i><?= form_error('mapel_id') ?></i></small>
                    </div>
                    <div class="form-group col-6">
                      <label for="agama">Agama</label>
                      <select class="form-control" name="agama" id="agama">
                        <?php foreach($system as $data) : ?>
                          <?php if($data->system_type == 'AGAMA') : ?>
                            <option value="<?= $data->system_value_txt ?>"><?= $data->system_value_txt ?></option>
                          <?php endif; ?>
                        <?php endforeach; ?>
                      </select>
                      <small class="text-danger"><i><?= form_error('agama') ?></i></small>
                    </div>
                  </div>

                  <div class="row">
                    <div class="form-group col-6">
                      <label for="tanggal_lahir">Tanggal Lahir</label>
                      <input type="date" class="form-control datepicker" name="tanggal_lahir" id="tanggal_lahir" value="<?= set_value('tanggal_lahir') ?>">
                      <small class="text-danger"><i><?= form_error('tanggal_lahir') ?></i></small>
                    </div>
                    <div class="form-group col-6">
                      <label for="jenis_kelamin">Jenis Kelamin</label>
                      <select class="form-control" id="jenis_kelamin" name="jenis_kelamin">
                        <?php foreach($system as $data) : ?>
                          <?php if($data->system_type == 'JENIS_KELAMIN') : ?>
                            <option value="<?= $data->system_value_txt ?>"><?= $data->system_value_txt ?></option>
                          <?php endif; ?>
                        <?php endforeach; ?>
                      </select>
                      <small class="text-danger"><i><?= form_error('jenis_kelamin') ?></i></small>
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-6">
                      <label for="avatar">Avatar</label>
                      <input type="file" name="avatar" id="avatar" class="form-control" required>
                      <small class="text-danger"><i><?= form_error('avatar') ?></i></small>
                    </div>
                    <div class="form-group col-6">
                      <label for="status_kawin">Status Kawin</label>
                      <select class="form-control" name="status_kawin" id="status_kawin">
                        <?php foreach($system as $data) : ?>
                          <?php if($data->system_type == 'STATUS_KAWIN') : ?>
                            <option value="<?= $data->system_value_txt ?>"><?= $data->system_value_txt ?></option>
                          <?php endif; ?>
                        <?php endforeach; ?>
                      </select>
                      <small class="text-danger"><i><?= form_error('status_kawin') ?></i></small>
                    </div>
                  </div>

                  <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-lg btn-block">
                      Daftar
                    </button>
                  </div>
                </form>
              </div>
            </div>
            <div class="simple-footer">
              Copyright © Cleary 2020
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
