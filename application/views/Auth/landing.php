<body class="sidebar-gone">
  <div id="app">
    <section class="section">
      <div class="container mt-5">
        <div class="row">
          <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4">
            <div class="login-brand">
              <img src="<?= base_url('assets/img/cleary_lores.png'); ?>" alt="logo" width="100">
            </div>

            <div class="card card-primary mt-5">
              <div class="card-header"><h4 class="mx-auto">Daftar Sebagai</h4></div>

              <div class="card-body">
              <div class="row">
                <div class="col-6">
                    <a href="<?= base_url('Auth/daftarsiswa'); ?>" class="btn btn-info btn-lg" style="width:100px;" id="siswa"> Siswa </a>
                </div>
                <div class="col-6">
                    <a href="<?= base_url('Auth/daftarpengajar'); ?>" class="btn btn-success btn-lg ml-4" style="width:100px;" id="pengajar">Pengajar</a>
                </div>
              </div>
              </div>
            </div>
            <div class="mt-5 text-muted text-center">
              Sudah punya akun? <a href="<?= base_url('Auth/login'); ?>">Login sekarang</a>
            </div>
            <div class="simple-footer" style="margin-top: 190px;">
              Copyright © Cleary 2020
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
