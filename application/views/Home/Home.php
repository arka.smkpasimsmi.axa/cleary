<?= $this->session->userdata('email') ?>
<nav class="navbar navbar-reverse navbar-expand-lg">
        <div class="container">
        <img src="assets/img/cleary_miring.png" alt="logo" width="50"> 
            <a class="navbar-brand smooth" href="<?= base_url(); ?>">Cleary</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fas fa-bars"></i>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto ml-lg-3 align-items-lg-center">
                    <li class="nav-item"><a href="<?= base_url('Home/Panduan'); ?>" class="nav-link">Panduan Penggunaan</a></li>
                    <li class="nav-item"><a href="<?= base_url('Auth/landing'); ?>" class="nav-link">Daftar</a></li>
                </ul>
                <ul class="navbar-nav ml-auto align-items-lg-center d-none d-lg-block">
                    <li class="ml-lg-3 nav-item">
                        <a href="<?= base_url('Auth/login'); ?>" class="btn btn-round smooth btn-icon icon-left" target="_blank">
                            <i class="far fa-user"></i> Login
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

        <div class="hero-wrapper" id="top"> 
        <div class="hero">
            <div class="container">
                <div class="text text-center text-lg-left">
                    <a href="#" class="headline">
                        <div class="badge badge-danger">baru</div>
                        Cleary sekarang 100% gratis &nbsp; <i class="fas fa-chevron-right"></i>
                    </a>
                    <h1>Untuk setiap siswa,     setiap kelas & Hasil yang memuaskan.</h1>
                    <p class="lead">
                        Cleary memiliki misi untuk menyediakan pembelajaran gratis bagi siapa saja,kapan saja,dan di mana saja.
                    </p>
                    <div class="cta">
                        <a class="btn btn-lg btn-warning btn-icon icon-right" href="<?= base_url('Auth/landing'); ?>">Mulai Belajar <i class="fas fa-chevron-right"></i></a> &nbsp;
                        <div class="mt-3 text-job">
                            &nbsp;&nbsp;•&nbsp;&nbsp; Version: 0.0.1
                        </div>
                    </div>
                </div>
                <div class="image d-none d-lg-block">
                    <img src="<?= base_url('assets/img/ill.svg'); ?>" alt="img">
                </div>
            </div>
        </div>
    </div>
    <div class="callout container">
        <div class="row">
            <div class="col-md-6 col-12 mb-4 mb-lg-0">
                <div class="text-job text-muted text-14">mengapa harus menggunakan Cleary</div>
                <div class="h1 mb-0 font-weight-bold"><span class="font-weight-500">tempat belajar </span>terpercaya</div>
            </div>
            <div class="col-4 col-md-2 text-center">
                <div class="h2 font-weight-bold">1000+</div>
                <div class="text-uppercase font-weight-bold ls-2 text-primary">Unduhan</div>
            </div>
            <div class="col-4 col-md-2 text-center">
                <div class="h2 font-weight-bold">20+</div>
                <div class="text-uppercase font-weight-bold ls-2 text-primary">Negara</div>
            </div>
            <div class="col-4 col-md-2 text-center">
                <div class="h2 font-weight-bold">8500+</div>
                <div class="text-uppercase font-weight-bold ls-2 text-primary">Pengguna</div>
            </div>
        </div>
    </div>
    <section id="features">
        <div class="container">
            <div class="row mb-5 text-center">
                <div class="col-lg-10 offset-lg-1">
                    <h2> <span class="text-primary">Cleary hadir untuk para</span> guru dan siswa</h2>
                    <p class="lead">Diintegrasikan dengan komponen update untuk pengalaman yang terbaik.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="features">
                        <div class="feature">
                            <div class="feature-icon">
                                <i class="fas fa-mobile-alt mt-3"></i>
                            </div>
                            <h5>Desain Responsive</h5>
                            <p>Cleary menerapkan responsive design yang memungkinkan penggunaan di gadget maupun desktop.</p>
                        </div>
                        <div class="feature">
                            <div class="feature-icon">
                                <i class="fas fa-check mt-3"></i>
                            </div>
                            <h5>Konten Terpercaya</h5>
                            <p>Cleary mencakup mata pelajaran matematika, sains, dan banyak lagi & Selalu gratis para untuk siswa dan guru.</p>
                        </div>
                        <div class="feature">
                            <div class="feature-icon">
                                <i class="fas fa-fire mt-3"></i>
                            </div>
                            <h5>Waktu Belajar Efektif</h5>
                            <p>Siswa dapat berlatih dengan langkah mereka sendiri, Cleary bisa diakses saat disekolah maupun dirumah.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="design" class="section-design">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 d-none d-lg-block">
                    <img src="<?= base_url('assets/img/undraw_processing_qj6a.svg'); ?>" alt="user flow" class="img-fluid">
                </div>
                <div class="col-lg-7 pl-lg-5 col-md-12">
                    <div class="badge badge-primary mb-3">Penghematan total</div>
                    <h2>Simpan waktumu untuk <span class="text-primary">pembelajaran di Cleary</span><span class="text-primary"></span></h2>
                    <p class="lead">penghematan total karena Cleary 100% gratis, bisa digunakan kapanpun dan dimanapun kamu berada.</p>
                    <div class="mt-4">
                        <a href="<?= base_url('Auth/landing'); ?>" class="link-icon">
                            Mulai menggunakan Cleary <i class="fas fa-chevron-right"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="dashboard" class="section-skew">
        <div class="container">
            <div class="row mb-5 text-center">
                <div class="col-lg-10 offset-lg-1">
                    <h2>Kamu bisa belajar <span class="text-primary">apapun</span></h2>
                    <p class="lead">Bangun pemahaman yang mendalam dan solid dalam matematika, sains, dan banyak lagi.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <img src="<?= base_url('assets/img/web_search.png'); ?>" alt="user flow" class="img-fluid rounded dark-shadow" width="300" height="500">
                </div>
                <div class="col-md-4">
                    <img src="<?= base_url('assets/img/undraw_design_notes_8dmv.png'); ?>" alt="user flow" class="img-fluid rounded dark-shadow" width="300" height="500">
                </div>
                <div class="col-md-4">
                    <img src="<?= base_url('assets/img/undraw_mobile_devices_k1ok.png'); ?>" alt="user flow" class="img-fluid rounded dark-shadow" width="300" height="500">
                </div>
            </div>
            <div class="row mt-4">
                <div class="col-sm-12 col-lg-8 offset-lg-2 text-center">
                </div>
            </div>
        </div>
    </section>

    <section id="components" class="section-design section-design-right">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 pr-lg-5 pr-0">
                    <div class="badge badge-primary mb-3">Tampilan menarik</div>
                    <h2>Fokus pada <span class="text-primary">tujuanmu</span>, biarkan <span class="text-primary">Cleary</span> melakukannya untukmu <span class="text-primary"></span> </h2>
                    <p class="lead">Fokus saja pada tujuanmu karena Cleary akan membantumu semaksimal mungkin agar pembelajaranmu bisa seefektif mungkin.</p>
                    <div class="mt-4">
                        <a href="<?= base_url('Auth/login'); ?>" class="link-icon">
                            Telusuri Pembelajaran <i class="fas fa-chevron-right"></i>
                        </a>
                    </div>
                </div>
                <div class="col-lg-5 d-none d-lg-block">
                <div class="pre-block">
                    <div class="abs-images">
                        <img src="<?= base_url('assets/img/undraw_mobile_messages_u848.png'); ?>" alt="user flow" class="img-fluid rounded dark-shadow">
                        <img src="<?= base_url('assets/img/undraw_season_change_f99v.png'); ?>" alt="user flow" class="img-fluid rounded dark-shadow">
                        <img src="<?= base_url('assets/img/undraw_mobile_development_8gyo.png'); ?>" alt="user flow" class="img-fluid rounded dark-shadow">
                    </div>
                </div>
                </div>
            </div>
        </div>
    </section>

    <section id="try" class="section-dark">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 offset-lg-2 text-center">
                    <h2>Ingin mencobanya?</h2>
                    <p class="lead">Merupakan sebuah kesenangan bagi kami jika anda ingin mencoba fitur fitur yang disediakan di Cleary.</p>
                    <div class="mt-4">
                        <a href="<?= base_url('Auth/login'); ?>" class="btn">Ke Halaman Login</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="support-us" class="support-us">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 d-none d-lg-block pr-lg-5 pr-sm-0">
                    <div class="d-flex align-items-center h-100 justify-content-center abs-images-2">
                        <img src="<?= base_url('assets/img/undraw_browsing_urt9.png'); ?>" alt="image" class="img-fluid">
                    </div>
                </div>
                <div class="col-lg-4 col-md-12">
                    <h2>Cleary gratis untuk semua orang <span class="text-primary">Termasuk Kamu!</span></h2>
                    <p class="lead">jangan khawatir soal biaya karena Cleary 100% gratis!.</p>
                    <ul class="list-icons">
                        <li>
                            <span class="card-icon bg-primary text-white">
                                <i class="fas fa-box-open"></i>
                            </span>
                            <span>Dipenuhi fitur terbaru untuk menunjang pembelajaran efektif</span>
                        </li>
                        <li>
                            <span class="card-icon bg-primary text-white">
                                <i class="fas fa-stopwatch"></i>
                            </span>
                            <span>Waktu pembelajaran yang efektif.</span>
                        </li>
                        <li>
                            <span class="card-icon bg-primary text-white">
                                <i class="fas fa-heart"></i>
                            </span>
                            <span>Support dari team Cleary.</span>
                        </li>
                        <li>
                            <span class="card-icon bg-primary text-white">
                                <i class="fas fa-clock"></i>
                            </span>
                            <span>update fitur terbaru.</span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <section class="download-section">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-7">
                    <h2>Mulai Pembelajaranmu sekarang</h2>
                    <p class="lead">Mulai belajar di Cleary!</p>
                </div>
                <div class="col-md-5 text-right">
                    <a href="<?= base_url('Auth/landing'); ?>" class="btn btn-primary btn-lg">Mulai sekarang</a>
                </div>
            </div>
        </div>
    </section>

    <section class="before-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="card long-shadow">
                        <div class="card-body d-flex p-45">
                            <div class="card-icon bg-primary text-white">
                                <i class="far fa-file"></i>
                            </div>
                            <div>
                                <h5>Lihat Cara Penggunaan</h5>
                                <p class="lh-sm">Modul penggunaan cara menggunakan Cleary.</p>
                                <div class="mt-4 text-right">
                                    <a href="<?= base_url('Home/Panduan'); ?>" class="link-icon">Panduan <i class="fas fa-chevron-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <h3 class="text-capitalize">Cleary</h3>
                    <div class="pr-lg-5">
                        <p>© Cleary. With <i class="fas fa-heart text-danger"></i> from Indonesia</p>
                        <div class="mt-4 social-links">
                            <a href="https://github.com/stisla"><i class="fab fa-github"></i></a>
                            <a href="https://twitter.com/getstisla"><i class="fab fa-twitter"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="row">
                      <div class="col-md-6">
                        <h4>Info</h4>
                        <ul>
                          <li><a href="<?= base_url('Auth/landing'); ?>">Mulai</a></li>
                          <li><a href="<?= base_url('Auth/login'); ?>">Login</a></li>
                          <li><a href="<?= base_url('Home/Panduan') ?>">Panduan</a></li>
                        </ul>
                      </div>
                      <div class="col-md-4">
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>




</script>
    <script>var csrf_token = 'zXwKlIeZMD0TLbXou3uhaIgZPt015SkLfeylkf4E';</script>
    
    <script src="<?= base_url('modules/jquery.min.js'); ?>"></script>
    <script src="<?= base_url('modules/popper.js'); ?>"></script>
    <script src="<?= base_url('modules/tooltip.js'); ?>"></script>
    <script src="<?= base_url('modules/bootstrap/js/bootstrap.min.js'); ?>"></script>
    <script src="<?= base_url('modules/prism/prism.js'); ?>"></script>
    <script src="<?= base_url('js/stisla.js'); ?>"></script>
    <script src="https://getstisla.com/landing/script.js"></script><div class="modal fade" tabindex="-1" role="dialog" id="fire-modal-1"><div class="modal-dialog modal-md" role="document">         <div class="modal-content">           <div class="modal-header">             <h5 class="modal-title">Modal Title</h5>             <button type="button" class="close" data-dismiss="modal" aria-label="Close">               <span aria-hidden="true">×</span>             </button>           </div>           <div class="modal-body">           <p>Your content goes here.</p></div>           <div class="modal-footer">           <button type="button" class="btn btn-primary btn-shadow" id="">Action</button></div>         </div>       </div>    </div>

    
  
    

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Fonts -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
        html {
            line-height: 1.15;
                -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
        }

        body {
            margin: 0;
        }

        header,
        nav,
        section {
            display: block;
        }

        figcaption,
        main {
            display: block;
        }

        a {
            background-color: transparent;
            -webkit-text-decoration-skip: objects;
        }

        strong {
            font-weight: inherit;
        }

        strong {
            font-weight: bolder;
        }

        code {
            font-family: monospace, monospace;
            font-size: 1em;
        }

        dfn {
            font-style: italic;
        }

        svg:not(:root) {
            overflow: hidden;
        }

        button,
        input {
            font-family: sans-serif;
            font-size: 100%;
            line-height: 1.15;
            margin: 0;
        }

        button,
        input {
            overflow: visible;
        }

        button {
            text-transform: none;
        }

        button,
        html [type="button"],
        [type="reset"],
        [type="submit"] {
            -webkit-appearance: button;
        }

        button::-moz-focus-inner,
        [type="button"]::-moz-focus-inner,
        [type="reset"]::-moz-focus-inner,
        [type="submit"]::-moz-focus-inner {
            border-style: none;
            padding: 0;
        }

        button:-moz-focusring,
        [type="button"]:-moz-focusring,
        [type="reset"]:-moz-focusring,
        [type="submit"]:-moz-focusring {
            outline: 1px dotted ButtonText;
        }

        legend {
            -webkit-box-sizing: border-box;
                    box-sizing: border-box;
            color: inherit;
            display: table;
            max-width: 100%;
            padding: 0;
            white-space: normal;
        }

        [type="checkbox"],
        [type="radio"] {
            -webkit-box-sizing: border-box;
                    box-sizing: border-box;
            padding: 0;
        }

        [type="number"]::-webkit-inner-spin-button,
        [type="number"]::-webkit-outer-spin-button {
            height: auto;
        }

        [type="search"] {
            -webkit-appearance: textfield;
            outline-offset: -2px;
        }

        [type="search"]::-webkit-search-cancel-button,
        [type="search"]::-webkit-search-decoration {
            -webkit-appearance: none;
        }

        ::-webkit-file-upload-button {
            -webkit-appearance: button;
            font: inherit;
        }

        menu {
            display: block;
        }

        canvas {
            display: inline-block;
        }

        template {
            display: none;
        }

        [hidden] {
            display: none;
        }

        html {
            -webkit-box-sizing: border-box;
                    box-sizing: border-box;
            font-family: sans-serif;
        }

        *,
        *::before,
        *::after {
            -webkit-box-sizing: inherit;
                    box-sizing: inherit;
        }

        p {
            margin: 0;
        }

        button {
            background: transparent;
            padding: 0;
        }

        button:focus {
            outline: 1px dotted;
            outline: 5px auto -webkit-focus-ring-color;
        }

        *,
        *::before,
        *::after {
            border-width: 0;
            border-style: solid;
            border-color: #dae1e7;
        }

        button,
        [type="button"],
        [type="reset"],
        [type="submit"] {
            border-radius: 0;
        }

        button,
        input {
            font-family: inherit;
        }

        input::-webkit-input-placeholder {
            color: inherit;
            opacity: .5;
        }

        input:-ms-input-placeholder {
            color: inherit;
            opacity: .5;
        }

        input::-ms-input-placeholder {
            color: inherit;
            opacity: .5;
        }

        input::placeholder {
            color: inherit;
            opacity: .5;
        }

        button,
        [role=button] {
            cursor: pointer;
        }

        .bg-transparent {
            background-color: transparent;
        }

        .bg-white {
            background-color: #fff;
        }

        .bg-teal-light {
            background-color: #64d5ca;
        }

        .bg-blue-dark {
            background-color: #2779bd;
        }

        .bg-indigo-light {
            background-color: #7886d7;
        }

        .bg-purple-light {
            background-color: #a779e9;
        }

        .bg-no-repeat {
            background-repeat: no-repeat;
        }

        .bg-cover {
            background-size: cover;
        }

        .border-grey-light {
            border-color: #dae1e7;
        }

        .hover\:border-grey:hover {
            border-color: #b8c2cc;
        }

        .rounded-lg {
            border-radius: .5rem;
        }

        .border-2 {
            border-width: 2px;
        }

        .hidden {
            display: none;
        }

        .flex {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
        }

        .items-center {
            -webkit-box-align: center;
                -ms-flex-align: center;
                    align-items: center;
        }

        .justify-center {
            -webkit-box-pack: center;
                -ms-flex-pack: center;
                    justify-content: center;
        }

        .font-sans {
            font-family: Nunito, sans-serif;
        }

        .font-light {
            font-weight: 300;
        }

        .font-bold {
            font-weight: 700;
        }

        .font-black {
            font-weight: 900;
        }

        .h-1 {
            height: .25rem;
        }

        .leading-normal {
            line-height: 1.5;
        }

        .m-8 {
            margin: 2rem;
        }

        .my-3 {
            margin-top: .75rem;
            margin-bottom: .75rem;
        }

        .mb-8 {
            margin-bottom: 2rem;
        }

        .max-w-sm {
            max-width: 30rem;
        }

        .min-h-screen {
            min-height: 100vh;
        }

        .py-3 {
            padding-top: .75rem;
            padding-bottom: .75rem;
        }

        .px-6 {
            padding-left: 1.5rem;
            padding-right: 1.5rem;
        }

        .pb-full {
            padding-bottom: 100%;
        }

        .absolute {
            position: absolute;
        }

        .relative {
            position: relative;
        }

        .pin {
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
        }

        .text-black {
            color: #22292f;
        }

        .text-grey-darkest {
            color: #3d4852;
        }

        .text-grey-darker {
            color: #606f7b;
        }

        .text-2xl {
            font-size: 1.5rem;
        }

        .text-5xl {
            font-size: 3rem;
        }

        .uppercase {
            text-transform: uppercase;
        }

        .antialiased {
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
        }

        .tracking-wide {
            letter-spacing: .05em;
        }

        .w-16 {
            width: 4rem;
        }

        .w-full {
            width: 100%;
        }

        @media (min-width: 768px) {
            .md\:bg-left {
                background-position: left;
            }

            .md\:bg-right {
                background-position: right;
            }

            .md\:flex {
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
            }

            .md\:my-6 {
                margin-top: 1.5rem;
                margin-bottom: 1.5rem;
            }

            .md\:min-h-screen {
                min-height: 100vh;
            }

            .md\:pb-0 {
                padding-bottom: 0;
            }

            .md\:text-3xl {
                font-size: 1.875rem;
            }

            .md\:text-15xl {
                font-size: 9rem;
            }

            .md\:w-1\/2 {
                width: 50%;
            }
        }

        @media (min-width: 992px) {
            .lg\:bg-center {
                background-position: center;
            }
        }
        </style>
    
    
        
            </div>
        </div>
    