<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function index() {
		$title = array(
			'title' => "Selamat Datang"
		);

		$this->load->view('dist/header', $title);
		$this->load->view('home/Home');
		$this->load->view('dist/footer');
	}
	

}