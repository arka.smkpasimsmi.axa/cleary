<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Siswa extends CI_Controller {

	public function index() {
		$title = array(
			'title' => "Siswa"
		);
		$auth = $this->db->get_where('m_siswa', ['nis' => $this->session->userdata('nis')])->row_array();
		$datamengikuti = explode('| ', $auth['mengikuti_nip']);
		$data['modul'] = $this->m_siswa->listmodul('r_modul');
		$data['datamengikuti'] = $this->m_siswa->datamengikuti($datamengikuti);

		$this->load->view('dist/header', $title);
		$this->load->view('dist/navbar2');
		$this->load->view('siswa/index', $data);
		$this->load->view('dist/footer');
	}

	public function daftarpengajar() {
		$title = array(
			'title' => "Daftar Pengajar"
		);
		$data['profilepengajar'] = $this->m_pengajar->get_mapel('m_pengajar');
		$data['siswa'] = $this->m_siswa->get_data();

		$this->load->view('dist/header', $title);
		$this->load->view('dist/navbar2');
		$this->load->view('dist/daftarpengajar', $data);
		$this->load->view('dist/footer');

	}

	public function panduan() {
		$title = array(
			'title' => "Panduan"
		);

		$this->load->view('dist/header', $title);
		$this->load->view('dist/navbar2');
		$this->load->view('siswa/panduan');
		$this->load->view('dist/footer');
	}

	public function mulai() {
		$title = array(
			'title' => "Mulai"
		);
		$data['modul'] = $this->m_siswa->listmodul('r_modul');

		$this->load->view('dist/header', $title);
		$this->load->view('dist/navbar2');
		$this->load->view('siswa/mulai', $data);
		$this->load->view('dist/footer');
	}

	public function daftarnilai() {
		$title = array(
			'title' => "Daftar Nilai"
		);
		$auth = $this->db->get_where('m_siswa', ['nis' => $this->session->userdata('nis')])->row_array();
		$nilai = $auth['daftar_nilai'];
		$data['nilai'] = $this->m_siswa->nilais($nilai);
		$data['modul'] = $this->m_siswa->listmodul('r_modul');
		$data['listnilai'] = $this->m_siswa->listnilai('r_nilai');

		$this->load->view('dist/header', $title);
		$this->load->view('dist/navbar2');
		$this->load->view('siswa/daftarnilai', $data);
		$this->load->view('dist/footer');
	}

	public function mengikuti() {
		$title = array(
			'title' => "Mengikuti"
		);
		$auth = $this->db->get_where('m_siswa', ['nis' => $this->session->userdata('nis')])->row_array();
		$mengikuti = explode('| ', $auth['mengikuti_nip']);
		$data['pengajar'] = $this->m_siswa->mengikuts($mengikuti);

		$this->load->view('dist/header', $title);
		$this->load->view('dist/navbar2');
		$this->load->view('siswa/mengikuti', $data);
		$this->load->view('dist/footer');
	}

	public function profile($nis) {
		$nis = ['nis' => $nis];
		$data['system'] = $this->m_system->getSystem();
		$title = ['title' => 'Profile'];
		$data['profilesiswa'] = $this->m_siswa->getDataProfile($nis, 'm_siswa');

		$this->load->view('dist/header', $title);
		$this->load->view('dist/navbar2');
		$this->load->view('siswa/profile', $data);
		$this->load->view('dist/footer');
		
	}
	public function postprofile($niss) {
		$nis= ['nis' => $niss];
		$nama_siswa = $this->input->post('nama_siswa');
		$tanggal_lahir = $this->input->post('tanggal_lahir');
		$jenis_kelamin = $this->input->post('jenis_kelamin');
		$agama = $this->input->post('agama');
		$no_hp = $this->input->post('no_hp');
		$alamat = $this->input->post('alamat');
		$bio = $this->input->post('bio');

		$namaDepan = explode('| ', $nama_siswa);
		$data = [
			'nama_siswa' => $nama_siswa,
			'value_system' => $agama.'| '.$jenis_kelamin,
			'tanggal_lahir' => $tanggal_lahir,
			'no_hp' => $no_hp,
			'alamat' => $alamat,
			'bio' => $bio,
			'changed_by' => $namaDepan[0]
		];

		$this->m_siswa->edit($data, $nis ,'m_siswa');
		$this->session->set_flashdata('success', 'Profile berhasil diperbarui');
		redirect(base_url('Siswa/profile/').$niss);
	}
	public function ikuti() {

		// Follow
		if ($this->input->post('ikuti')) {
			$nis= ['nis' => $this->session->userdata('nis')];
			$nip= ['nip' => $this->input->post('pengajar_nip')];
			$niss = $this->session->userdata('nis');
			$nipp = $this->input->post('pengajar_nip');
			$mengikuti = $this->input->post('mengikuti');
			$pengikut = $this->input->post('pengikut');
			if($mengikuti == '') {
				$inputMengikuti = $nipp;
			}else{
				$inputMengikuti = $mengikuti.'| ' .$nipp;
			}
			if($pengikut == '') {
				$inputPengikut = $niss;
			}else{
				$inputPengikut = $pengikut.'| '.$niss;
			}

			$dataSiswa = [
				'mengikuti_nip' => $inputMengikuti,
			];
			$dataPengajar = [
				'pengikut_nis' => $inputPengikut,
			];


			$this->m_siswa->edit($dataSiswa, $nis,  'm_siswa');
			$this->m_pengajar->edit($dataPengajar, $nip, 'm_pengajar');
			Redirect('Siswa/daftarpengajar');

		// Unfollow
		}elseif ($this->input->post('stop')) {
			$nis = ['nis' => $this->session->userdata('nis')];
			$nip = ['nip' => $this->input->post('nip')];
			$rls = $this->input->post('replacesiswa');
			$rlp = $this->input->post('replacepengajar');

			$dataSiswa = [
				'mengikuti_nip' => $rls
			];

			$dataPengajar = [
				'pengikut_nis' => $rlp
			];

			$this->m_siswa->edit($dataSiswa, $nis,  'm_siswa');
			$this->m_pengajar->edit($dataPengajar, $nip,  'm_pengajar');
			Redirect('Siswa/daftarpengajar');
		}
		
	}
	public function search() {
		$title = array(
			'title' => "Mulai"
		);
		$keyword = $this->input->get('keyword');
		$data['modul'] = $this->m_siswa->get_keyword($keyword, 'r_modul');

		$this->load->view('dist/header', $title);
		$this->load->view('dist/navbar2');
		$this->load->view('siswa/mulai', $data);
		$this->load->view('dist/footer');
	}
	public function detailprofile($nip)
	{
		$title = array(
			'title' => "Detail Profile"
		);
		$nip = ['nip' => $nip];
		$data['profilepengajar'] = $this->m_pengajar->getDataProfile($nip, 'm_pengajar');
		$data['system'] = $this->m_system->getSystem();

		$this->load->view('dist/header', $title);
		$this->load->view('dist/navbar2');
		$this->load->view('dist/detailprofile', $data);
		$this->load->view('dist/footer');
	}
	public function modul($materi, $id) {
		$title = array(
			'title' => "Modul"
		);
		$id = ['id_modul' => $id];
		$data['modul'] = $this->m_modul->getModulById($id, 'r_modul');
		$modul = $this->db->get_where('r_modul', $id)->row_array();
		$data['listnilai'] = $this->m_siswa->listmodul('r_modul');

	if ($materi == 'materi') {
		$this->load->view('dist/header', $title);
		$this->load->view('dist/navbar2');
		$this->load->view('Siswa/modul', $data);
		$this->load->view('dist/footer');
	}elseif($materi == 'pilgan') {
		$id = ['id_pilgan' => $modul['pilgan_id']];
		$data['pilgan'] = $this->m_pilgan->getPilganById($id, 'm_pilgan');
		$this->load->view('dist/header', $title);
		$this->load->view('dist/navbar2');
		$this->load->view('Siswa/modul', $data);
		$this->load->view('dist/footer');
	}elseif($materi == 'essay') {
		$id = ['id_essay' => $modul['essay_id']];
		$data['essay'] = $this->m_essay->getEssayById($id, 'm_essay');
		$this->load->view('dist/header', $title);
		$this->load->view('dist/navbar2');
		$this->load->view('Siswa/modul', $data);
		$this->load->view('dist/footer');
	}
	
}

	public function prosesnilai($id) {
		$nis= ['nis' => $this->session->userdata('nis')];
		$niss = $this->session->userdata('nis');
		$daftarnilai = $this->input->post('daftar_nilai');

		$id = ['id_pilgan' => $id];
		$id_modul = $this->input->post('id_modul');
		$pilgan = $this->m_pilgan->getPilganById($id, 'm_pilgan');
		$mapel_id = $this->input->post('mapel_id');
		$pengajar_nip = $this->input->post('pengajar_nip');
		$nis = $this->session->userdata('nis');
		$namaDepan = explode(' ',$this->session->userdata('name'));
		$a=1;
		$b=1;
		$c=1;
		$benar=0;
		$salah=0;
		$jumlahsoal = count($pilgan);
		
		foreach ($pilgan as $data) {
			if($this->input->post('isijawaban-'.$a++) == $this->input->post('jawaban-'.$b++)) {
				$benar++;
			}else{
				$salah++;
			}
		}
		$nilai = 100 / $jumlahsoal * $benar;
		
		$data = [
			'siswa_nis' => $nis,
			'modul_id'	=> $id_modul,
			'nilai'		=> $nilai,
			'mapel_id'	=> $mapel_id,
			'tipe_modul_id'	=> 2,
			'pengajar_nip'	=> $pengajar_nip,
			'created_by'	=> $namaDepan[0],
		];
		
		$this->m_modul->postModul($data, 'r_nilai');
		$this->session->set_flashdata('success', ' Nilai anda '.$nilai);
		redirect(base_url('Siswa/daftarnilai/'));
		
	}

	public function postessay() {
		$this->session->set_flashdata('success', ' Jawaban anda sudah dikirim!');
		redirect(base_url('Siswa/daftarnilai/'));
		
	}

}	
