<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

    public function landing() {
		$data = array(
			'title' => "Daftar Sebagai"
		);

		$this->load->view('dist/header', $data);
		$this->load->view('auth/landing');
		$this->load->view('dist/footer');
    }

	public function daftarsiswa() {

		$this->form_validation->set_rules('nama_siswa', 'Nama Siswa', 'required', 
																			['required' => 'nama siswa harus diisi']);
		$this->form_validation->set_rules('nis', 'NIS', 'required', 
																			['required' => 'nis siswa harus diisi']);
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[m_auth.email]', 
																			['required' => 'email harus diisi',
																			'valid_email' => 'masukkan email yang benar',
																			'is_unique' => 'email sudah terdaftar!']);
		$this->form_validation->set_rules('password', 'Password', 'required', 
																			['required' => 'password harus diisi']);
		$this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'required', 
																			['required' => 'jenis kelamin harus diisi']);
		$this->form_validation->set_rules('agama', 'Agama', 'required', 
																			['required' => 'agama harus diisi']);
		$this->form_validation->set_rules('alamat', 'Alamat', 'required', 
																			['required' => 'alamat harus diisi']);
		$this->form_validation->set_rules('tanggal_lahir', 'Tanggal Lahir', 'required', 
																			['required' => 'tanggal lahir harus diisi']);

		if ($this->form_validation->run() == false) {
			$data = array(
				'title' => "Pendaftaran Siswa",
				'system' => $this->m_system->getSystem()
			);
			$this->session->set_flashdata('fail', 'Pendaftaran Gagal! Silahkan Coba Lagi ');

			$this->load->view('dist/header', $data);
			$this->load->view('auth/daftarsiswa', $data);
			$this->load->view('dist/footer');
		} else {
			$nama_siswa = $this->input->post('nama_siswa');
			$nis = $this->input->post('nis');
			$email = $this->input->post('email');
			$password = $this->input->post('password');
			$jenis_kelamin = $this->input->post('jenis_kelamin');
			$agama = $this->input->post('agama');
			$alamat = $this->input->post('alamat');
			$tanggal_lahir = $this->input->post('tanggal_lahir');

			$config['upload_path']		= './assets/img/avatar';
			$config['allowed_types']	= 'jpg|png|jpeg';
			$config['file_name']		= $nama_siswa.'-'.date('y-m-d');
			$this->load->library('upload', $config);

			if(!$this->upload->do_upload('avatar')){
				$this->session->set_flashdata('fail', 'Kesalahan mengunggah gambar');
				Redirect('Auth/daftarsiswa');
			} else {
				$avatar = $this->upload->data('file_name');
				$namaDepan = explode(' ', $nama_siswa);
				$dataAuth = [
					'name' => $nama_siswa,
					'email' => $email,
					'password' => password_hash($password, PASSWORD_DEFAULT),
					'role' => 'siswa',
					'siswa_nis' => $nis,
					'status_aktivasi' => 'dikonfirmasi',
					'created_by' => $namaDepan[0]
				];
				$dataSiswa = [
					'nis' => $nis,
					'nama_siswa' => $nama_siswa,
					'value_system' => $agama.'| '.$jenis_kelamin,
					'tanggal_lahir' => $tanggal_lahir,
					'alamat' => $alamat,
					'avatar' => $avatar,
					'created_by' => $namaDepan[0]
				];
				$this->m_auth->daftar($dataAuth, 'm_auth');
				$this->m_auth->daftar($dataSiswa, 'm_siswa');
				$this->session->set_flashdata('success', 'Pendaftaran berhasil');
				Redirect('Auth/login');
			}
		}


	}
	
    public function daftarpengajar() {
    	$this->form_validation->set_rules('nama_pengajar', 'Nama Pengajar', 'required',
    																					['required' => 'nama pengajar harus diisi!']);
    	$this->form_validation->set_rules('nip', 'NIP', 'required',
    																					['required' => 'NIP harus diisi!']);

    	$this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[m_auth.email]',
    																					['required' => 'email harus diisi!',
    																					 'valid_email' => 'masukan email yang benar!',
    																					 'is_unique' => 'email sudah terdaftar!']);

    	$this->form_validation->set_rules('password', 'Password', 'required',
    																					['required' => 'Password harus diisi!']);

    	$this->form_validation->set_rules('alamat', 'Alamat', 'required',
    																					['required' => 'Alamat harus diisi!']);

    	$this->form_validation->set_rules('mapel_id', 'Mapel', 'required',
    																					['required' => 'Mapel harus diisi!']);

    	$this->form_validation->set_rules('tanggal_lahir', 'Tanggal Lahir', 'required',
    																					['required' => 'Tanggal Lahir harus diisi!']);

    	// $this->form_validation->set_rules('avatar', 'Avatar', 'required',
    	// 																				['required' => 'Avatar harus diisi!']);

    	$this->form_validation->set_rules('agama', 'Agama', 'required',
    																					['required' => 'Agama harus diisi!']);

    	$this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'required',
    																					['required' => 'Jenis Kelamin harus diisi!']);
    	$this->form_validation->set_rules('status_kawin', 'Status Kawin', 'required',
    																					['required' => 'Status Kawin harus diisi!']);


    	if($this->form_validation->run() == false) {
				$data = array(
				'title' => "Pendaftaran Pengajar",
				'system' => $this->m_system->getSystem(),
				'mapel' => $this->m_mapel->getMapel()
			);
			$this->session->set_flashdata('fail', 'Pendaftaran Gagal! Silahkan coba lagi.');

			$this->load->view('dist/header', $data);
			$this->load->view('auth/daftarpengajar', $data);
			$this->load->view('dist/footer');
		}else{
			$nama_pengajar = $this->input->post('nama_pengajar');
			$nip = $this->input->post('nip');
			$email = $this->input->post('email');
			$password = $this->input->post('password');
			$alamat = $this->input->post('alamat');
			$mapel_id = $this->input->post('mapel_id');
			$tanggal_lahir = $this->input->post('tanggal_lahir');
			$agama = $this->input->post('agama');
			$jenis_kelamin = $this->input->post('jenis_kelamin');
			$status_kawin = $this->input->post('status_kawin');

				$config['upload_path']		= './assets/img/avatar/';
				$config['allowed_types']	= 'jpg|png|jpeg';
				$config['file_name']		= $nama_pengajar.'-'.date('y-m-d');
				$this->load->library('upload', $config);

				if(!$this->upload->do_upload('avatar')){
					$this->session->set_flashdata('fail', 'Kesalahan mengunggah gambar!');
    				Redirect( 'Auth/daftarpengajar' );
				}else{
					$avatar = $this->upload->data('file_name');
					$namaDepan = explode(' ', $nama_pengajar);
					$dataAuth = [
						'name' => $nama_pengajar,
						'email' => $email,
						'password' => password_hash($password, PASSWORD_DEFAULT),
						'role' => 'pengajar',
						'pengajar_nip' => $nip,
						'status_aktivasi' => 'menunggu',
						'created_by' => $namaDepan[0]
					];
					$dataPengajar = [
						'nip' => $nip,
						'nama_pengajar' => $nama_pengajar,
						'mapel_id' => $mapel_id,
						'value_system' => $agama.'| '.$jenis_kelamin.'| '.$status_kawin,
						'tanggal_lahir' => $tanggal_lahir,
						'alamat' => $alamat,
						'avatar' => $avatar,
						'created_by' => $namaDepan[0]
					];
					$this->m_auth->daftar($dataAuth, 'm_auth');
					$this->m_auth->daftar($dataPengajar, 'm_pengajar');
					$this->session->set_flashdata('success' , 'Pendaftaran Berhasil!');
					Redirect('Auth/login');
				}
		}

    }

    public function Login() {
    	$this->form_validation->set_rules('email', 'Email', 'required', ['required' => 'email harus diisi!']);
		$this->form_validation->set_rules('password', 'Password', 'required', ['required' => 'password harus diisi!']);

		if ($this->form_validation->run() == false) {
			$data = array(
				'title' => "Halaman Login"
			);

			$this->load->view('dist/header', $data);
			$this->load->view('auth/login');
			$this->load->view('dist/footer');
		}else{
			$this->postlogin();
		}
    }

    private function postlogin() {
		$email = $this->input->post('email');
		$password = $this->input->post('password');

		$auth = $this->db->get_where('m_auth', ['email' => $email])->row_array();
		if ($auth) {
			if (password_verify($password, $auth['password'])) {
				if($auth['status_aktivasi'] == 'dikonfirmasi') {
					$data = [
						'id' 				=> $auth['id'],
						'name' 				=> $auth['name'],
						'email' 			=> $auth['email'],
						'role' 				=> $auth['role'],
						'nip' 				=> $auth['pengajar_nip'],
						'nis' 				=> $auth['siswa_nis'],
						'status_aktivasi'   => $auth['status_aktivasi'],
					];
					if($auth['role'] == 'admin') {
						$this->session->set_userdata($data);
						$this->session->set_flashdata('success', 'Selamat datang '.$auth['name']);
						Redirect('Admin/Dashboard');
					}elseif($auth['role'] == 'pengajar') {
						$authPengajar = $this->db->get_where('m_pengajar', ['nip' => $auth['pengajar_nip']])->row_array();
						$data['avatar'] = $authPengajar['avatar'];
						$this->session->set_userdata($data);
						$this->session->set_flashdata('success', 'Selamat datang '.$auth['name']);
						Redirect('Pengajar');
					}elseif($auth['role'] == 'siswa') {
						$authSiswa = $this->db->get_where('m_siswa', ['nis' => $auth['siswa_nis']])->row_array();
						$data['avatar'] = $authSiswa['avatar'];
						$this->session->set_userdata($data);
						$this->session->set_flashdata('success', 'Selamat datang '.$auth['name']);
						Redirect('Siswa');
					}
				}elseif($auth['status_aktivasi'] == 'ditolak'){
					$this->session->set_flashdata('fail', 'Akun kamu ditolak! <br> tolong hubungi Admin!');
					Redirect('Auth/login');
				}elseif($auth['status_aktivasi'] == 'menunggu'){
					$this->session->set_flashdata('fail', 'Akun kamu belum dikonfirmasi! <br> silahkan tunggu atau hubungi Admin!');
					Redirect('Auth/login');
				}
			}else{
				$this->session->set_flashdata('fail', 'Password salah! <br> Tolong coba lagi!');
				Redirect('Auth/login');
			}
		}else{
			$this->session->set_flashdata('fail', 'Email salah! <br> Tolong coba lagi!');
			Redirect('Auth/login');
		}
	}

	public function logout() {
		$data = ['id', 'name', 'email', 'role', 'nip', 'nis', 'status_aktivasi', 'avatar'];
		$this->session->unset_userdata($data);
		$this->session->set_flashdata('success', 'Berhasil keluar! ');	
		Redirect('Auth/login');
	}

	public function settings($halaman = 'gantiemail') {
		$title = ['title' => 'Settings'];

		if ($halaman == 'gantiemail') {
			$this->form_validation->set_rules('emailBaru', 'Email', 'required|valid_email|is_unique[m_auth.email]', 
																			['required' => 'email harus diisi',
																			'valid_email' => 'masukkan email yang benar',
																			'is_unique' => 'email sudah terdaftar']);
			if ($this->form_validation->run() == false) {
				$this->load->view('dist/header', $title);
				$this->load->view('dist/navbar2');
				$this->load->view('auth/settings');
				$this->load->view('dist/footer');
			}else{
				$id = $this->input->post('id');
				$id = ['id' => $id];
				$emailBaru = $this->input->post('emailBaru');
				$data = [
					'email' => $emailBaru
				];
				$this->m_auth->edit($id, $data, 'm_auth');
				$this->session->set_flashdata('success', 'Email berhasil diganti!');
				Redirect('Auth/settings');
			}
		}elseif ($halaman == 'gantipassword') {
			$this->form_validation->set_rules('passwordLama', 'Retype Password', 'required',
																		['required' => 'password lama harus diisi!']);
			$this->form_validation->set_rules('passwordBaru', 'Password', 'required', 
																		['required' => 'password baru harus diisi']);
			if ($this->form_validation->run() == false) {
				$this->load->view('dist/header', $title);
				$this->load->view('dist/navbar2');
				$this->load->view('auth/settings');
				$this->load->view('dist/footer');
			}else{
				$id = $this->input->post('id');
				$id = ['id' => $id];
				$auth = $this->db->get_where('m_auth', $id)->row_array();
				$passwordLama = $this->input->post('passwordLama');
				$passwordBaru = $this->input->post('passwordBaru');
				if (password_verify($passwordLama, $auth['password'])) {
					$data = [
						'password' => password_hash($passwordBaru, PASSWORD_DEFAULT) 
					];
					$this->m_auth->edit($id, $data, 'm_auth');
					$this->session->set_flashdata('success', 'Password berhasil diganti!');
					Redirect('Auth/settings/gantipassword');
				}else{
					$this->session->set_flashdata('error', 'password lama tidak sesuai');
					Redirect('Auth/settings/gantipassword');
				}
			}
		}
	}

	public function pageNotFound()
	{
		$title = array(
			'title' => "404 Not Found"
		);

		$this->load->view('dist/header', $title);
		$this->load->view('Admin/404');
		$this->load->view('dist/footer');
	}
}

?>