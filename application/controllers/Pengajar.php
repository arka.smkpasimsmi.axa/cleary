<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengajar extends CI_Controller {

	public function index() {
		$title = array(
			'title' => "Pengajar"
		);
		$data['listnilai'] = $this->m_modul->getNilai();

		$this->load->view('dist/header', $title);
		$this->load->view('dist/navbar2');
		$this->load->view('pengajar/index', $data);
		$this->load->view('dist/footer');
	}

	public function daftarpengajar() {
		$title = array(
			'title' => "Daftar Pengajar"
		);
		$data['profilepengajar'] = $this->m_pengajar->get_mapel('m_pengajar');

		$this->load->view('dist/header', $title);
		$this->load->view('dist/navbar2');
		$this->load->view('dist/daftarpengajar', $data);
		$this->load->view('dist/footer');
	}

	public function panduan() {
		$title = array(
			'title' => "Panduan"
		);

		$this->load->view('dist/header', $title);
		$this->load->view('dist/navbar2');
		$this->load->view('pengajar/panduan');
		$this->load->view('dist/footer');
	}

	public function mulai() {
		$title = array(
			'title' => "Mulai"
		);
		$nip = ['nip' => $this->session->userdata('nip')];
		$data['auth'] = $this->m_pengajar->getDataProfile($nip, 'm_pengajar');

		$this->load->view('dist/header', $title);
		$this->load->view('dist/navbar2');
		$this->load->view('pengajar/mulai', $data);
		$this->load->view('dist/footer');
	}

	public function daftarmodul() {
		$title = array(
			'title' => "Daftar Modul"
		);
		$data['modul'] = $this->m_modul->getModul();

		$this->load->view('dist/header', $title);
		$this->load->view('dist/navbar2');
		$this->load->view('pengajar/daftarmodul', $data);
		$this->load->view('dist/footer');
	}

	public function pengikut() {
		$title = array(
			'title' => "Pengikut"
		);

  		$auth = $this->db->get_where('m_pengajar', ['nip' => $this->session->userdata('nip')])->row_array(); 
  		$pengikut = explode('| ', $auth['pengikut_nis']); 
		$data['pengikut'] = $this->m_pengajar->pengikuts($pengikut);

		$this->load->view('dist/header', $title);
		$this->load->view('dist/navbar2');
		$this->load->view('pengajar/pengikut', $data);
		$this->load->view('dist/footer');
	}

	public function profilesiswa($nis) {
		$nis = ['nis' => $nis];
  		$auth = $this->db->get_where('m_siswa', $nis)->row_array(); 
		$title = array(
			'title' => $auth['nama_siswa'],
		);
		$data['system'] = $this->m_system->getSystem();
		$data['profilesiswa'] = $this->m_siswa->getDataProfile($nis, 'm_siswa');

		$this->load->view('dist/header', $title);
		$this->load->view('dist/navbar2');
		$this->load->view('pengajar/profilesiswa', $data);
		$this->load->view('dist/footer');
	}

	public function profile($nip) {
		$nip = ['nip' => $nip];
		$title = ['title' => 'Profile'];
		$data['profilepengajar'] = $this->m_pengajar->getDataProfile($nip, 'm_pengajar');
		$data['system'] = $this->m_system->getSystem();

		$this->load->view('dist/header', $title);
		$this->load->view('dist/navbar2');
		$this->load->view('pengajar/profile', $data);
		$this->load->view('dist/footer');
	}

	public function postprofile($nips) {
		$nip = ['nip' => $nips];
		$nama_pengajar = $this->input->post('nama_pengajar');
		$agama = $this->input->post('agama');
		$jenis_kelamin = $this->input->post('jenis_kelamin');
		$status_kawin = $this->input->post('status_kawin');
		$tanggal_lahir = $this->input->post('tanggal_lahir');
		$no_hp = $this->input->post('no_hp');
		$alamat = $this->input->post('alamat');
		$bio = $this->input->post('bio');

		$namaDepan = explode(' ', $nama_pengajar);
		$data = [
			'nama_pengajar' => $nama_pengajar,
			'value_system'  => $agama.'| '.$jenis_kelamin.'| '.$status_kawin,
			'tanggal_lahir' => $tanggal_lahir,
			'no_hp' => $no_hp,
			'alamat' => $alamat,
			'bio' => $bio,
			'changed_by' => $namaDepan[0]
		];

		$this->m_pengajar->edit($data, $nip, 'm_pengajar');
		$this->session->set_flashdata('success', 'Profile berhasil diperbarui!');
		Redirect(base_url('Pengajar/profile/'.$nips));
	}

	public function materi() {
		$this->form_validation->set_rules('judul_materi', 'Judul Materi', 'required', ['required' => 'judul materi harus diisi']);
		$this->form_validation->set_rules('mapel_id', 'Mapel', 'required', ['required' => 'mapel harus diisi']);
		$this->form_validation->set_rules('deskripsi_materi', 'Deskripsi Materi', 'required', ['required' => 'deskripsi materi harus diisi']);

		if ($this->form_validation->run() == false) {
			$title = array(
				'title' => "Mulai"
			);
			$nip = ['nip' => $this->session->userdata('nip')];
			$data['auth'] = $this->m_pengajar->getDataProfile($nip, 'm_pengajar');

			$this->load->view('dist/header', $title);
			$this->load->view('dist/navbar2');
			$this->load->view('pengajar/mulai', $data);
			$this->load->view('dist/footer');
		}else{
			$judul_materi = $this->input->post('judul_materi');
			$mapel_id = $this->input->post('mapel_id');
			$deskripsi_materi = $this->input->post('deskripsi_materi');
			$namaDepan = explode(' ', $this->session->userdata('name'));

			$dataMateri = [
				'deskripsi_materi' => $deskripsi_materi,
				'created_by' => $namaDepan[0],
			];
			$this->m_materi->postMateri($dataMateri, 'm_materi');
			$insert_id = $this->db->insert_id();

			$dataModul = [
				'pengajar_nip' => $this->session->userdata('nip'),
				'judul_modul' => $judul_materi,
				'materi_id' => $insert_id,
				'mapel_id' => $mapel_id,
				'tipe_modul_id' => 1,
				'created_by' => $namaDepan[0],
			];
			$this->m_modul->postModul($dataModul, 'r_modul');

			$this->session->set_flashdata('success', 'Materi berhasil ditambahkan!');
			Redirect(base_url('Pengajar/daftarmodul'));
		}
	}

	public function pilgan() {
		$this->form_validation->set_rules('judul_pilgan', 'Judul Pilgan', 'required', ['required' => 'judul pilgan harus diisi']);
		$this->form_validation->set_rules('waktu_pengerjaan', 'Waktu Pengerjaan', 'required', ['required' => 'waktu pengerjaan harus diisi']);
		$this->form_validation->set_rules('batas_waktu', 'Batas Waktu', 'required', ['required' => 'batas waktu harus diisi']);

		if ($this->form_validation->run() == false) {
			$title = array(
				'title' => "Mulai"
			);
			$nip = ['nip' => $this->session->userdata('nip')];
			$data['auth'] = $this->m_pengajar->getDataProfile($nip, 'm_pengajar');

			$this->load->view('dist/header', $title);
			$this->load->view('dist/navbar2');
			$this->load->view('pengajar/mulai', $data);
			$this->load->view('dist/footer');
		}else{
			$judul_pilgan = $this->input->post('judul_pilgan');
			$mapel_id = $this->input->post('mapel_id');
			$soal_pilgan = $this->input->post('soal_pilgan');
			$a = $this->input->post('a');
			$b = $this->input->post('b');
			$c = $this->input->post('c');
			$d = $this->input->post('d');
			$jawaban = $this->input->post('jawaban');
			$waktu_pengerjaan = $this->input->post('waktu_pengerjaan');
			$batas_waktu = $this->input->post('batas_waktu');
			$namaDepan = explode(' ', $this->session->userdata('name'));
			$id_pilgan = rand(1, 1000);

			$dataPilgan = array();
			foreach ($soal_pilgan as $key => $value) {
				$dataPilgan[] = [
					'id_pilgan'	=> $id_pilgan,
					'soal_pilgan' => $soal_pilgan[$key],
					'a' => $a[$key],
					'b' => $b[$key],
					'c' => $c[$key],
					'd' => $d[$key],
					'jawaban' => $jawaban[$key],
					'created_by' => $namaDepan[0],
				];
			}
			$this->m_pilgan->postPilgan($dataPilgan, 'm_pilgan');

			$dataModul = [
				'pengajar_nip' => $this->session->userdata('nip'),
				'judul_modul' => $judul_pilgan,
				'pilgan_id' => $id_pilgan,
				'mapel_id' => $mapel_id,
				'tipe_modul_id' => 2,
				'waktu_pengerjaan' => $waktu_pengerjaan,
				'batas_waktu' => $batas_waktu,
				'created_by' => $namaDepan[0],
			];
			$this->m_modul->postModul($dataModul, 'r_modul');

			$this->session->set_flashdata('success', 'Pilahan Ganda berhasil ditambahkan!');
			Redirect(base_url('Pengajar/daftarmodul'));
		}
	}

	public function essay() {
		$this->form_validation->set_rules('judul_essay', 'Judul Essay', 'required', ['required' => 'judul essay harus diisi']);
		$this->form_validation->set_rules('waktu_pengerjaan', 'Waktu Pengerjaan', 'required', ['required' => 'waktu pengerjaan harus diisi']);
		$this->form_validation->set_rules('batas_waktu', 'Batas Waktu', 'required', ['required' => 'batas waktu harus diisi']);

		if ($this->form_validation->run() == false) {
			$title = array(
				'title' => "Mulai"
			);
			$nip = ['nip' => $this->session->userdata('nip')];
			$data['auth'] = $this->m_pengajar->getDataProfile($nip, 'm_pengajar');

			$this->load->view('dist/header', $title);
			$this->load->view('dist/navbar2');
			$this->load->view('pengajar/mulai', $data);
			$this->load->view('dist/footer');
		}else{
			$judul_essay = $this->input->post('judul_essay');
			$mapel_id = $this->input->post('mapel_id');
			$soal_essay = $this->input->post('soal_essay');
			$waktu_pengerjaan = $this->input->post('waktu_pengerjaan');
			$batas_waktu = $this->input->post('batas_waktu');
			$namaDepan = explode(' ', $this->session->userdata('name'));
			$id_essay = rand(1, 1000);

			$dataEssay = array();
			foreach ($soal_essay as $key => $value) {
				$dataEssay[] = [
					'id_essay'	=> $id_essay,
					'soal_essay' => $soal_essay[$key],
					'created_by' => $namaDepan[0],
				];
			}
			$this->m_essay->postEssay($dataEssay, 'm_essay');

			$dataModul = [
				'pengajar_nip' => $this->session->userdata('nip'),
				'judul_modul' => $judul_essay,
				'essay_id' => $id_essay,
				'mapel_id' => $mapel_id,
				'tipe_modul_id' => 3,
				'waktu_pengerjaan' => $waktu_pengerjaan,
				'batas_waktu' => $batas_waktu,
				'created_by' => $namaDepan[0],
			];
			$this->m_modul->postModul($dataModul, 'r_modul');

			$this->session->set_flashdata('success', 'Essay berhasil ditambahkan!');
			Redirect(base_url('Pengajar/daftarmodul'));
		}
	}

	public function modul($materi, $id) {
			$title = array(
				'title' => "Modul"
			);
			$id = ['id_modul' => $id];
			$data['modul'] = $this->m_modul->getModulById($id, 'r_modul');
			$modul = $this->db->get_where('r_modul', $id)->row_array();

		if ($materi == 'materi') {
			$this->load->view('dist/header', $title);
			$this->load->view('dist/navbar2');
			$this->load->view('pengajar/modul', $data);
			$this->load->view('dist/footer');
		}elseif($materi == 'pilgan') {
			$id = ['id_pilgan' => $modul['pilgan_id']];
			$data['pilgan'] = $this->m_pilgan->getPilganById($id, 'm_pilgan');
			$this->load->view('dist/header', $title);
			$this->load->view('dist/navbar2');
			$this->load->view('pengajar/modul', $data);
			$this->load->view('dist/footer');
		}elseif($materi == 'essay') {
			$id = ['id_essay' => $modul['essay_id']];
			$data['essay'] = $this->m_essay->getEssayById($id, 'm_essay');
			$this->load->view('dist/header', $title);
			$this->load->view('dist/navbar2');
			$this->load->view('pengajar/modul', $data);
			$this->load->view('dist/footer');
		}
		
	}

	public function hapusmodul() {
		$id_modul = $this->input->post('id_modul');
		$materi_id = $this->input->post('materi_id');
		$pilgan_id = $this->input->post('pilgan_id');
		$essay_id = $this->input->post('essay_id');

		$this->m_modul->hapus($id_modul, 'r_modul');

		if(isset($materi_id)) {
			$this->m_materi->hapusByModulId($materi_id, 'm_materi');
		}if(isset($pilgan_id)) {
			$this->m_pilgan->hapusByModulId($pilgan_id, 'm_pilgan');
		}if(isset($essay_id)) {
			$this->m_essay->hapusByModulId($essay_id, 'm_essay');
		}

		$this->session->set_flashdata('success', 'Modul Berhasil dihapus!');
		Redirect('Pengajar/daftarmodul');
	}

	public function editmateri($id) {
		$this->form_validation->set_rules('judul_materi', 'Judul Materi', 'required', ['required' => 'judul materi harus diisi']);
		$this->form_validation->set_rules('mapel_id', 'Mapel', 'required', ['required' => 'mapel harus diisi']);
		$this->form_validation->set_rules('deskripsi_materi', 'Deskripsi Materi', 'required', ['required' => 'deskripsi materi harus diisi']);

		if ($this->form_validation->run() == false) {
			$title = array(
				'title' => "Modul"
			);
			$nip = ['nip' => $this->session->userdata('nip')];
			$data['auth'] = $this->m_pengajar->getDataProfile($nip, 'm_pengajar');

			$this->load->view('dist/header', $title);
			$this->load->view('dist/navbar2');
			$this->load->view('pengajar/modul/materi/'.$id, $data);
			$this->load->view('dist/footer');
		}else{
			$judul_materi = $this->input->post('judul_materi');
			$mapel_id = $this->input->post('mapel_id');
			$materi_id = $this->input->post('materi_id');
			$deskripsi_materi = $this->input->post('deskripsi_materi');
			$namaDepan = explode(' ', $this->session->userdata('name'));
			$idmateri = ['id' => $materi_id];
			$idmodul = ['id_modul' => $id];

			$dataMateri = [
				'deskripsi_materi' => $deskripsi_materi,
				'changed_by' => $namaDepan[0],
			];
			$this->m_materi->edit($idmateri, $dataMateri, 'm_materi');

			$dataModul = [
				'pengajar_nip' => $this->session->userdata('nip'),
				'judul_modul' => $judul_materi,
				'mapel_id' => $mapel_id,
				'changed_by' => $namaDepan[0],
			];
			$this->m_modul->edit($idmodul, $dataModul, 'r_modul');

			$this->session->set_flashdata('success', 'Materi berhasil diedit!');
			Redirect(base_url('Pengajar/modul/materi/'.$id));
		}
	}

	public function hapuspilgan($idm, $id) {
		$this->m_pilgan->hapus($id, 'm_pilgan');

		$this->session->set_flashdata('success', 'Soal pilgan berhasil dihapus!');
		Redirect(base_url('Pengajar/modul/pilgan/'.$idm));
	}

	public function editpilgan($idm, $id) {
		$soal_pilgan = $this->input->post('soal_pilgan');
		$a = $this->input->post('a');
		$b = $this->input->post('b');
		$c = $this->input->post('c');
		$d = $this->input->post('d');
		$jawaban = $this->input->post('jawaban');
		$namaDepan = explode(' ', $this->session->userdata('name'));
		$idpilgan = ['id' => $id];

		$dataPilgan = [
			'soal_pilgan' => $soal_pilgan,
			'a' => $a,
			'b' => $b,
			'c' => $c,
			'd' => $d,
			'jawaban' => $jawaban,
			'changed_by' => $namaDepan[0],
		];

		$this->m_pilgan->edit($idpilgan, $dataPilgan, 'm_pilgan');
		$this->session->set_flashdata('success', 'Soal pilgan berhasil diedit!');
		Redirect(base_url('Pengajar/modul/pilgan/'.$idm));
	}

	public function tambahpilgan($idm) {
		$this->form_validation->set_rules('waktu_pengerjaan', 'Waktu Pengerjaan', 'required', ['required' => 'waktu pengerjaan harus diisi']);
		$this->form_validation->set_rules('batas_waktu', 'Batas Waktu', 'required', ['required' => 'batas waktu harus diisi']);

		if ($this->form_validation->run() == false) {
			$title = array(
				'title' => "Mulai"
			);
			$nip = ['nip' => $this->session->userdata('nip')];
			$data['auth'] = $this->m_pengajar->getDataProfile($nip, 'm_pengajar');

			$this->load->view('dist/header', $title);
			$this->load->view('dist/navbar2');
			$this->load->view('pengajar/mulai', $data);
			$this->load->view('dist/footer');
		}else{
			$soal_pilgan = $this->input->post('soal_pilgan');
			$a = $this->input->post('a');
			$b = $this->input->post('b');
			$c = $this->input->post('c');
			$d = $this->input->post('d');
			$jawaban = $this->input->post('jawaban');
			$namaDepan = explode(' ', $this->session->userdata('name'));
			$id_pilgan = $this->input->post('id_pilgan');;

			if ($soal_pilgan == '' ) {
				Redirect(base_url('Pengajar/modul/essay/'.$idm));
			}else{
				$dataPilgan = array();
				foreach ($soal_pilgan as $key => $value) {
					$dataPilgan[] = [
						'id_pilgan'	=> $id_pilgan,
						'soal_pilgan' => $soal_pilgan[$key],
						'a' => $a[$key],
						'b' => $b[$key],
						'c' => $c[$key],
						'd' => $d[$key],
						'jawaban' => $jawaban[$key],
						'created_by' => $namaDepan[0],
					];
				}
				$this->m_pilgan->postPilgan($dataPilgan, 'm_pilgan');

				$this->session->set_flashdata('success', 'Soal pilgan berhasil ditambahkan!');
				Redirect(base_url('Pengajar/modul/pilgan/'.$idm));
			}
		}
	}

	public function hapusessay($idm, $id) {
		$this->m_essay->hapus($id, 'm_essay');

		$this->session->set_flashdata('success', 'Soal essay berhasil dihapus!');
		Redirect(base_url('Pengajar/modul/essay/'.$idm));
	}

	public function editessay($idm, $id) {
		$soal_essay = $this->input->post('soal_essay');
		$namaDepan = explode(' ', $this->session->userdata('name'));
		$idEssay = ['id' => $id];

		$dataEssay = [
			'soal_essay' => $soal_essay,
			'changed_by' => $namaDepan[0],
		];

		$this->m_essay->edit($idEssay, $dataEssay, 'm_essay');
		$this->session->set_flashdata('success', 'Soal Essay berhasil diedit!');
		Redirect(base_url('Pengajar/modul/essay/'.$idm));
	}

	public function tambahessay($idm) {
		$this->form_validation->set_rules('waktu_pengerjaan', 'Waktu Pengerjaan', 'required', ['required' => 'waktu pengerjaan harus diisi']);
		$this->form_validation->set_rules('batas_waktu', 'Batas Waktu', 'required', ['required' => 'batas waktu harus diisi']);

		if ($this->form_validation->run() == false) {
			$title = array(
				'title' => "Mulai"
			);
			$nip = ['nip' => $this->session->userdata('nip')];
			$data['auth'] = $this->m_pengajar->getDataProfile($nip, 'm_pengajar');

			$this->load->view('dist/header', $title);
			$this->load->view('dist/navbar2');
			$this->load->view('pengajar/mulai', $data);
			$this->load->view('dist/footer');
		}else{
			$soal_essay = $this->input->post('soal_essay');
			$namaDepan = explode(' ', $this->session->userdata('name'));
			$id_essay = $this->input->post('id_essay');

			if ($soal_essay == '' ) {
				Redirect(base_url('Pengajar/modul/essay/'.$idm));
			}else{
				$dataEssay = array();
				foreach ($soal_essay as $key => $value) {
					$dataEssay[] = [
						'id_essay'	=> $id_essay,
						'soal_essay' => $soal_essay[$key],
						'changed_by' => $namaDepan[0],
					];
				}
				$this->m_essay->postEssay($dataEssay, 'm_essay');

				$this->session->set_flashdata('success', 'Soal essay berhasil ditambahkan!');
				Redirect(base_url('Pengajar/modul/essay/'.$idm));
			}
		}
	}

	public function detailprofile($nip)
	{
		$title = array(
			'title' => "Detail Profile"
		);
		$nip = ['nip' => $nip];
		$data['profilepengajar'] = $this->m_pengajar->getDataProfile($nip, 'm_pengajar');
		$data['system'] = $this->m_system->getSystem();

		$this->load->view('dist/header', $title);
		$this->load->view('dist/navbar2');
		$this->load->view('dist/detailprofile', $data);
		$this->load->view('dist/footer');
	}

}
