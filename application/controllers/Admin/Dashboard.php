<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function index() {
		$title = array(
			'title' => "Admin Dashboard"
		);
		$data=[
			'countPengajar' => $this->m_pengajar->countPengajarConfirmed(),
			'countSiswa'	=> $this->m_siswa->countAllSiswa(),
			'countModul'	=> $this->m_modul->countAllModul(),
			'pengajar' 		=> $this->m_pengajar->getPengajar(),
			'aktivitasBaru'	=> $this->m_modul->getModulByNewest()
		];

		$this->load->view('dist/header', $title);
		$this->load->view('dist/navbar');
		$this->load->view('dist/sidebar');
		$this->load->view('Admin/index',$data);
		$this->load->view('dist/footer');
	}

}
?>