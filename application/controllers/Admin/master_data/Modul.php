<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Modul extends CI_Controller {

	public function index()
	{
		$title['title'] = "Modul | Admin Dashboard";
		$data = [
				'modul' 	=> $this->m_modul->getModul(),
				'mapel'		=> $this->m_mapel->getMapel()
			];
		$this->load->view('dist/header', $title);
		$this->load->view('dist/navbar');
		$this->load->view('dist/sidebar');
		$this->load->view('Admin/modul/list',$data);
		$this->load->view('dist/footer');
	}

	public function loadTableModul()
	{
		$data = [
				'modul' 	=> $this->m_modul->getModul(),
				'mapel'		=> $this->m_mapel->getMapel()
			];
		$this->load->view('Admin/modul/table',$data);
	}

	public function tampilIsiModul($jenis,$id)
	{
		$title = array(
				'title' => "Edit|Modul | Admin Dashboard"
			);
			$id = ['id_modul' => $id];
			$data['modul'] = $this->m_modul->getModulById($id, 'r_modul');
			$modul = $this->db->get_where('r_modul', $id)->row_array();

		if ($jenis == 'materi') {
			$this->load->view('dist/header', $title);
			$this->load->view('dist/navbar');
			$this->load->view('dist/sidebar');
			$this->load->view('Admin/modul/edit', $data);
			$this->load->view('dist/footer');
		}elseif($jenis == 'pilgan') {
			$id = ['id_pilgan' => $modul['pilgan_id']];
			$data['pilgan'] = $this->m_pilgan->getPilganById($id, 'm_pilgan');
			$this->load->view('dist/header', $title);
			$this->load->view('dist/navbar');
			$this->load->view('dist/sidebar');
			$this->load->view('Admin/modul/edit', $data);
			$this->load->view('dist/footer');
		}elseif($jenis == 'essay') {
			$id = ['id_essay' => $modul['essay_id']];
			$data['essay'] = $this->m_essay->getEssayById($id, 'm_essay');
			$this->load->view('dist/header', $title);
			$this->load->view('dist/navbar');
			$this->load->view('dist/sidebar');
			$this->load->view('Admin/modul/edit', $data);
			$this->load->view('dist/footer');
		}
	}

	public function deleteModul($jenis,$id,$modulId)
	{
		if ($jenis == 'essayPilgan') {
			$value = explode('-', $id);
			$this->m_modul->delete_data($modulId,'id_modul','r_modul');
			$this->m_modul->delete_data($value[1],'id_essay','m_essay');
			$this->m_modul->delete_data($value[0],'id_pilgan','m_pilgan');

			$this->session->set_flashdata('success', 'Modul Berhasil dihapus!');
			Redirect('Admin/master_data/Modul');
		}elseif($jenis == 'materi'){
			$this->m_modul->delete_data($modulId,'id_modul','r_modul');
			$this->m_modul->delete_data($id,'id','m_materi');

			$this->session->set_flashdata('success', 'Modul Berhasil dihapus!');
			Redirect('Admin/master_data/Modul');
		}elseif($jenis == 'pilgan'){
			$this->m_modul->delete_data($modulId,'id_modul','r_modul');
			$this->m_modul->delete_data($id,'id_pilgan','m_pilgan');

			$this->session->set_flashdata('success', 'Modul Berhasil dihapus!');
			Redirect('Admin/master_data/Modul');
		}elseif($jenis == 'essay'){
			$this->m_modul->delete_data($modulId,'id_modul','r_modul');
			$this->m_modul->delete_data($id,'id_essay','m_essay');

			$this->session->set_flashdata('success', 'Modul Berhasil dihapus!');
			Redirect('Admin/master_data/Modul');
		}else{
			$this->session->set_flashdata('fail', 'error');
			Redirect('Admin/master_data/Modul');
		}
	}

	public function editmateri($id) {
		$this->form_validation->set_rules('judul_materi', 'Judul Materi', 'required', ['required' => 'judul materi harus diisi']);
		$this->form_validation->set_rules('mapel_id', 'Mapel', 'required', ['required' => 'mapel harus diisi']);
		$this->form_validation->set_rules('deskripsi_materi', 'Deskripsi Materi', 'required', ['required' => 'deskripsi materi harus diisi']);

		if ($this->form_validation->run() == false) {
			$title = array(
				'title' => "Modul"
			);
			$nip = ['nip' => $this->session->userdata('nip')];
			$data['auth'] = $this->m_pengajar->getDataProfile($nip, 'm_pengajar');

			$this->load->view('dist/header', $title);
			$this->load->view('dist/navbar2');
			$this->load->view('pengajar/modul/materi/'.$id, $data);
			$this->load->view('dist/footer');
		}else{
			$judul_materi = $this->input->post('judul_materi');
			$mapel_id = $this->input->post('mapel_id');
			$materi_id = $this->input->post('materi_id');
			$deskripsi_materi = $this->input->post('deskripsi_materi');
			$namaDepan = explode(' ', $this->session->userdata('name'));
			$idmateri = ['id' => $materi_id];
			$idmodul = ['id_modul' => $id];

			$dataMateri = [
				'deskripsi_materi' => $deskripsi_materi,
				'changed_by' => $namaDepan[0],
			];
			$this->m_materi->edit($idmateri, $dataMateri, 'm_materi');

			$dataModul = [
				'pengajar_nip' => $this->session->userdata('nip'),
				'judul_modul' => $judul_materi,
				'mapel_id' => $mapel_id,
				'changed_by' => $namaDepan[0],
			];
			$this->m_modul->edit($idmodul, $dataModul, 'r_modul');

			$this->session->set_flashdata('success', 'Materi berhasil diedit!');
			Redirect(base_url('Admin/master_data/Modul/tampilIsiModul/materi/'.$id));
		}
	}

	public function editpilgan($idm, $id) {
		$soal_pilgan = $this->input->post('soal_pilgan');
		$a = $this->input->post('a');
		$b = $this->input->post('b');
		$c = $this->input->post('c');
		$d = $this->input->post('d');
		$jawaban = $this->input->post('jawaban');
		$namaDepan = explode(' ', $this->session->userdata('name'));
		$idpilgan = ['id' => $id];

		$dataPilgan = [
			'soal_pilgan' => $soal_pilgan,
			'a' => $a,
			'b' => $b,
			'c' => $c,
			'd' => $d,
			'jawaban' => $jawaban,
			'changed_by' => $namaDepan[0],
		];

		$this->m_pilgan->edit($idpilgan, $dataPilgan, 'm_pilgan');
		$this->session->set_flashdata('success', 'Soal pilgan berhasil diedit!');
		Redirect(base_url('Admin/master_data/Modul/tampilIsiModul/pilgan/'.$idm));
	}

	public function editessay($idm, $id) {
		$soal_essay = $this->input->post('soal_essay');
		$namaDepan = explode(' ', $this->session->userdata('name'));
		$idEssay = ['id' => $id];

		$dataEssay = [
			'soal_essay' => $soal_essay,
			'changed_by' => $namaDepan[0],
		];

		$this->m_essay->edit($idEssay, $dataEssay, 'm_essay');
		$this->session->set_flashdata('success', 'Soal Essay berhasil diedit!');
		Redirect(base_url('Pengajar/daftarmodul'.$idm));
	}


	public function tambahpilgan($idm) {
		$this->form_validation->set_rules('waktu_pengerjaan', 'Waktu Pengerjaan', 'required', ['required' => 'waktu pengerjaan harus diisi']);
		$this->form_validation->set_rules('batas_waktu', 'Batas Waktu', 'required', ['required' => 'batas waktu harus diisi']);

		if ($this->form_validation->run() == false) {
			$title = array(
				'title' => "Mulai"
			);
			$nip = ['nip' => $this->session->userdata('nip')];
			$data['auth'] = $this->m_pengajar->getDataProfile($nip, 'm_pengajar');

			$this->load->view('dist/header', $title);
			$this->load->view('dist/navbar2');
			$this->load->view('pengajar/mulai', $data);
			$this->load->view('dist/footer');
		}else{
			$soal_pilgan = $this->input->post('soal_pilgan');
			$a = $this->input->post('a');
			$b = $this->input->post('b');
			$c = $this->input->post('c');
			$d = $this->input->post('d');
			$jawaban = $this->input->post('jawaban');
			$namaDepan = explode(' ', $this->session->userdata('name'));
			$id_pilgan = $this->input->post('id_pilgan');;

			$dataPilgan = array();
			foreach ($soal_pilgan as $key => $value) {
				$dataPilgan[] = [
					'id_pilgan'	=> $id_pilgan,
					'soal_pilgan' => $soal_pilgan[$key],
					'a' => $a[$key],
					'b' => $b[$key],
					'c' => $c[$key],
					'd' => $d[$key],
					'jawaban' => $jawaban[$key],
					'created_by' => $namaDepan[0],
				];
			}
			$this->m_pilgan->postPilgan($dataPilgan, 'm_pilgan');

			$this->session->set_flashdata('success', 'Soal pilgan berhasil ditambahkan!');
			Redirect(base_url('Admin/master_data/Modul/tampilIsiModul/pilgan/'.$idm));
		}
	}

	public function tambahessay($idm) {
		$this->form_validation->set_rules('waktu_pengerjaan', 'Waktu Pengerjaan', 'required', ['required' => 'waktu pengerjaan harus diisi']);
		$this->form_validation->set_rules('batas_waktu', 'Batas Waktu', 'required', ['required' => 'batas waktu harus diisi']);

		if ($this->form_validation->run() == false) {
			$title = array(
				'title' => "Mulai"
			);
			$nip = ['nip' => $this->session->userdata('nip')];
			$data['auth'] = $this->m_pengajar->getDataProfile($nip, 'm_pengajar');

			$this->load->view('dist/header', $title);
			$this->load->view('dist/navbar2');
			$this->load->view('pengajar/mulai', $data);
			$this->load->view('dist/footer');
		}else{
			$soal_essay = $this->input->post('soal_essay');
			$namaDepan = explode(' ', $this->session->userdata('name'));
			$id_essay = $this->input->post('id_essay');

			$dataEssay = array();
			foreach ($soal_essay as $key => $value) {
				$dataEssay[] = [
					'id_essay'	=> $id_essay,
					'soal_essay' => $soal_essay[$key],
					'changed_by' => $namaDepan[0],
				];
			}
			$this->m_essay->postEssay($dataEssay, 'm_essay');

			$this->session->set_flashdata('success', 'Soal essay berhasil ditambahkan!');
			Redirect(base_url('Admin/master_data/Modul/tampilIsiModul/essay/'.$idm));
		}
	}


}
?>