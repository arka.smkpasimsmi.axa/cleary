<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Siswa extends CI_Controller {

	public function index()
	{
		$title['title'] = "Siswa | Admin Dashboard";
		$data = [
				'system' 	=> $this->m_system->getSystem(),
				'mapel' 	=> $this->m_mapel->getMapel(),
				'siswa' 	=> $this->m_siswa->getSiswa()
			];
		$this->load->view('dist/header', $title);
		$this->load->view('dist/navbar');
		$this->load->view('dist/sidebar');
		$this->load->view('Admin/siswa/list',$data);
		$this->load->view('dist/footer');
	}

	public function insertSiswa()
	{
		
		$this->form_validation->set_rules('nama_siswa', 'Nama Siswa', 'required', 
			['required' => 'nama siswa harus diisi']);
		$this->form_validation->set_rules('nis', 'NIS', 'required', 
			['required' => 'nis siswa harus diisi']);
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[m_auth.email]', 
			['required' => 'email harus diisi'],
			['valid_email' => 'masukkan email yang benar'],
			['is_unique' => 'email sudah terdaftar!']);
		$this->form_validation->set_rules('password', 'Password', 'required', 
			['required' => 'password harus diisi']);
		$this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'required', 
			['required' => 'jenis kelamin harus diisi']);
		$this->form_validation->set_rules('agama', 'Agama', 'required', 
			['required' => 'agama harus diisi']);
		$this->form_validation->set_rules('alamat', 'Alamat', 'required', 
			['required' => 'alamat harus diisi']);
		$this->form_validation->set_rules('tanggal_lahir', 'Tanggal Lahir', 'required', 
			['required' => 'tanggal lahir harus diisi']);

		if ($this->form_validation->run() == false) {
			$title['title'] = "Siswa | Admin Dashboard";
		$data = [
				'system' 	=> $this->m_system->getSystem(),
				'mapel' 	=> $this->m_mapel->getMapel(),
				'siswa' 	=> $this->m_siswa->getSiswa()
			];
			$this->session->set_flashdata('fail', 'Pendaftaran Gagal! Silahkan Coba Lagi ');
			$this->load->view('dist/header', $title);
			$this->load->view('dist/navbar');
			$this->load->view('dist/sidebar');
			$this->load->view('Admin/siswa/list',$data);
			$this->load->view('dist/footer');
		} else {
			$nama_siswa = $this->input->post('nama_siswa');
			$nis = $this->input->post('nis');
			$email = $this->input->post('email');
			$password = $this->input->post('password');
			$jenis_kelamin = $this->input->post('jenis_kelamin');
			$agama = $this->input->post('agama');
			$alamat = $this->input->post('alamat');
			$tanggal_lahir = $this->input->post('tanggal_lahir');

			$config['upload_path']		= './assets/img/avatar';
			$config['allowed_types']	= 'jpg|png|jpeg';
			$config['file_name']		= $nama_siswa.'-'.date('y-m-d');
			$this->load->library('upload', $config);

			if(!$this->upload->do_upload('avatar')){
				$this->session->set_flashdata('fail', 'Kesalahan mengunggah gambar');
				Redirect('Auth/daftarsiswa');
			} else {
				$avatar = $this->upload->data('file_name');
				$dataAuth = [
					'name' => $nama_siswa,
					'email' => $email,
					'password' => password_hash($password, PASSWORD_DEFAULT),
					'role' => 'siswa',
					'siswa_nis' => $nis,
					'status_aktivasi' => 'dikonfirmasi',
					'created_by' => $nama_siswa
				];
				$dataSiswa = [
					'nis' => $nis,
					'nama_siswa' => $nama_siswa,
					'value_system' => $agama.'| '.$jenis_kelamin,
					'tanggal_lahir' => $tanggal_lahir,
					'alamat' => $alamat,
					'avatar' => $avatar,
					'created_by' => $nama_siswa
				];
				$this->m_auth->daftar($dataAuth, 'm_auth');
				$this->m_auth->daftar($dataSiswa, 'm_siswa');
				$this->session->set_flashdata('succes', 'Pendaftaran berhasil');
				Redirect('Admin/master_data/Siswa');
			}
		}

	}

	public function tampilEdit($nis)
	{
		$nis = ['nis' => $nis];
		$title['title'] = " Edit|Siswa | Admin Dashboard";
		$data = [
			'siswa'	=> $this->m_siswa->getDataProfile($nis,'m_siswa'),
			'system' 	=> $this->m_system->getSystem(),
			'mapel' 	=> $this->m_mapel->getMapel()
		];

		$this->load->view('dist/header', $title);
		$this->load->view('dist/navbar');
		$this->load->view('dist/sidebar');
		$this->load->view('Admin/siswa/edit',$data);
		$this->load->view('dist/footer');
	}

	public function postprofile($niss) {
		$nis= ['nis' => $niss];
		$nama_siswa = $this->input->post('nama_siswa');
		$tanggal_lahir = $this->input->post('tanggal_lahir');
		$jenis_kelamin = $this->input->post('jenis_kelamin');
		$agama = $this->input->post('agama');
		$no_hp = $this->input->post('no_hp');
		$alamat = $this->input->post('alamat');
		$bio = $this->input->post('bio');

		$data = [
			'nama_siswa' => $nama_siswa,
			'value_system' => $agama.'| '.$jenis_kelamin,
			'tanggal_lahir' => $tanggal_lahir,
			'no_hp' => $no_hp,
			'alamat' => $alamat,
			'bio' => $bio,
			'changed_by' => 'SYSTEM'
		];

		$this->m_siswa->edit($data, $nis ,'m_siswa');
		$this->session->set_flashdata('success', 'Data berhasil diperbarui');
		redirect(base_url('Admin/master_data/Siswa/tampilEdit/').$niss);
	}

	public function deleteSiswa()
	{
		$id = $this->input->post('nis');

		$this->m_siswa->delete_data($id,'m_siswa');
		$this->m_auth->deleteSiswa($id,'m_auth');

		$this->session->set_flashdata('success', 'Akun Berhasil dihapus!');
		Redirect('Admin/master_data/Siswa');
	}

	public function deleteSiswaEdit($nis)
	{
		$this->m_siswa->delete_data($nis,'m_siswa');
		$this->m_auth->deleteSiswa($nis,'m_auth');

		$this->session->set_flashdata('success', 'Akun Berhasil dihapus!');
		Redirect('Admin/master_data/Siswa');
	}


}
?>