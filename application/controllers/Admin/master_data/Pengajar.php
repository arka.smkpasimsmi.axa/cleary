<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengajar extends CI_Controller {

	public function index()
	{
		$title['title'] = "Pengajar | Admin Dashboard";
		$data = [
				'system' 	=> $this->m_system->getSystem(),
				'mapel' 	=> $this->m_mapel->getMapel(),
				'pengajar' 	=> $this->m_pengajar->getPengajar()
			];
		$this->load->view('dist/header', $title);
		$this->load->view('dist/navbar');
		$this->load->view('dist/sidebar');
		$this->load->view('Admin/pengajar/list',$data);
		$this->load->view('dist/footer');
	}

	public function indexPengajuan()
	{
		$title['title'] = "Pengajuan Pengajar | Admin Dashboard";
		$data = [
				'system' 	=> $this->m_system->getSystem(),
				'mapel' 	=> $this->m_mapel->getMapel(),
				'pengajar' 	=> $this->m_pengajar->getPengajar()
			];
		$this->load->view('dist/header', $title);
		$this->load->view('dist/navbar');
		$this->load->view('dist/sidebar');
		$this->load->view('Admin/pengajar/pengajuan',$data);
		$this->load->view('dist/footer');
	}

	public function insertPengajar()
	{
		$this->form_validation->set_rules('nama_pengajar', 'Nama Pengajar', 'required',
    		['required' => 'nama pengajar harus diisi!']);
    	$this->form_validation->set_rules('nip', 'NIP', 'required|is_unique[m_pengajar.nip]',
    		['required' => 'NIP harus diisi!',
    		 'is_unique' => 'NIP sudah terdaftar!']);

    	$this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[m_auth.email]',
    		['required' => 'email harus diisi!',
				'valid_email' => 'masukan email yang benar!',
				'is_unique' => 'email sudah terdaftar!']);

    	$this->form_validation->set_rules('password', 'Password', 'required',
    		['required' => 'Password harus diisi!']);

    	$this->form_validation->set_rules('alamat', 'Alamat', 'required',
    		['required' => 'Alamat harus diisi!']);

    	$this->form_validation->set_rules('mapel_id', 'Mapel', 'required',
    		['required' => 'Mapel harus diisi!']);

    	$this->form_validation->set_rules('tanggal_lahir', 'Tanggal Lahir', 'required',
    		['required' => 'Tanggal Lahir harus diisi!']);

    	$this->form_validation->set_rules('agama', 'Agama', 'required',
    		['required' => 'Agama harus diisi!']);

    	$this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'required',
    		['required' => 'Jenis Kelamin harus diisi!']);
    	$this->form_validation->set_rules('status_kawin', 'Status Kawin', 'required',
    		['required' => 'Status Kawin harus diisi!']);


    	if($this->form_validation->run() == false) {
				$title['title'] = "Pengajar | Admin Dashboard";
				$data = array(
				'system' 	=> $this->m_system->getSystem(),
				'mapel' 	=> $this->m_mapel->getMapel(),
				'pengajar' 	=> $this->m_pengajar->getPengajar()
			);
			$this->session->set_flashdata('fail', 'Pendaftaran Gagal! Silahkan coba lagi.');
			$this->load->view('dist/header', $title);
			$this->load->view('dist/navbar');
			$this->load->view('dist/sidebar');
			$this->load->view('Admin/pengajar/list',$data);
			$this->load->view('dist/footer');
		}else{
			$nama_pengajar 	= $this->input->post('nama_pengajar');
			$nip 			= $this->input->post('nip');
			$email 			= $this->input->post('email');
			$password 		= $this->input->post('password');
			$alamat 		= $this->input->post('alamat');
			$mapel_id 		= $this->input->post('mapel_id');
			$tanggal_lahir 	= $this->input->post('tanggal_lahir');
			$agama 			= $this->input->post('agama');
			$jenis_kelamin 	= $this->input->post('jenis_kelamin');
			$status_kawin 	= $this->input->post('status_kawin');

				$config['upload_path']		= './assets/img/avatar/';
				$config['allowed_types']	= 'jpg|png|jpeg';
				$config['file_name']		= $nama_pengajar.'-'.date('y-m-d');
				$this->load->library('upload', $config);

				if(!$this->upload->do_upload('avatar')){
					$this->session->set_flashdata('fail', 'Kesalahan mengunggah gambar!');
    				Redirect('Admin/master_data/Pengajar');
				}else{
					$avatar = $this->upload->data('file_name');
					$dataAuth = [
						'name' 				=> $nama_pengajar,
						'email' 			=> $email,
						'password' 			=> password_hash($password, PASSWORD_DEFAULT),
						'role' 				=> 'pengajar',
						'pengajar_nip'	 	=> $nip,
						'status_aktivasi' 	=> 'dikonfirmasi',
						'created_by' 		=> $this->session->userdata('name')
					];
					$dataPengajar = [
						'nip' 			=> $nip,
						'nama_pengajar' => $nama_pengajar,
						'mapel_id'		=> $mapel_id,
						'value_system' 	=> $agama.'| '.$jenis_kelamin.'| '.$status_kawin,
						'tanggal_lahir' => $tanggal_lahir,
						'alamat' 		=> $alamat,
						'avatar' 		=> $avatar,
						'created_by' 	=> $this->session->userdata('name')
					];
					$this->m_auth->daftar($dataAuth, 'm_auth');
					$this->m_auth->daftar($dataPengajar, 'm_pengajar');
					$this->session->set_flashdata('success' , 'Pendaftaran Berhasil!');
					Redirect('Admin/master_data/Pengajar');
		}
	}
}

	public function tampilEdit($nip)
	{
		$nip = ['nip' => $nip];
		$title['title'] = " Edit|Pengajar | Admin Dashboard";
		$data = [
			'pengajar'	=> $this->m_pengajar->getDataProfile($nip,'m_pengajar'),
			'system' 	=> $this->m_system->getSystem(),
			'mapel' 	=> $this->m_mapel->getMapel()
		];

		$this->load->view('dist/header', $title);
		$this->load->view('dist/navbar');
		$this->load->view('dist/sidebar');
		$this->load->view('Admin/pengajar/edit',$data);
		$this->load->view('dist/footer');
	}

	public function postEdit($nips) {
		$nip = ['nip' => $nips];
		$nama_pengajar = $this->input->post('nama_pengajar');
		$agama = $this->input->post('agama');
		$jenis_kelamin = $this->input->post('jenis_kelamin');
		$status_kawin = $this->input->post('status_kawin');
		$tanggal_lahir = $this->input->post('tanggal_lahir');
		$no_hp = $this->input->post('no_hp');
		$alamat = $this->input->post('alamat');
		$bio = $this->input->post('bio');

		$data = [
			'nama_pengajar' => $nama_pengajar,
			'value_system'  => $agama.'| '.$jenis_kelamin.'| '.$status_kawin,
			'tanggal_lahir' => $tanggal_lahir,
			'no_hp' => $no_hp,
			'alamat' => $alamat,
			'bio' => $bio,
			'changed_by' => 'SYSTEM'
		];

		$this->m_pengajar->edit($data, $nip, 'm_pengajar');
		$this->session->set_flashdata('success', 'Data berhasil diperbarui!');
		redirect(base_url('Admin/master_data/Pengajar/tampilEdit/').$nips);
	}

	public function deletePengajar()
	{
		$id = $this->input->post('nip');

		$this->m_pengajar->delete_data($id,'m_pengajar');
		$this->m_auth->deletePengajar($id,'m_auth');

		$this->session->set_flashdata('success', 'Akun Berhasil dihapus!');
		Redirect('Admin/master_data/Pengajar');
	}

	public function pengajarAksi($nip,$jenis)
	{
		if ($jenis == "hapus") {
			$this->m_pengajar->delete_data($nip,'m_pengajar');
			$this->m_auth->deletePengajar($nip,'m_auth');

			$this->session->set_flashdata('success', 'Akun Berhasil dihapus!');
			Redirect('Admin/master_data/Pengajar');
		}else{
			$data=[
				'status_aktivasi'	=>	$jenis
			];
			$this->m_auth->editPengajar($nip,$data,'m_auth');
			$this->session->set_flashdata('success', 'Status Pengajar Berhasil Diubah!');
			redirect('Admin/master_data/Pengajar/indexPengajuan');
		}
	}

		
}
?>