<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mapel extends CI_Controller {

	public function index()
	{
		$title['title'] = "Mapel | Admin Dashboard";
		$data = [
				'system' 		=> $this->m_system->getSystem(),
				'mapel' 		=> $this->m_mapel->getMapel(),
				'countPengajar'	=> $this->m_mapel->countPengajarMapel()->result(),
				'countNich'		=> $this->m_mapel->countPengajarMapel()->num_rows()

			];
		$this->load->view('dist/header', $title);
		$this->load->view('dist/navbar');
		$this->load->view('dist/sidebar');
		$this->load->view('Admin/mapel/list',$data);
		$this->load->view('dist/footer');
	}

	public function insertMapel()
	{
		$this->form_validation->set_rules('mapel','Mapel','required|is_unique[m_mapel.nama_mapel]',
			['required' => 'Kolom harus diisi!',
    		 'is_unique' => 'Mapel sudah terdaftar!']);

		if ($this->form_validation->run()==FALSE) {
			$title['title'] = "Mapel | Admin Dashboard";
			$data = [
					'system' 	=> $this->m_system->getSystem(),
					'mapel' 	=> $this->m_mapel->getMapel()
				];
			$this->session->set_flashdata('fail','Kesalahan! Mohon coba kembali');
			$this->load->view('dist/header', $title);
			$this->load->view('dist/navbar');
			$this->load->view('dist/sidebar');
			$this->load->view('Admin/mapel/list',$data);
			$this->load->view('dist/footer');
		}else{
			$mapel = $this->input->post('mapel');

			$data=[
				'nama_mapel'	=> $mapel,
				'created_by'	=> 'SYSTEM'
			];

			$this->m_mapel->insert($data,'m_mapel');
			$this->session->set_flashdata('success' , 'Pendaftaran Berhasil!');
			Redirect('Admin/master_data/Mapel');
		}
	}

	public function tampilEdit($id)
	{
		$id = ['id' => $id];
		$title['title'] = " Edit|Mapel | Admin Dashboard";
		$data = [
			'mapel'	=> $this->m_mapel->tampil_edit($id,'m_mapel')
		];

		$this->load->view('dist/header', $title);
		$this->load->view('dist/navbar');
		$this->load->view('dist/sidebar');
		$this->load->view('Admin/mapel/edit',$data);
		$this->load->view('dist/footer');
	}

	public function postEdit($id) {
		$id = ['id' => $id];
		$mapel = $this->input->post('mapel');

		$data=[
				'nama_mapel'	=> $mapel,
				'changed_by'	=> 'SYSTEM'
			];

		$this->m_mapel->edit($data, $id, 'm_mapel');
		$this->session->set_flashdata('success', 'Data berhasil diperbarui!');
		redirect('Admin/master_data/Mapel');	
	}

	public function deleteMapel()
	{
		$id = $this->input->post('id');

		$this->m_mapel->delete_data($id,'m_mapel');

		$this->session->set_flashdata('success', 'Mata Pelajaran Berhasil dihapus!');
		Redirect('Admin/master_data/Mapel');
	}

}
?>