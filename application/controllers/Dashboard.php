<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function index() {
		$title = array(
			'title' => "Ecommerce Dashboard"
		);

		$this->load->view('dist/header', $title);
		$this->load->view('dist/navbar');
		$this->load->view('dist/sidebar');
		$this->load->view('dashboard');
		$this->load->view('dist/footer');
	}
	

}
