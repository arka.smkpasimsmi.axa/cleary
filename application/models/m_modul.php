<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class m_modul extends CI_Model{

		public function getModul()
		{
			$this->db->join('m_pengajar','m_pengajar.nip=r_modul.pengajar_nip','left');
			$this->db->select('r_modul.*,m_pengajar.nama_pengajar as nama_pengajar');
			$this->db->join('m_tipe_modul','m_tipe_modul.id=r_modul.tipe_modul_id','left');
			$this->db->select('r_modul.*,m_tipe_modul.tipe_modul as nama_modul');
			$this->db->join('m_mapel','m_mapel.id=r_modul.mapel_id','left');
			$this->db->select('r_modul.*,m_mapel.nama_mapel as nama_mapel');
			return $this->db->get('r_modul')->result();
		}

		public function postModul($data, $table) {
			$this->db->insert($table, $data);
		}

		public function getModulById($id, $table) {
			$this->db->join('m_pengajar', 'm_pengajar.nip=r_modul.pengajar_nip','left');
			$this->db->select('r_modul.*,m_pengajar.nama_pengajar as nama_pengajar');

			$this->db->join('m_mapel', 'm_mapel.id=r_modul.mapel_id','left');
			$this->db->select('r_modul.*,m_mapel.nama_mapel as nama_mapel');

			$this->db->join('m_tipe_modul', 'm_tipe_modul.id=r_modul.tipe_modul_id','left');
			$this->db->select('r_modul.*,m_tipe_modul.tipe_modul as tipe_modul');

			$this->db->join('m_materi', 'm_materi.id=r_modul.materi_id','left');
			$this->db->select('r_modul.*,m_materi.deskripsi_materi as deskripsi_materi');
			
			return $this->db->get_where($table, $id)->result();
		}

		public function edit($id, $data, $table) {
			$this->db->where($id);
			$this->db->update($table, $data);
		}

		public function countAllModul()
		{
			$query = $this->db->query('SELECT * FROM r_modul');
			$hasil = $query->num_rows();
			return $hasil;
		}

		public function tampilDetail($id, $table) {
			return $this->db->get_where($table, $id);
		}


		public function getModulByNewest()
		{
			$this->db->join('m_pengajar','m_pengajar.nip=r_modul.pengajar_nip','left');
			$this->db->select('r_modul.*,m_pengajar.nama_pengajar as nama_pengajar');
			$this->db->join('m_tipe_modul','m_tipe_modul.id=r_modul.tipe_modul_id','left');
			$this->db->select('r_modul.*,m_tipe_modul.tipe_modul as nama_modul');
			$this->db->join('m_mapel','m_mapel.id=r_modul.mapel_id','left');
			$this->db->select('r_modul.*,m_mapel.nama_mapel as nama_mapel');
			$this->db->order_by('r_modul.created_dt','ASC');
			return $this->db->get('r_modul')->result();
		}

		public function delete_data($id,$jenisId, $table) {
			$this->db->where_in($jenisId, $id);
			$this->db->delete($table);
		}
		public function hapus($id, $table) {
	        $this->db->where_in('id_modul', $id);
	        $this->db->delete($table);
	    }

	    public function getNilai() {
			$this->db->join('m_siswa','m_siswa.nis=r_nilai.siswa_nis','left');
			$this->db->select('r_nilai.*,m_siswa.nis as nis, m_siswa.nama_siswa as nama_siswa');


			$this->db->join('r_modul','r_modul.id_modul=r_nilai.modul_id','left');
			$this->db->select('r_nilai.*,r_modul.judul_modul as judul_modul');


			$this->db->join('m_mapel','m_mapel.id=r_nilai.mapel_id','left');
			$this->db->select('r_nilai.*,m_mapel.nama_mapel as nama_mapel');


			$this->db->join('m_tipe_modul','m_tipe_modul.id=r_nilai.tipe_modul_id','left');
			$this->db->select('r_nilai.*,m_tipe_modul.tipe_modul as tipe_modul');

			return $this->db->get('r_nilai')->result();
		}
		
	}

?>