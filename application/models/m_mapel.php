<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class m_mapel extends CI_Model{


		public function getMapel() {
			$this->db->order_by('nama_mapel', 'ASC');
			return $this->db->get('m_mapel')->result();
		}

		public function countPengajar($mapel)
		{
			 $query = $this->db->query('SELECT * FROM m_pengajar WHERE mapel_id ='.$mapel);
			 $total = $query->num_rows();
			 return $total;
		}

		public function insert($data, $table)
		{
			$this->db->insert($table,$data);
		}

		public function tampil_edit($id, $table) {
			return $this->db->get_where($table, $id)->result();
		}

		public function edit($data, $nip, $table) {
			$this->db->where($nip);
			$this->db->update($table, $data);
		}

		public function delete_data($id, $table) {
			$this->db->where_in('id', $id);
			$this->db->delete($table);
		}

		public function countPengajarMapel()
		{
			$this->db->join('m_pengajar','m_pengajar.mapel_id=m_mapel.id');
			$this->db->select('m_mapel.*,m_pengajar.mapel_id as idmapel');
			return $this->db->get('m_mapel');
		}

		public function countModulMapel()
		{
			$query = $this->db->query('SELECT * FROM m_mapel,r_modul WHERE m_mapel.id = r_modul.mapel_id');
			$hitung = $query->num_rows();
			return $hitung;
		}
	}

?>