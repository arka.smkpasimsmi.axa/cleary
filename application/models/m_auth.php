<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class m_auth extends CI_Model {

	public function daftar($data, $table) {
		$this->db->insert($table, $data);
	}

	public function edit($id, $data, $table) {
		$this->db->where($id);
		$this->db->update($table, $data);
	}

	public function editPengajar($id, $data, $table)
	{
		$this->db->where('pengajar_nip',$id);
		$this->db->update($table, $data);
	}

	public function getAccount()
	{
		return $this->db->get('m_auth')->result();
	}

	public function deletePengajar($id, $table) {
		$this->db->where_in('pengajar_nip', $id);
		$this->db->delete($table);
	}

	public function deleteSiswa($id, $table) {
		$this->db->where_in('siswa_nis', $id);
		$this->db->delete($table);
	}

}    
