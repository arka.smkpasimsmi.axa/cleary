<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class m_system extends CI_Model {

    public function getSystem() {
        return $this->db->get('m_system')->result();
    }

}