<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class m_pengajar extends CI_Model {

		public function getDataProfile($nip, $table) {
			$this->db->join('m_mapel', 'm_mapel.id=m_pengajar.mapel_id','left');
			$this->db->select('m_pengajar.*,m_mapel.nama_mapel as nama_mapel');
			$this->db->join('m_auth','m_pengajar.nip = m_auth.pengajar_nip','left');
			$this->db->select('m_pengajar.*,m_auth.email as email_pengajar,m_auth.status_aktivasi as status');
			return $this->db->get_where($table, $nip)->result();
		}

		public function edit($data, $nip, $table) {
			$this->db->where($nip);
			$this->db->update($table, $data);
		}

		public function pengikuts($pengikut) {
			$this->db->select('*');
			$this->db->from('m_siswa');
			$this->db->where_in('nis', $pengikut);
			return $this->db->get()->result();
		}

		public function get_pengajar() {
			return $this->db->get('m_pengajar')->result();
		}

		public function get_mapel() {
			$this->db->select('
				m_pengajar.*, m_mapel.id AS mapel_id, m_mapel.nama_mapel
			');
			$this->db->join('m_mapel', 'm_pengajar.mapel_id = m_mapel.id');
			$this->db->from('m_pengajar');
			$this->db->order_by('pengikut_nis', 'DESC');
			$query = $this->db->get();
			return $query->result();
		}
		public function pengikut($data, $table) {
			$this->db->insert($table, $data);
		}

		public function getPengajar()
		{
			$this->db->join('m_mapel','m_pengajar.mapel_id=m_mapel.id','left');
			$this->db->select('m_pengajar.*, m_mapel.nama_mapel as namapel');
			$this->db->join('m_auth','m_pengajar.nip = m_auth.pengajar_nip','left');
			$this->db->select('m_pengajar.*,m_auth.email as email_pengajar,m_auth.status_aktivasi as status');
			return $this->db->get('m_pengajar')->result();
		}

		public function getWherePengajar($id)
		{
			$this->db->join('m_mapel','m_pengajar.mapel_id=m_mapel.id','left');
			$this->db->select('m_pengajar.*, m_mapel.nama_mapel as namapel');
			$this->db->where('m_pengajar.nip',$id);
			return $this->db->get('m_pengajar');
		}

		public function create_data($data, $table) {
			$this->db->insert($table, $data);
		}

		public function delete_data($id, $table) {
			$this->db->where_in('nip', $id);
			$this->db->delete($table);
		}

		public function tampil_edit($id, $table) {
			return $this->db->get_where($table, $id);
		}

		public function countPengajarConfirmed()
		{
			$query = $this->db->query('SELECT * FROM m_auth WHERE status_aktivasi = "dikonfirmasi" AND role ="pengajar"');
			$total = $query->num_rows();
			return $total;
		}

		public function countModulPengajarMax()
		{
			$query = $this->db->query('SELECT MAX("modul") * FROM m_pengajar ');
		}

	}

?>