<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class m_pilgan extends CI_Model{

		public function postPilgan($data, $table) {
			$this->db->insert_batch($table, $data);
		}

		public function getPilganById($id, $table) {
			return $this->db->get_where($table, $id)->result();
		}

		public function hapus($id, $table) {
			$this->db->where_in('id', $id);
			$this->db->delete($table);
		}

		public function edit($id, $data, $table) {
			$this->db->where($id);
			$this->db->update($table, $data);
		}


		public function hapusByModulId($id, $table) {
			$this->db->where_in('id_pilgan', $id);
			$this->db->delete($table);
		}

	}

?>