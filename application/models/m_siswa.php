<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class m_siswa extends CI_Model {

    public function getDataProfile($nis, $table) {
        $this->db->join('m_auth','m_siswa.nis = m_auth.siswa_nis','left');
        $this->db->select('m_siswa.*,m_auth.email as email_siswa');       
        return $this->db->get_where($table, $nis)->result();
    }
    public function edit($data, $nis, $table){
        $this->db->where($nis);
        $this->db->update($table, $data);
    }
    public function ikuti($data, $table) {
		$this->db->insert($table, $data);
    }
    public function mengikuts($mengikuti)
    {
        $this->db->select('*');
        $this->db->from('m_pengajar');
        $this->db->where_in('nip', $mengikuti);
        return $this->db->get()->result();
    }
    public function datamengikuti($datamengikuti)
    {
        $this->db->select('*');
        $this->db->from('r_modul');
        $this->db->where_in('pengajar_nip', $datamengikuti);
        $this->db->join('m_pengajar','m_pengajar.nip = r_modul.pengajar_nip','left');
        $this->db->select('r_modul.*,m_pengajar.nama_pengajar as nama_pengajar');
        return $this->db->get()->result();
    }
    public function get_data()
    {
		return $this->db->get('m_siswa')->result();
    }
    public function listmodul($table) {
        $this->db->join('m_mapel', 'm_mapel.id=r_modul.mapel_id','left');
        $this->db->select('r_modul.*,m_mapel.nama_mapel as nama_mapel');
        $this->db->join('m_pengajar','m_pengajar.nip = r_modul.pengajar_nip','left');
        $this->db->select('r_modul.*,m_pengajar.nama_pengajar as nama_pengajar');
        $this->db->join('m_tipe_modul','m_tipe_modul.id=r_modul.tipe_modul_id','left');
        $this->db->select('r_modul.*,m_tipe_modul.tipe_modul as tipe_modul');
        return $this->db->get_where($table)->result();
    }
    public function listnilai($table) {
        $this->db->join('m_mapel', 'm_mapel.id=r_nilai.mapel_id','left');
        $this->db->select('r_nilai.*,m_mapel.nama_mapel as nama_mapel');
        $this->db->join('m_pengajar','m_pengajar.nip = r_nilai.pengajar_nip','left');
        $this->db->select('r_nilai.*,m_pengajar.nama_pengajar as nama_pengajar');
        return $this->db->get_where($table)->result();
    }
    public function getSiswa()
    {
        return $this->db->get('m_siswa')->result();
    }

    public function delete_data($id, $table) {
        $this->db->where_in('nis', $id);
        $this->db->delete($table);
    }

    public function countAllSiswa()
    {
        $query = $this->db->query('SELECT * FROM m_siswa');
        $hasil = $query->num_rows();
        return $hasil;
    }
    public funcTion get_keyword($keyword, $table) {
        $this->db->join('m_mapel', 'm_mapel.id=r_modul.mapel_id','left');
        $this->db->select('r_modul.*,m_mapel.nama_mapel as nama_mapel');
        $this->db->join('m_pengajar','m_pengajar.nip = r_modul.pengajar_nip','left');
        $this->db->select('r_modul.*,m_pengajar.nama_pengajar as nama_pengajar');
        $this->db->like('judul_modul', $keyword);
        $this->db->or_like('tipe_modul_id', $keyword);
        return $this->db->get_where($table)->result();
    }
    public function nilais($nilai)
    {
        $this->db->select('*');
        $this->db->from('r_nilai');
        $this->db->where_in('modul_id', $nilai);
        return $this->db->get()->result();
    }
    
}    