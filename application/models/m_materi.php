<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class m_materi extends CI_Model{

		public function postMateri($data, $table) {
			$this->db->insert($table, $data);
		}

		public function edit($id, $data, $table) {
			$this->db->where($id);
			$this->db->update($table, $data);
		}

		public function hapusByModulId($id, $table) {
			$this->db->where_in('id', $id);
			$this->db->delete($table);
		}

	}

?>