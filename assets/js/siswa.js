
  $("#berhenti-mengikuti").click(function(e) {

    e.preventDefault();
    const href = $(this).attr('href');

    swal({
        title: 'Yakin Berhenti Mengikuti?',
        text: 'Anda bisa kembali mengikuti di halaman daftar pengajar',
        icon: 'info',
        buttons: true,
        dangerMode: true,
      })
      .then((result) => {
        if (result) {
        $("#berhenti").submit();
        swal('Anda berhasil berhenti mengikuti!', {
          icon: 'success',
        });
        } else {
        swal('Anda membatalkan aksi');
        }
      });
  });
